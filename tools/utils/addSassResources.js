const path = require('path');

const addSassResources = (rules) => {
  rules.map((rule) => {
    if (rule.test instanceof RegExp && rule.test.toString() === '/\\.css$|\\.scss$|\\.sass$|\\.less$|\\.styl$/') {
      rule.oneOf.map((item) => {
        if (item.test instanceof RegExp && (item.test.toString() === '/\\.module\\.(scss|sass)$/' || item.test.toString() === '/\\.scss$|\\.sass$/')) {
          item.use.push({
            loader: require.resolve('sass-resources-loader'),
            options: {
              resources: path.resolve(__dirname, '../../libs/styles/src/core.sass'),
            },
          });
        }
        return item;
      });
    }
    return rule;
  });

  // FOR testing
  // rules.forEach((rule) => {
  //   if (rule.test.toString() === '/\\.css$|\\.scss$|\\.sass$|\\.less$|\\.styl$/') {
  //     rule.oneOf.forEach((item) => {
  //       if (item.test.toString() === '/\\.module\\.(scss|sass)$/' || item.test.toString() === '/\\.scss$|\\.sass$/') {
  //         console.log('item', item);
  //       }
  //     });
  //   }
  // });
  // process.exit();
};

module.exports = addSassResources;
