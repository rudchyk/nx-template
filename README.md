# NxTemplate

This project was generated using [Nx](https://nx.dev).

## Getting Started

### Start

1. Create the project monorepo:

- app: client

```
yarn create nx-workspace
```

2. COPY form REPO ./tools/utils to {project}/tools/utils

#### Clients libs

1. Run command:

```
mkdir -p apps/client/src/components/lib && touch apps/client/src/components/index.ts
mkdir -p apps/client/src/layouts/lib && touch apps/client/src/layouts/index.ts
mkdir -p apps/client/src/modules/lib && touch apps/client/src/modules/index.ts
mkdir -p apps/client/src/pages/lib && touch apps/client/src/pages/index.ts
mkdir -p apps/client/src/templates/lib && touch apps/client/src/templates/index.ts
mkdir -p apps/client/src/hooks/lib && touch apps/client/src/hooks/index.ts
mkdir -p apps/client/src/services/lib && touch apps/client/src/services/index.ts
mkdir -p apps/client/src/utils/lib && touch apps/client/src/utils/index.ts
mkdir -p apps/client/src/forms/lib && touch apps/client/src/forms/index.ts
```

2. ./tsconfig.base.json

```
{
  ...,
  "compilerOptions": {
    ...,
    "paths": {
      "@client/components": [
        "apps/client/src/components/index.ts"
      ],
      "@client/layouts": [
        "apps/client/src/layouts/index.ts"
      ],
      "@client/modules": [
        "apps/client/src/modules/index.ts"
      ],
      "@client/pages": [
        "apps/client/src/pages/index.ts"
      ],
      "@client/templates": [
        "apps/client/src/templates/index.ts"
      ],
      "@client/hooks": [
        "apps/client/src/hooks/index.ts"
      ],
      "@client/services": [
        "apps/client/src/services/index.ts"
      ],
      "@client/utils": [
        "apps/client/src/utils/index.ts"
      ],
      "@client/forms": [
        "apps/client/src/forms/index.ts"
      ],
    }
  }
}
```

3. COPY form REPO ./tools/element to {project}/tools/element

#### Client store

1. Run command:

```
yarn add @reduxjs/toolkit redux-thunk redux react-redux
mkdir -p apps/client/src/store/slices/lib && touch apps/client/src/store/store.ts apps/client/src/store/reducers.ts apps/client/src/store/slices/index.ts
```

2. ./tsconfig.base.json

```
{
  ...,
  "compilerOptions": {
    ...,
    "paths": {
      "@client/store": [
        "apps/client/src/store/store.ts"
      ],
      "@client/reducers": [
        "apps/client/src/store/slices/index.ts"
      ],
    }
  }
}
```

3. COPY form REPO ./tools/redux to {project}/tools/redux

4. {project}/apps/client/src/store/store.ts

```
import { configureStore } from '@reduxjs/toolkit';
import ReduxThunk from 'redux-thunk';
import { isDev } from '@utils';
import { reducers } from './reducers';

export type RootState = ReturnType<typeof store.getState>;

const store = configureStore({
  reducer: reducers,
  middleware: [ReduxThunk],
  devTools: isDev(),
});

export default store;

```

5. {project}/apps/client/src/store/reducers.ts

```
import { combineReducers } from 'redux';
import {
  // imports
} from '@client/reducers';

export interface StoreState {
  // state
}

export const reducers = combineReducers<StoreState>({
  // reducers
});

```

#### Libs

##### Interfaces

```
yarn nx generate @nrwl/workspace:library --name=client --directory=interfaces --unitTestRunner=none --no-interactive
yarn nx generate @nrwl/workspace:library --name=common --directory=interfaces --unitTestRunner=none --no-interactive
```

##### Constants

```
yarn nx generate @nrwl/workspace:library --name=client --directory=constants --unitTestRunner=none --no-interactive &&
yarn nx generate @nrwl/workspace:library --name=common --directory=constants --unitTestRunner=none --no-interactive
```

yarn nx generate @nrwl/workspace:library --name=interfaces --unitTestRunner=none --no-interactive yarn nx generate @nrwl/workspace:library --name=constants --unitTestRunner=none --no-interactive

yarn nx generate @nrwl/workspace:remove --projectName=interfaces-api --forceRemove --no-interactive

##### UI

```
yarn nx generate @nrwl/react:library --name=ui --appProject=client --no-component --no-interactive
rm -rf libs/ui/src/lib libs/ui/src/index.ts
mkdir -p libs/ui/src/components/lib && touch libs/ui/src/components/index.ts
mkdir -p libs/ui/src/styles
mkdir -p libs/ui/src/svg/lib && touch libs/ui/src/svg/index.ts
yarn nx generate @nrwl/storybook:configuration --name=ui --uiFramework=@storybook/react --no-interactive
yarn add @types/prismjs -D
```

##### Utils

```
yarn nx generate @nrwl/workspace:library --no-component --name=utils --no-interactive
```

### Linters

1. Run command:

```
yarn add eslint-plugin-jest eslint-plugin-prettier -D
```

2. ./.prettierrc

```
{
  "tabWidth": 2,
  "trailingComma": "es5",
  "semi": true,
  "singleQuote": true,
  "endOfLine": "lf",
  "jsxSingleQuote": false,
  "bracketSameLine": true,
  "arrowParens": "always",
  "bracketSpacing": true,
  "proseWrap": "never",
  "printWidth": 160
}
```

3. ./.prettierignore

```
...
/dist-tools
```

4. ./.eslintrc.json

COPY form REPO ./.eslintrc.json

5. lint-staged

```
npx mrm@2 lint-staged
```

### ./.gitignore

```
...
/dist-tools
```

### ./apps/client/tsconfig.json

```
{
  "compilerOptions": {
    ...,
    "jsx": "react-jsx",
    "allowJs": true,
    "esModuleInterop": true,
    "allowSyntheticDefaultImports": true,
    "forceConsistentCasingInFileNames": true,
    "strict": true,
    "noImplicitReturns": true,
    "noFallthroughCasesInSwitch": true,
    "noImplicitOverride": true,
    "noPropertyAccessFromIndexSignature": false,
  }
}
```

### Helpers and tools

#### classnames

- [classnames](https://www.npmjs.com/package/classnames)
- [js-cookie](https://www.npmjs.com/package/js-cookie)
- [React Timeout](https://github.com/plougsgaard/react-timeout)

```
yarn add classnames js-cookie react-timeout
yarn add @types/js-cookie @types/react-timeout -D
```

#### Error Boundaries

- [React error boundary component](https://github.com/bvaughn/react-error-boundary)

```
yarn add react-error-boundary
```

#### Internationalization

- [react-i18next](https://react.i18next.com/)

```
yarn add react-i18next i18next
```

#### React Feature Toggles

- [React Feature Toggles](https://github.com/paralleldrive/react-feature-toggles)

```
yarn add @paralleldrive/react-feature-toggles
```

#### MUI

- [mui](https://mui.com/)
- [material-icons](https://mui.com/components/material-icons/)
- [dark-mode](https://mui.com/customization/dark-mode/)

```
yarn add @mui/material @emotion/react @emotion/styled @mui/icons-material @mui/lab @mui/styles
```

#### react-hook-form

- [react-hook-form](https://react-hook-form.com/get-started)

```
yarn add react-hook-form @hookform/resolvers yap
```

#### Styles

- [SASS](https://sass-lang.com/)

```
yarn nx generate @nrwl/workspace:library --name=styles --unitTestRunner=none --no-interactive
```

### API

#### API libs

1.

```
yarn add --dev @nrwl/express
yarn nx g @nrwl/express:app api --frontendProject client
```

1. Run command:

```
mkdir -p apps/api/src/controllers/lib && touch apps/api/src/controllers/index.ts
mkdir -p apps/api/src/decorators/lib && touch apps/api/src/decorators/index.ts
mkdir -p apps/api/src/models/lib && touch apps/api/src/models/index.ts
mkdir -p apps/api/src/routes/lib && touch apps/api/src/routes/index.ts
mkdir -p apps/api/src/services/lib && touch apps/api/src/services/index.ts
mkdir -p apps/api/src/validators/lib && touch apps/api/src/validators/index.ts
mkdir -p apps/api/src/utils/lib && touch apps/api/src/utils/index.ts
mkdir -p apps/api/src/middlewares/lib && touch apps/api/src/middlewares/index.ts
```

2. ./tsconfig.base.json

```
{
  ...,
  "compilerOptions": {
    ...,
    "paths": {
      "@api/controllers": ["apps/api/src/controllers/index.ts"],
      "@api/decorators": ["apps/api/src/decorators/index.ts"],
      "@api/models": ["apps/api/src/models/index.ts"],
      "@api/routes": ["apps/api/src/routes/index.ts"],
      "@api/services": ["apps/api/src/services/index.ts"],
      "@api/validators": ["apps/api/src/validators/index.ts"],
      "@api/utils": ["apps/api/src/utils/index.ts"],
      "@api/middlewares": ["apps/api/src/middlewares/index.ts"],
    }
  }
}
```

3. COPY form REPO ./tools/mongoose-model to {project}/tools/mongoose-model
4. COPY form REPO ./tools/express-controller to {project}/tools/express-controller
5. COPY form REPO ./tools/express-decorator to {project}/tools/express-decorator
6. COPY form REPO ./tools/express-route to {project}/tools/express-route
7. COPY form REPO ./tools/express-validator to {project}/tools/express-validator
8. COPY form REPO ./tools/api-service to {project}/tools/api-service

##### API Interfaces

```
yarn nx generate @nrwl/workspace:library --name=api --directory=interfaces --unitTestRunner=none --no-interactive
```

#### API Constants

```
yarn nx generate @nrwl/workspace:library --name=api --directory=constants --unitTestRunner=none --no-interactive
```

```
yarn add jsrsasign
yarn add @types/jsrsasign -D
```

#### dnd

```
yarn add react-dnd react-dnd-html5-backend immutability-helper
```

[mdx Components](https://mdxjs.com/table-of-components/)

```
touch .env.local
```

> NX_MONGODB={url}
