import { DeleteItemsRequestData } from '@interfaces';
import { Model } from 'mongoose';

export const deleteManyItemsFromDBByID = async (controller: string, model: typeof Model, data: DeleteItemsRequestData) => {
  try {
    const { ids } = data;

    const deletedUsers = await model.find({
      _id: {
        $in: ids,
      },
    });
    const deletedEmails = deletedUsers.map((user) => user.email);

    await model.deleteMany({
      _id: {
        $in: ids,
      },
    });

    const message = `${deletedEmails.join(', ')} ${ids.length > 1 ? 'were' : 'was'} deleted!`;

    return message;
  } catch (error) {
    throw new Error(error.message);
  }
};
