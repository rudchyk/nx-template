import { Response } from 'express';
import { ApiErrorResponse, ApiSuccessResponse } from '@interfaces';

export const useControllerResponses = (res: Response, controller: string) => {
  const errorResponse = (error: any, status = 400) => {
    const errorResponseData: ApiErrorResponse = {
      controller,
      message: error.message,
    };

    console.error(controller, error.message, error);

    res.status(status).json(errorResponseData);
  };

  const successResponse = <T>(data: ApiSuccessResponse & T, status = 200) => {
    return res.status(status).json(data);
  };

  return {
    errorResponse,
    successResponse,
  };
};
