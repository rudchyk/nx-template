import { Model } from 'mongoose';

interface GetItemsFromDBParams {
  [key: string]: string;
}

export const getItemsFromDB = async <T>(model: typeof Model, params: GetItemsFromDBParams | undefined = {}): Promise<T[]> => {
  try {
    const items = await model.find(params);
    return items.map((item) => item.getItem());
  } catch (error) {
    throw new Error(error.message);
  }
};
