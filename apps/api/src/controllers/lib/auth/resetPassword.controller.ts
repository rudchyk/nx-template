import { Request, Response } from 'express';
import { UserModel } from '@api/models';
import * as Bcrypt from 'bcryptjs';
import { useControllerResponses } from '@api/utils';
import { ResetPasswordData, ResetPasswordAPIData } from '@interfaces';

interface RequestBody {
  password: string;
  email: string;
  decodedToken: ResetPasswordData;
}

export const resetPasswordController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'resetPasswordController');

  try {
    const { email, password } = req.body as RequestBody;
    const filter = { email };
    const update = {
      password: Bcrypt.hashSync(password),
    };
    const response = await UserModel.findOneAndUpdate(filter, update);

    return successResponse<ResetPasswordAPIData>({
      email: response.email,
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default resetPasswordController;
