import { Request, Response } from 'express';
import { UserModel, ConfigModel } from '@api/models';
import { SignUpData, AuthAPIData } from '@interfaces';
import { useControllerResponses } from '@api/utils';
import { UserRolesEnum, adminUsersList, ConfigEnum } from '@constants';

interface RequestBody {
  token: string;
  decodedToken: SignUpData;
}

export const registerUserController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'registerUserController');

  try {
    const {
      decodedToken: { firstName, lastName, email, password, remember, marketingPromotions },
    } = req.body as RequestBody;

    const getRole = () => (adminUsersList.includes(email) ? UserRolesEnum.ADMIN : UserRolesEnum.USER);

    if (marketingPromotions) {
      // TODO: Marketing Promotions
    }

    const user = await UserModel.create({
      firstName,
      lastName,
      email,
      password,
      role: getRole(),
    });
    await ConfigModel.create({
      userId: user._id,
      [ConfigEnum.ENABLED_FEATURES]: [],
    });

    return successResponse<AuthAPIData>({
      user: user.getItem(),
      token: user.generateToken(),
      remember,
    });
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default registerUserController;
