import { Request, Response } from 'express';
import { useControllerResponses } from '@api/utils';
import { AuthHeaderAPIData, AuthAPIData } from '@interfaces';
import { UserModel } from '@api/models';

interface RequestBody {
  decodedAuthorization: AuthHeaderAPIData;
}

export const authController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'loginUserController');

  try {
    const {
      decodedAuthorization: { email },
    } = req.body as RequestBody;

    const user = await UserModel.findOne({ email });

    if (!user) {
      throw new Error(`User with email: ${email} doesn't exist`);
    }

    return successResponse<AuthAPIData>({
      user: user.getItem(),
      token: user.generateToken(),
      remember: true,
    });
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default authController;
