import { Request, Response } from 'express';
import { useControllerResponses } from '@api/utils';
import { VerifyEmailAPIData } from '@interfaces';

export interface RequestBody {
  email: string;
}

export const forgotPasswordController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'forgotPasswordController');

  try {
    const { email } = req.body as RequestBody;

    return successResponse<VerifyEmailAPIData>({
      email,
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default forgotPasswordController;
