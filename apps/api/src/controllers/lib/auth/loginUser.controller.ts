import { Request, Response } from 'express';
import { UserSchemaInterface } from '@api/models';
import { useControllerResponses } from '@api/utils';
import { LogInFormInputs, AuthAPIData } from '@interfaces';

interface RequestBody {
  token: string;
  decodedToken: LogInFormInputs;
  user: UserSchemaInterface;
}

export const loginUserController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'loginUserController');

  try {
    const {
      user,
      decodedToken: { remember },
    } = req.body as RequestBody;

    return successResponse<AuthAPIData>({
      user: user.getItem(),
      token: user.generateToken(),
      remember,
    });
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default loginUserController;
