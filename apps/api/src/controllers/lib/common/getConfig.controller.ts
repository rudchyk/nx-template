import { Request, Response } from 'express';
import { useControllerResponses } from '@api/utils';
import { GetConfigResponseAPIData } from '@interfaces';
import { ConfigModel } from '@api/models';

export const getConfigController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'getConfigController');

  try {
    const { userId } = req.params;
    const config = await ConfigModel.findOne({ userId });

    return successResponse<GetConfigResponseAPIData>({
      config: config ? config.getConfig() : null,
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default getConfigController;
