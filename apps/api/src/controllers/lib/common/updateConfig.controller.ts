import { Request, Response } from 'express';
import { useControllerResponses } from '@api/utils';
import { AuthHeaderAPIData, ConfigData, UpdateConfigResponseAPIData } from '@interfaces';
import { ConfigModel } from '@api/models';

interface RequestBody {
  data: ConfigData;
  decodedAuthorization: AuthHeaderAPIData;
}

export const updateConfigController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'updateConfigController');

  try {
    const {
      data,
      decodedAuthorization: { email },
    } = req.body as RequestBody;
    const { userId } = req.params;
    const config = await ConfigModel.findOne({ userId });

    if (config) {
      await ConfigModel.findByIdAndUpdate(config._id, data);
    } else {
      await ConfigModel.create({
        userId,
        ...data,
      });
    }

    return successResponse<UpdateConfigResponseAPIData>({
      message: `The config for user ${email} is updated!`,
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default updateConfigController;
