import { Request, Response } from 'express';
import { UserModel } from '@api/models';
import { useControllerResponses, deleteManyItemsFromDBByID, getItemsFromDB } from '@api/utils';
import { User, DeleteItemsRequestData, DeleteUsersResponseAPIData } from '@interfaces';

export const deleteUsersController = async (req: Request, res: Response) => {
  const controllerName = 'deleteUsersController';
  const { errorResponse, successResponse } = useControllerResponses(res, controllerName);

  try {
    const result = await deleteManyItemsFromDBByID(controllerName, UserModel, req.body as DeleteItemsRequestData);

    return successResponse<DeleteUsersResponseAPIData>({
      message: result,
      users: await getItemsFromDB<User>(UserModel),
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default deleteUsersController;
