import { Request, Response } from 'express';
import { User } from '@interfaces';
import { UserModel } from '@api/models';
import { useControllerResponses, getItemsFromDB } from '@api/utils';
import { AddUserFormData, AddUserAPIData } from '@interfaces';

interface RequestBody {
  token: string;
  decodedToken: AddUserFormData;
}

export const addUsersController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'getUsersController');

  try {
    const {
      decodedToken: { firstName, lastName, email, password, role },
    } = req.body as RequestBody;

    // TODO: send password to the email

    await UserModel.create({
      firstName,
      lastName,
      email,
      password,
      role,
    });

    return successResponse<AddUserAPIData>({
      users: await getItemsFromDB<User>(UserModel),
      message: `${email} was added!`,
    });
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default addUsersController;
