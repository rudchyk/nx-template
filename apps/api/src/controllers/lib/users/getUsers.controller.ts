import { Request, Response } from 'express';
import { UsersResponseAPIData, User } from '@interfaces';
import { UserModel } from '@api/models';
import { useControllerResponses, getItemsFromDB } from '@api/utils';

export const getUsersController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'getUsersController');

  try {
    return successResponse<UsersResponseAPIData>({
      users: await getItemsFromDB<User>(UserModel),
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default getUsersController;
