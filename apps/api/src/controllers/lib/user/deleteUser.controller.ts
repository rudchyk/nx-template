import { Request, Response } from 'express';
import { UserModel } from '@api/models';
import { DeleteUserResponseAPIData } from '@interfaces';
import { useControllerResponses } from '@api/utils';

export const deleteUserController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'getUserController');

  try {
    const { id } = req.params;
    const deletedUser = await UserModel.findByIdAndDelete(id);

    return successResponse<DeleteUserResponseAPIData>({
      message: `${deletedUser.email} was deleted!`,
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default deleteUserController;
