import { Request, Response } from 'express';
import { UserModel } from '@api/models';
import { UpdateUserResponseAPIData } from '@interfaces';
import { useControllerResponses } from '@api/utils';

export const updateUserController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'updateUserController');

  try {
    const { id } = req.params;
    const { data } = req.body;
    const update = {
      ...data,
      updatedAt: new Date(),
    };

    await UserModel.findByIdAndUpdate(id, update);

    const user = await UserModel.findById(id);

    return successResponse<UpdateUserResponseAPIData>({
      message: `${user.email} was updated!`,
      user: user.getItem(),
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default updateUserController;
