import { Request, Response } from 'express';
import { UserModel } from '@api/models';
import { ChangeUserPasswordResponseAPIData, ChangePasswordFormData, AuthHeaderAPIData } from '@interfaces';
import { useControllerResponses } from '@api/utils';
import * as Bcrypt from 'bcryptjs';

interface RequestBody {
  token: string;
  decodedToken: ChangePasswordFormData;
  decodedAuthorization: AuthHeaderAPIData;
  password: string;
  newPassword: string;
  email: string;
}

export const changeUserPasswordController = async (req: Request, res: Response) => {
  const { errorResponse, successResponse } = useControllerResponses(res, 'changeUserPasswordController');

  try {
    const { id } = req.params;
    const { newPassword } = req.body as RequestBody;
    const update = {
      password: Bcrypt.hashSync(newPassword),
      updatedAt: new Date(),
    };

    const user = await UserModel.findByIdAndUpdate(id, update);

    return successResponse<ChangeUserPasswordResponseAPIData>({
      message: `Password for ${user.email} was updated!`,
      user: user.getItem(),
    });
  } catch (error) {
    return errorResponse(error);
  }
};

export default changeUserPasswordController;
