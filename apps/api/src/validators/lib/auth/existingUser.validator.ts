import { Request, Response, NextFunction } from 'express';
import { UserModel } from '@api/models';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  email: string;
}

export const existingUserValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'existingUserValidator');

  try {
    const { email } = req.body as RequestBody;
    const user = await UserModel.findOne({ email });

    if (user) {
      throw new Error(`An account with email ${email} already exists`);
    }

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default existingUserValidator;
