import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { useControllerResponses } from '@api/utils';
import { ResetPasswordData } from '@interfaces';
import { ResetPasswordFormInputsEnum } from '@constants';

interface RequestBody {
  decodedToken: ResetPasswordData;
}

const schema = yup.object().shape({
  [ResetPasswordFormInputsEnum.EMAIL]: yup.string().email().required(),
  [ResetPasswordFormInputsEnum.PASSWORD]: yup.string().required(),
});

export const resetPasswordDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'resetPasswordDataValidator');

  try {
    const { decodedToken } = req.body as RequestBody;

    await schema.validate(decodedToken);

    const { email, password } = decodedToken;

    req.body.email = email;
    req.body.password = password;

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default resetPasswordDataValidator;
