import { Request, Response, NextFunction } from 'express';
import { UserSchemaInterface } from '@api/models';
import { useControllerResponses } from '@api/utils';
import { LogInFormInputs } from '@interfaces';

interface RequestBody {
  password: string;
  email: string;
  user: UserSchemaInterface;
}

export const userPasswordValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'userPasswordValidator');

  try {
    const { user, email, password } = req.body as RequestBody;

    if (!user.comparePasswords(password)) {
      throw new Error(`Password for user: ${email} is invalid`);
    }

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default userPasswordValidator;
