import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { useControllerResponses } from '@api/utils';
import { LogInFormInputs } from '@interfaces';
import { LogInFormInputsEnum } from '@constants';

interface RequestBody {
  token: string;
  decodedToken: LogInFormInputs;
}

const schema = yup.object().shape({
  [LogInFormInputsEnum.EMAIL]: yup.string().email().required(),
  [LogInFormInputsEnum.PASSWORD]: yup.string().min(4).required(),
  [LogInFormInputsEnum.REMEMBER]: yup.boolean(),
});

export const loginDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'loginDataValidator');

  try {
    const { decodedToken } = req.body as RequestBody;

    await schema.validate(decodedToken);

    const { email, password } = decodedToken;

    req.body.email = email;
    req.body.password = password;

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default loginDataValidator;
