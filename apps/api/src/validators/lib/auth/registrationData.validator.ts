import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { SignUpData } from '@interfaces';
import { SignUpFormInputsEnum } from '@constants';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  token: string;
  decodedToken: SignUpData;
}

const schema = yup.object().shape({
  [SignUpFormInputsEnum.FIRST_NAME]: yup.string().required(),
  [SignUpFormInputsEnum.LAST_NAME]: yup.string(),
  [SignUpFormInputsEnum.EMAIL]: yup.string().required().email(),
  [SignUpFormInputsEnum.PASSWORD]: yup.string().required().min(4),
  [SignUpFormInputsEnum.REMEMBER]: yup.boolean().default(true),
  [SignUpFormInputsEnum.MARKETING_PROMOTIONS]: yup.boolean(),
});

export const registrationDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'registrationDataValidator');

  try {
    const { decodedToken } = req.body as RequestBody;

    await schema.validate(decodedToken);

    req.body.email = decodedToken.email;

    return next();
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default registrationDataValidator;
