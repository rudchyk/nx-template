import { Request, Response, NextFunction } from 'express';
import * as yup from 'yup';
import * as jwt from 'jsonwebtoken';
import { s } from '@constants';
import { useControllerResponses } from '@api/utils';

interface RequestHeaders {
  authorization: string;
}

const schema = yup.object().shape({
  authorization: yup.string().required(),
});

export const authorizationValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'authorizationValidator');

  try {
    const { authorization } = req.headers as RequestHeaders;

    await schema.validate({ authorization });

    req.body.decodedAuthorization = await jwt.verify(authorization, s.api);

    return next();
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default authorizationValidator;
