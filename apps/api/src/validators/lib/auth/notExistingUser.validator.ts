import { Request, Response, NextFunction } from 'express';
import { UserModel } from '@api/models';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  email: string;
}

export const notExistingUserValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'notExistingUserValidator');

  try {
    const { email } = req.body as RequestBody;
    const user = await UserModel.findOne({ email });

    if (!user) {
      throw new Error(`User with email: ${email} doesn't exist`);
    }

    req.body.user = user;

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default notExistingUserValidator;
