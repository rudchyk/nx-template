import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { VerifyEmailFormInputsEnum } from '@constants';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  [VerifyEmailFormInputsEnum.EMAIL]: string;
}

const schema = yup.object().shape({
  [VerifyEmailFormInputsEnum.EMAIL]: yup.string().email().required(),
});

export const forgotPasswordDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'forgotPasswordDataValidator');

  try {
    const { email } = req.body as RequestBody;

    await schema.validate({ email });

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default forgotPasswordDataValidator;
