import { Request, Response, NextFunction } from 'express';
import { useControllerResponses } from '@api/utils';
import { AuthHeaderAPIData } from '@interfaces';
import { UserRolesEnum } from '@constants';

interface RequestBody {
  decodedAuthorization: AuthHeaderAPIData;
}

export const adminUserValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'adminUserValidator');

  try {
    const {
      decodedAuthorization: { role },
    } = req.body as RequestBody;

    if (role !== UserRolesEnum.ADMIN) {
      throw new Error('User is not admin, access denied!');
    }

    return next();
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default adminUserValidator;
