import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { AddUserFormData } from '@interfaces';
import { AddUserFormInputsEnum } from '@constants';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  token: string;
  decodedToken: AddUserFormData;
}

const schema = yup.object().shape({
  [AddUserFormInputsEnum.FIRST_NAME]: yup.string().required(),
  [AddUserFormInputsEnum.LAST_NAME]: yup.string(),
  [AddUserFormInputsEnum.EMAIL]: yup.string().required().email(),
  [AddUserFormInputsEnum.PASSWORD]: yup.string().required().min(4),
  [AddUserFormInputsEnum.ROLE]: yup.string().required(),
});

export const addUserDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'addUserDataValidator');

  try {
    const { decodedToken } = req.body as RequestBody;

    await schema.validate(decodedToken);

    req.body.email = decodedToken.email;

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default addUserDataValidator;
