import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { useControllerResponses } from '@api/utils';
import { AuthHeaderAPIData } from '@interfaces';

interface RequestBody {
  decodedAuthorization: AuthHeaderAPIData;
  data: any;
}

export const updateUserDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'updateUserDataValidator');

  try {
    const {
      data,
      decodedAuthorization: { email },
    } = req.body as RequestBody;

    if (!data) {
      throw new Error(`Data is required to update user ${email}`);
    }

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default updateUserDataValidator;
