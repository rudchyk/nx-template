import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { useControllerResponses } from '@api/utils';
import { ChangePasswordFormData, AuthHeaderAPIData } from '@interfaces';
import { ChangePasswordFormInputsEnum } from '@constants';
import { UserModel } from '@api/models';

interface RequestBody {
  token: string;
  decodedToken: ChangePasswordFormData;
  decodedAuthorization: AuthHeaderAPIData;
}

const schema = yup.object().shape({
  [ChangePasswordFormInputsEnum.OPD_PASSWORD]: yup.string().required().min(4),
  [ChangePasswordFormInputsEnum.NEW_PASSWORD]: yup.string().required().min(4),
});

export const changeUserPasswordDataValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'changeUserPasswordDataValidator');

  try {
    const { id } = req.params;
    const {
      decodedToken,
      decodedAuthorization: { email },
    } = req.body as RequestBody;

    await schema.validate(decodedToken);

    const user = await UserModel.findById(id);

    req.body.email = email;
    req.body.password = decodedToken.oldPassword;
    req.body.newPassword = decodedToken.password;
    req.body.user = user;

    return next();
  } catch (error) {
    return errorResponse(error);
  }
};

export default changeUserPasswordDataValidator;
