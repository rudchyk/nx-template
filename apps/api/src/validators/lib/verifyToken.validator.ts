import * as yup from 'yup';
import { Request, Response, NextFunction } from 'express';
import { s } from '@constants';
import * as jwt from 'jsonwebtoken';
import { useControllerResponses } from '@api/utils';

interface RequestBody {
  token: string;
}

const tokenSchema = yup.object().shape({
  token: yup.string().required(),
});

export const verifyTokenValidator = async (req: Request, res: Response, next: NextFunction) => {
  const { errorResponse } = useControllerResponses(res, 'verifyTokenValidator');

  try {
    const { token } = req.body as RequestBody;

    await tokenSchema.validate({ token });

    req.body.decodedToken = await jwt.verify(token, s.client);

    return next();
  } catch (error) {
    return errorResponse(error, 401);
  }
};

export default verifyTokenValidator;
