import { router } from '../router';
import { APIRoutesEnum } from '@constants';

router.get(APIRoutesEnum.API, (req, res) => {
  res.send({ message: 'Welcome to api!' });
});
