import { APIRoutesEnum } from '@constants';
import { getConfigController, updateConfigController } from '@api/controllers';
import { withAuthDecorator } from '@api/decorators';
import { router } from '../router';

router.get(APIRoutesEnum.CONFIG, withAuthDecorator([getConfigController]));

router.put(APIRoutesEnum.CONFIG, withAuthDecorator([updateConfigController]));
