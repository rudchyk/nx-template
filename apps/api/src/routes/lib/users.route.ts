import { router } from '../router';
import { APIRoutesEnum } from '@constants';
import {
  getUsersController,
  deleteUsersController,
  deleteUserController,
  updateUserController,
  addUsersController,
  changeUserPasswordController,
} from '@api/controllers';
import { withAdminDecorator, withAuthDecorator } from '@api/decorators';
import {
  addUserDataValidator,
  verifyTokenValidator,
  existingUserValidator,
  updateUserDataValidator,
  userPasswordValidator,
  changeUserPasswordDataValidator,
} from '@api/validators';

router.get(APIRoutesEnum.USERS, withAdminDecorator([getUsersController]));

router.delete(APIRoutesEnum.USERS, withAdminDecorator([deleteUsersController]));

router.post(APIRoutesEnum.USERS, withAdminDecorator([verifyTokenValidator, addUserDataValidator, existingUserValidator, addUsersController]));

router.delete(APIRoutesEnum.USER, withAuthDecorator([deleteUserController]));

router.put(APIRoutesEnum.USER, withAuthDecorator([updateUserDataValidator, updateUserController]));

router.post(
  APIRoutesEnum.USER_CHANGE_PASSWORD,
  withAuthDecorator([verifyTokenValidator, changeUserPasswordDataValidator, userPasswordValidator, changeUserPasswordController])
);
