import { router } from '../router';
import { APIRoutesEnum } from '@constants';
import {
  verifyTokenValidator,
  registrationDataValidator,
  existingUserValidator,
  loginDataValidator,
  notExistingUserValidator,
  userPasswordValidator,
  forgotPasswordDataValidator,
  resetPasswordDataValidator,
} from '@api/validators';
import { registerUserController, loginUserController, resetPasswordController, forgotPasswordController, authController } from '@api/controllers';
import { withAuthDecorator } from '@api/decorators';

router.post(APIRoutesEnum.AUTH, withAuthDecorator([authController]));

router.post(APIRoutesEnum.REGISTER, verifyTokenValidator, registrationDataValidator, existingUserValidator, registerUserController);

router.post(APIRoutesEnum.LOGIN, verifyTokenValidator, loginDataValidator, notExistingUserValidator, userPasswordValidator, loginUserController);

router.post(APIRoutesEnum.RESET_PASSWORD, verifyTokenValidator, resetPasswordDataValidator, resetPasswordController);

router.post(APIRoutesEnum.FORGOT_PASSWORD, forgotPasswordDataValidator, notExistingUserValidator, forgotPasswordController);
