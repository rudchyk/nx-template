import { router } from './router';

import './lib/api.route';
import './lib/auth.route';
import './lib/users.route';
import './lib/config.route';
// import

export default router;
