import { Request, Response, NextFunction } from 'express';
import * as express from 'express';
import * as mongoose from 'mongoose';
import * as path from 'path';
import routes from './routes';

const app = express();
const clientPath = path.join(__dirname, 'client');

mongoose.connect(process.env.NX_MONGODB);

app.use(express.static(clientPath));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(routes);

app.use((req: Request, res: Response) => {
  console.error('catch 404 and forward to error handler');
  res.sendFile(path.join(clientPath, 'index.html'));
});

app.use((err, req: Request, res: Response, next: NextFunction) => {
  const statusCode = err.statusCode || 500;
  console.error('Error handler middleware', err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
});

const port = process.env.port || 3000;
const server = app.listen(port);

server.on('error', console.error);
server.on('listening', () => {
  const addr = server.address();
  console.log('addr', addr);

  if (addr instanceof Object) {
    console.log(`Listening at http://${addr.address === '::' ? 'localhost' : addr.address}:${addr.port}`);
  } else if (typeof addr === 'string') {
    console.log(`Listening at http://${addr}:${port}`);
  } else {
    console.log(`Listening at http://localhost:${port}`);
  }
});
