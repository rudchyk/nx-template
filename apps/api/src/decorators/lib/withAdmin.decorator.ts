import { MethodController } from '@interfaces';
import { authorizationValidator, adminUserValidator } from '@api/validators';

export const withAdminDecorator = (controllers: MethodController[]) => [authorizationValidator, adminUserValidator, ...controllers];
