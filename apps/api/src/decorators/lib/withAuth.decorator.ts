import { MethodController } from '@interfaces';
import { authorizationValidator } from '@api/validators';

export const withAuthDecorator = (controllers: MethodController[]) => [authorizationValidator, ...controllers];
