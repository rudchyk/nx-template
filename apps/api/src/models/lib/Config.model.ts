import { Document, Schema, model, Model } from 'mongoose';
import { ConfigEnum } from '@constants';
import { ConfigData } from '@interfaces';

export interface ConfigSchemaInterface extends Document, ConfigData {
  userId: string;
  getConfig(): any;
}

type ConfigModelInterface = Model<ConfigSchemaInterface>;

const ConfigSchema = new Schema<ConfigSchemaInterface, ConfigModelInterface>({
  userId: String,
  [ConfigEnum.ENABLED_FEATURES]: [String],
});

ConfigSchema.methods.getConfig = function () {
  const schemaObj = this.toObject();

  schemaObj.id = schemaObj._id;
  delete schemaObj._id;
  delete schemaObj.id;
  delete schemaObj.userId;
  delete schemaObj.__v;

  return schemaObj;
};

export const ConfigModel = model<ConfigSchemaInterface, ConfigModelInterface>('Config', ConfigSchema);

export default ConfigModel;
