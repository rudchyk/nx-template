import { Document, Schema, model, Model } from 'mongoose';
import * as jwt from 'jsonwebtoken';
import { s } from '@constants';
import * as Bcrypt from 'bcryptjs';
import { UserData, User } from '@interfaces';

export interface UserSchemaInterface extends Document, UserData {
  createdAt: Date;
  updatedAt: Date;
  getItem(): User;
  comparePasswords(plainPassword: string): boolean;
  generateToken(): string;
}

type UserModelInterface = Model<UserSchemaInterface>;

const UserSchema = new Schema<UserSchemaInterface, UserModelInterface>({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  phone: String,
  dob: Date,
  role: String,
  createdAt: Date,
  updatedAt: Date,
  emailConfirmedAt: Date,
  emailConfirmedCode: String,
});

UserSchema.pre('save', function () {
  this.password = Bcrypt.hashSync(this.password);
  this.createdAt = new Date();
  this.phone = '';
  this.dob = null;
});

UserSchema.methods.getItem = function () {
  const schemaObj = this.toObject();

  schemaObj.id = schemaObj._id;

  delete schemaObj._id;
  delete schemaObj.__v;
  delete schemaObj.emailConfirmedCode;
  delete schemaObj.password;

  return schemaObj;
};

UserSchema.methods.comparePasswords = function (plainPassword: string) {
  return Bcrypt.compareSync(plainPassword, this.password);
};

UserSchema.methods.generateToken = function () {
  return jwt.sign(
    {
      email: this.email,
      id: this._id,
      role: this.role,
    },
    s.api
  );
};

export const UserModel = model<UserSchemaInterface, UserModelInterface>('User', UserSchema);

export default UserModel;
