import { useTranslation } from 'react-i18next';
import { Default } from '@client/layouts';
import { Grid, Stack } from '@mui/material';
import { Page } from '@client/templates';
import { ErrorBoundary, GlobalError } from './sections';

export const Playground = () => {
  const { t } = useTranslation();

  return (
    <Default>
      <Page title={t('Playground')}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Stack direction="row" spacing={2}>
              <ErrorBoundary />
              <GlobalError />
            </Stack>
          </Grid>
        </Grid>
      </Page>
    </Default>
  );
};

export default Playground;
