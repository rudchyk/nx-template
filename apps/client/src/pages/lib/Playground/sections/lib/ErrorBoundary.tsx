import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mui/material/Button';
import ErrorIcon from '@mui/icons-material/Error';

const Bomb = () => {
  const { t } = useTranslation();
  throw new Error(t('caboom'));
};

export const ErrorBoundary = () => {
  const { t } = useTranslation();
  const [explode, setExplode] = useState(false);
  const onExplode = () => setExplode(!explode);

  return (
    <>
      <Button variant="contained" color="error" onClick={onExplode} startIcon={<ErrorIcon />}>
        {t('testErrorFallback')}
      </Button>
      {explode ? <Bomb /> : null}
    </>
  );
};

export default ErrorBoundary;
