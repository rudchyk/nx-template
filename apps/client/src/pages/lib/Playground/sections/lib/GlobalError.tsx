import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mui/material/Button';
import ErrorIcon from '@mui/icons-material/Error';
import axios, { AxiosError } from 'axios';
import { useDispatch } from 'react-redux';
import { setActiveError } from '@client/reducers';

export const GlobalError = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const onClick = async () => {
    try {
      await axios.get('/sdfsdfsdf');
    } catch (error) {
      const err = error as AxiosError;
      const { status, stack, message, name }: any = err.toJSON();

      dispatch(setActiveError({ status, message, stack, name }));
    }
  };

  return (
    <Button variant="contained" color="warning" onClick={onClick} startIcon={<ErrorIcon />}>
      {t('Test Global Error')}
    </Button>
  );
};

export default GlobalError;
