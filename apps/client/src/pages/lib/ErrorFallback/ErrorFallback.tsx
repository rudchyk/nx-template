import { useTranslation } from 'react-i18next';
import { Simple } from '@client/layouts';
import { UIPageTitle, UIButton, UICodeBlock } from '@ui';

export interface ErrorFallbackProps {
  error: Error;
  resetErrorBoundary: any;
}

export const ErrorFallback = ({ error, resetErrorBoundary }: ErrorFallbackProps) => {
  const { t } = useTranslation();

  return (
    <Simple>
      <UIPageTitle>{t('AppErrorTitle')}</UIPageTitle>
      <h3>{t('errorMessage')}:</h3>
      <UICodeBlock code={error.message} language="html" />
      <h3>{t('errorStack')}:</h3>
      <UICodeBlock code={error.stack || ''} />
      <UIButton variant="contained" onClick={resetErrorBoundary}>
        {t('tryAgain')}
      </UIButton>
    </Simple>
  );
};

export default ErrorFallback;
