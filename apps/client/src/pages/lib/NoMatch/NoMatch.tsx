import { useTranslation } from 'react-i18next';
import { Default } from '@client/layouts';
import { Page } from '@client/templates';
import { UIFlexBox } from '@ui';
import { HomeButton } from '@client/components';
import { Typography } from '@mui/material';

export const NoMatch = () => {
  const { t } = useTranslation();

  return (
    <Default>
      <Page title={t('404Title')}>
        <Typography variant="h1" sx={{ fontSize: '220px' }} component="p" align="center">
          404
        </Typography>
        <UIFlexBox>
          <HomeButton />
        </UIFlexBox>
      </Page>
    </Default>
  );
};

export default NoMatch;
