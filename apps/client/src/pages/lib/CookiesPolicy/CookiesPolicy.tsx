import { useTranslation } from 'react-i18next';
import { Default } from '@client/layouts';
import { Page } from '@client/templates';

export const CookiesPolicy = () => {
  const { t } = useTranslation();

  return (
    <Default>
      <Page title={t('Cookies Policy')}>
        <section>{t('Cookies Policy Page Content')}</section>
      </Page>
    </Default>
  );
};

export default CookiesPolicy;
