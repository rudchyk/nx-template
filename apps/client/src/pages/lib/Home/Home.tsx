import { useTranslation } from 'react-i18next';
import { Default } from '@client/layouts';
import { UIPageTitle } from '@ui';
import { TwitterSvg } from '@svg';
import { Page } from '@client/templates';
import styles from './Home.module.sass';

export const Home = () => {
  const { t } = useTranslation();
  const homeTitle = (
    <>
      <TwitterSvg id="hellooo" fill="green" className={styles.icon} /> {t('welcome')}
    </>
  );

  return (
    <Default>
      <Page title={homeTitle}>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Illum eaque doloribus velit modi vel suscipit saepe culpa maxime odit alias, quam, ducimus
        beatae dolores omnis quas assumenda eos ipsa quisquam. Eligendi autem natus, magnam deleniti nisi numquam fuga libero voluptatum illo magni molestias
        earum expedita. Molestias tempora labore dolorum aspernatur earum! Tempore possimus repellendus incidunt facere dolorem magni totam earum recusandae
        quidem commodi, doloremque, debitis placeat repudiandae illo corrupti voluptate illum enim similique non expedita! Fugiat facere sint nihil quos, atque
        numquam eum assumenda. Neque totam maxime, deserunt porro aspernatur recusandae maiores eligendi, magnam corporis, praesentium laboriosam? Hic, rem
        repellat.
      </Page>
    </Default>
  );
};

export default Home;
