import { Default } from '@client/layouts';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { Page } from '@client/templates';
import { FeatureTogglesSwitcher } from '@client/modules';

export const Settings = () => {
  const { t } = useTranslation();

  return (
    <Default>
      <Page title={t('Settings')}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FeatureTogglesSwitcher />
          </Grid>
        </Grid>
      </Page>
    </Default>
  );
};

export default Settings;
