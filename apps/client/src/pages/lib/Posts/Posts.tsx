import { useSelector, useDispatch } from 'react-redux';
import { ChangeEvent, useState } from 'react';
import { selectPostsState, fetchPosts, clearPosts } from '@client/reducers';
import { postsApiDefaultLimit } from '@client/services';
import { Default } from '@client/layouts';
import { UILoading, UIButton } from '@ui';
import { useTranslation } from 'react-i18next';
import Alert from '@mui/material/Alert';
import { TextField } from '@mui/material';
import { Page } from '@client/templates';

export const Posts = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [limit, setLimit] = useState(postsApiDefaultLimit);
  const { isPostsLoading, isPostsLoaded, posts, postsErrorMessage } = useSelector(selectPostsState);
  const onReloadPosts = async () => {
    if (posts?.length) {
      await dispatch(clearPosts());
    }
    await dispatch(fetchPosts(limit));
  };
  const onLimitChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    setLimit(+value);
  };
  const panel = (
    <>
      <TextField fullWidth size="small" margin="none" id="numberOfPosts" label={t('numberOfPosts')} type="number" value={limit} onInput={onLimitChange} />
      <UIButton variant="contained" sx={{ flexShrink: 0, ml: 2 }} loading={isPostsLoading} onClick={onReloadPosts}>
        {t('loadPosts')}
      </UIButton>
    </>
  );

  return (
    <Default>
      <Page title={t('Posts')} panel={panel}>
        {isPostsLoading && <UILoading />}
        {postsErrorMessage && (
          <Alert sx={{ my: 1 }} severity="error">
            {postsErrorMessage}
          </Alert>
        )}
        {isPostsLoaded && (
          <section>
            {posts?.map(({ title, body, id }) => (
              <article key={id}>
                <h2>
                  {id}) {title}
                </h2>
                <p>{body}</p>
              </article>
            ))}
          </section>
        )}
      </Page>
    </Default>
  );
};

export default Posts;
