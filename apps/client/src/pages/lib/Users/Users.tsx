import { Outlet } from 'react-router-dom';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { fetchUsers, clearUsers, cleanUsersErrorMessages } from '@client/reducers';

export const Users = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    console.log('Users Page mounted');

    dispatch(fetchUsers());
    return () => {
      console.log('Users Page distroed');
      dispatch(cleanUsersErrorMessages());
      dispatch(clearUsers());
    };
  }, []);

  return <Outlet />;
};

export default Users;
