import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import { Default } from '@client/layouts';
import { Page } from '@client/templates';
import { UIButton, UIFlexBox, UIInputCopyeditor, UIFormTextField } from '@ui';
import { selectUsersById } from '@client/reducers';
import moment from 'moment';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { ClientRoutesEnum } from '@constants';
import EventIcon from '@mui/icons-material/Event';
import EventRepeatIcon from '@mui/icons-material/EventRepeat';
import { Avatar, Fab, Grid, ListItemAvatar, ListItemText } from '@mui/material';
import { getFirstLetter, getDate } from '@utils';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';

export const User = () => {
  const { t } = useTranslation();
  const params = useParams();
  const user = useSelector(selectUsersById(params.userId));

  if (!user) {
    return null;
  }

  return (
    <Default>
      <Page
        title={`${user.firstName} ${user.lastName}`}
        titleJustify="start"
        flexDirection="row-reverse"
        titleActions={
          <UIButton icon to={ClientRoutesEnum.USERS} sx={{ mr: 2 }} size="large" color="primary">
            <ArrowBackIosNewIcon />
          </UIButton>
        }>
        <Grid container spacing={2}>
          <Grid item xs={3}>
            <UIFlexBox height={200}>
              <Avatar sx={{ width: 160, height: 160, fontSize: 90 }}>{getFirstLetter(user?.firstName)}</Avatar>
            </UIFlexBox>
          </Grid>
          <Grid item xs={9}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <List>
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar>
                        <EventIcon />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText>
                      {t('Registration date')}: <strong>{getDate(user.createdAt)}</strong>
                    </ListItemText>
                  </ListItem>
                  {user?.updatedAt && (
                    <ListItem>
                      <ListItemAvatar>
                        <Avatar>
                          <EventRepeatIcon />
                        </Avatar>
                      </ListItemAvatar>
                      <ListItemText>
                        {t('Updated at')}: <strong>{getDate(user.updatedAt)}</strong>
                      </ListItemText>
                    </ListItem>
                  )}
                </List>
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="ID" fullWidth label={t('ID')} defaultValue={user.id} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="role" fullWidth label={t('Role')} defaultValue={user.role} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="firstName" fullWidth label={t('First Name')} defaultValue={user.firstName} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="lastName" fullWidth label={t('Last Name')} defaultValue={user.lastName} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="email" fullWidth label={t('Email Address')} defaultValue={user.email} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                {user?.phone && (
                  <UIInputCopyeditor readOnly>
                    <UIFormTextField margin="none" fieldKey="phone" fullWidth label={t('Phone')} defaultValue={user.phone} />
                  </UIInputCopyeditor>
                )}
              </Grid>
              <Grid item xs={6}>
                {user?.dob && (
                  <UIInputCopyeditor readOnly>
                    <UIFormTextField margin="none" fieldKey="dob" fullWidth label={t('Date of birth')} defaultValue={moment(user.dob).format('DD/MM/YYYY')} />
                  </UIInputCopyeditor>
                )}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Page>
    </Default>
  );
};

export default User;
