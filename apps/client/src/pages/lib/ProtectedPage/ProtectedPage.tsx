import { useTranslation } from 'react-i18next';
import { Default } from '@client/layouts';
import { Page } from '@client/templates';

export const ProtectedPage = () => {
  const { t } = useTranslation();

  return (
    <Default>
      <Page title={t('Protected Page')}>
        <section>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse aspernatur aliquam a quaerat repellat doloribus voluptatem culpa quo ex sint tempora
            excepturi, sed soluta laborum officia repudiandae laudantium, voluptates impedit ipsam? Harum commodi dicta non incidunt asperiores consequuntur
            exercitationem hic corporis tenetur, aliquid esse minima a quam voluptatibus ea, facilis iste velit blanditiis quod! Quisquam, corrupti! Earum,
            sequi aliquam voluptatem ipsam fuga exercitationem, ducimus quibusdam iste praesentium cupiditate odio sit eius reprehenderit? Facilis recusandae
            ducimus delectus? Quis et eius autem deserunt quae eos dolorem nemo explicabo dicta ex! Reiciendis, ea adipisci. Aut officia dignissimos repellat
            modi accusantium, expedita quaerat adipisci.
          </p>
          <p>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias illum magnam explicabo dolorem quia rerum excepturi ducimus. Cum asperiores pariatur
            odio, eum atque veniam corporis! Dolorum deleniti cupiditate esse ullam tempore in corrupti impedit modi non omnis? Officiis nisi earum rem vel quam
            qui adipisci voluptatum soluta reiciendis animi explicabo accusamus, dolore recusandae in dolorem aperiam unde neque enim atque hic pariatur velit.
            Unde perspiciatis eos tempora, veniam saepe voluptatibus autem praesentium ipsam magni? Iure reiciendis quos voluptate minus illum voluptates
            provident tempore expedita, in itaque suscipit aliquam iusto facilis, ullam odio explicabo mollitia possimus voluptatum soluta deleniti fugit sit
            tenetur? Laudantium placeat porro, impedit ipsum voluptatum rerum sequi assumenda ducimus nam voluptatem est quia id aliquid tempora repudiandae
            nobis maiores dolores nostrum explicabo debitis similique magni voluptatibus! Accusamus, officia error iste hic obcaecati nesciunt non itaque
            cupiditate. Vel, inventore?
          </p>
        </section>
      </Page>
    </Default>
  );
};

export default ProtectedPage;
