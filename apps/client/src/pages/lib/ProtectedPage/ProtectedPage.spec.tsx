import { render } from '@testing-library/react';
import ProtectedPage from './ProtectedPage';

describe('ProtectedPage', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ProtectedPage />);

    expect(baseElement).toBeTruthy();
  });
});
