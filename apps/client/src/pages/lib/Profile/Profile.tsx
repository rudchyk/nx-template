import { Default } from '@client/layouts';
import { useTranslation } from 'react-i18next';
import { Alert, Avatar, Grid, ListItemAvatar, ListItemText } from '@mui/material';
import { Page } from '@client/templates';
import { UIFlexBox, UIInputCopyeditor, UIFormTextField } from '@ui';
import { getFirstLetter, getDate } from '@utils';
import { useSelector } from 'react-redux';
import { selectUserState } from '@client/reducers';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Grid3x3Icon from '@mui/icons-material/Grid3x3';
import SupervisorAccountIcon from '@mui/icons-material/SupervisorAccount';
import EventIcon from '@mui/icons-material/Event';
import EventRepeatIcon from '@mui/icons-material/EventRepeat';
import { DeleteUser, ChangeUserPassword, UpdateProfile } from '@client/modules';

export const Profile = () => {
  const { t } = useTranslation();
  const { user } = useSelector(selectUserState);
  const infoList1: any[] = [
    {
      key: 'id',
      icon: <Grid3x3Icon />,
      text: (
        <>
          {t('ID')}: <strong>{user?.id}</strong>
        </>
      ),
    },
    {
      key: 'role',
      icon: <SupervisorAccountIcon />,
      text: (
        <>
          {t('Role')}: <strong>{user?.role}</strong>
        </>
      ),
    },
  ];
  const infoList2: any[] = [
    {
      key: 'createdAt',
      icon: <EventIcon />,
      text: (
        <>
          {t('Registration date')}: <strong>{getDate(user?.createdAt)}</strong>
        </>
      ),
    },
    {
      isDisabled: !user?.updatedAt,
      key: 'updatedAt',
      icon: <EventRepeatIcon />,
      text: (
        <>
          {t('Updated at')}: <strong>{getDate(user?.updatedAt)}</strong>
        </>
      ),
    },
  ];

  return (
    <Default>
      <Page title={t('Profile')} titleActions={<DeleteUser userId={user?.id} />}>
        <Grid container spacing={2}>
          <Grid item xs={3}>
            <UIFlexBox height={200}>
              <Avatar sx={{ width: 160, height: 160, fontSize: 90 }}>{getFirstLetter(user?.firstName)}</Avatar>
            </UIFlexBox>
          </Grid>
          <Grid item xs={9}>
            <Grid container spacing={2}>
              <Grid item xs={6}>
                <List>
                  {infoList1.map(
                    ({ key, icon, text, isDisabled = false }) =>
                      !isDisabled && (
                        <ListItem key={key}>
                          {icon && (
                            <ListItemAvatar>
                              <Avatar>{icon}</Avatar>
                            </ListItemAvatar>
                          )}
                          <ListItemText primary={text} />
                        </ListItem>
                      )
                  )}
                </List>
              </Grid>
              <Grid item xs={6}>
                <List>
                  {infoList2.map(
                    ({ key, icon, text, isDisabled = false }) =>
                      !isDisabled && (
                        <ListItem key={key}>
                          {icon && (
                            <ListItemAvatar>
                              <Avatar>{icon}</Avatar>
                            </ListItemAvatar>
                          )}
                          <ListItemText primary={text} />
                        </ListItem>
                      )
                  )}
                </List>
              </Grid>
              <Grid sx={{ textAlign: 'right' }} item xs={12}>
                {user && <ChangeUserPassword userId={user.id} />}
              </Grid>
              <Grid item xs={6}>
                <UIInputCopyeditor readOnly>
                  <UIFormTextField margin="none" fieldKey="email" fullWidth label={t('Email Address')} value={user?.email} />
                </UIInputCopyeditor>
              </Grid>
              <Grid item xs={6}>
                <UIFlexBox jc="s" width="100%">
                  <Alert sx={{ width: '100%' }} severity={user?.emailConfirmedAt ? 'success' : 'error'}>
                    {user?.emailConfirmedAt ? t('The email is verified') : t('The email is not verified')}
                  </Alert>
                </UIFlexBox>
              </Grid>
              <Grid item xs={12}>
                {user && <UpdateProfile />}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Page>
    </Default>
  );
};

export default Profile;
