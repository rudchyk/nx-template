import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Admin } from '@client/layouts';
import { GridColDef, GridValueGetterParams, GridValueFormatterParams, GridRowParams } from '@mui/x-data-grid';
import NotInterestedIcon from '@mui/icons-material/NotInterested';
import VerifiedUserIcon from '@mui/icons-material/VerifiedUser';
import moment from 'moment';
import { selectUserState, selectUsersState, deleteUsers as deleteUsersThunk } from '@client/reducers';
import Grid3x3Icon from '@mui/icons-material/Grid3x3';
import PhoneDisabledIcon from '@mui/icons-material/PhoneDisabled';
import { UserRolesEnum, ClientRoutesEnum } from '@constants';
import { UILink } from '@ui';
import { AddUser } from '@client/modules';

export const UsersList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { user } = useSelector(selectUserState);
  const { users, isUsersLoading } = useSelector(selectUsersState);
  const restrictedCells = ['verified', '__check__', 'fullName'];
  const columns: GridColDef[] = [
    {
      field: 'id',
      headerName: t('ID'),
      width: 50,
      hideSortIcons: true,
      disableColumnMenu: true,
      align: 'center',
      headerAlign: 'center',
      renderCell: () => <Grid3x3Icon />,
    },
    {
      field: 'fullName',
      headerName: t('Full name'),
      width: 160,
      valueGetter: (params: GridValueGetterParams) => {
        const fullNameList = [];
        const { firstName, lastName } = params.row;
        firstName && fullNameList.push(firstName);
        lastName && fullNameList.push(lastName);

        return fullNameList.join(' ');
      },
      renderCell: ({ id, value }: GridValueGetterParams) => {
        let link = `${ClientRoutesEnum.USERS}/${id}`;

        if (user && user.id === id) {
          link = ClientRoutesEnum.PROFILE;
        }

        return <UILink to={link}>{value}</UILink>;
      },
    },
    { field: 'email', headerName: 'Email', width: 260 },
    {
      field: 'phone',
      headerName: t('Phone'),
      width: 200,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params: GridValueGetterParams) => {
        const phone = params.row.phone || '';
        if (!phone) {
          return <PhoneDisabledIcon />;
        }
        return phone;
      },
    },
    {
      field: 'emailVerified',
      headerName: t('Email Verified'),
      width: 130,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params: GridValueGetterParams) => {
        const emailVerified = params.row.emailConfirmedAt || '';
        if (emailVerified) {
          return <VerifiedUserIcon color="primary" />;
        }
        return <NotInterestedIcon color="error" />;
      },
    },
    {
      field: 'role',
      headerName: t('Role'),
      width: 90,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params: GridValueGetterParams) => {
        const role = params.row.role || '';
        if (role === UserRolesEnum.ADMIN) {
          return <strong>{role}</strong>;
        }
        return role;
      },
    },
    {
      field: 'dob',
      headerName: t('Date of birth'),
      width: 200,
      align: 'center',
      headerAlign: 'center',
      renderCell: (params: GridValueGetterParams) => {
        const dob = String(params.value || '');

        if (!dob) {
          return t('Not provided');
        }

        return moment(dob).format('DD/MM/YYYY');
      },
    },
    {
      field: 'createdAt',
      headerName: t('Registration date'),
      width: 200,
      valueFormatter: (params: GridValueFormatterParams) => {
        const createdAt = String(params.value);
        return moment(createdAt).format('DD/MM/YYYY, h:mm:ss');
      },
    },
    {
      field: 'updatedAt',
      headerName: t('Updated at'),
      width: 150,
      renderCell: (params: GridValueGetterParams) => {
        let updatedAt = params.row.updatedAt || '';

        if (!updatedAt) {
          return t('Not updated');
        }

        updatedAt = String(updatedAt);

        return moment(updatedAt).format('DD/MM/YYYY, h:mm:ss');
      },
    },
  ];
  const isRowSelectable = (params: GridRowParams) => {
    if (user && user.id === params.id) {
      return false;
    }

    return true;
  };
  const deleteUsers = (ids: string[]) => {
    dispatch(deleteUsersThunk(ids));
  };

  return (
    <Admin
      items={users}
      isLoading={isUsersLoading}
      pageName="Users"
      onDelete={deleteUsers}
      columns={columns}
      title={t('Users')}
      noItemsMsg={t('There are no users')}
      restrictedPromptItems={restrictedCells}
      isRowSelectable={isRowSelectable}
      titleActions={<AddUser />}
    />
  );
};

export default UsersList;
