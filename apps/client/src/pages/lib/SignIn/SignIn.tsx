import Avatar from '@mui/material/Avatar';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { UIFlexBox, UITabs } from '@ui';
import { Simple } from '@client/layouts';
import { LogIn, SignUp } from '@client/modules';
import { useSelector, useDispatch } from 'react-redux';
import { selectAuthState, setActiveSignInTab, selectConfigState } from '@client/reducers';
import { ClientRoutesEnum, SignInTabsEnum } from '@constants';
import { Navigate, useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import styles from './SignIn.module.sass';

export const SignIn = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const tabs = [
    {
      key: SignInTabsEnum.LOGIN,
      label: 'Log In',
      Component: LogIn,
    },
    {
      key: SignInTabsEnum.SINGUP,
      label: 'Sing Up',
      Component: SignUp,
    },
  ];
  const { isLoggedIn } = useSelector(selectAuthState);
  const { activeSignInTab } = useSelector(selectConfigState);

  const onTabsChange = (newValue: string) => dispatch(setActiveSignInTab(newValue as SignInTabsEnum));

  useEffect(() => {
    if (isLoggedIn) {
      navigate(ClientRoutesEnum.HOME);
    }
  }, [isLoggedIn]);

  useEffect(() => {
    if (activeSignInTab !== SignInTabsEnum.LOGIN) {
      dispatch(setActiveSignInTab(SignInTabsEnum.LOGIN));
    }
  }, []);

  if (isLoggedIn) {
    return <Navigate to={ClientRoutesEnum.HOME} replace />;
  }

  return (
    <Simple maxWidth="xs">
      <UIFlexBox fd="c" ai="c" jc="s" sx={{ pt: 10, minWidth: 400 }}>
        <Avatar sx={{ m: 1 }}>
          <LockOutlinedIcon />
        </Avatar>
        <UITabs stretchTab activeTab={activeSignInTab} onTabsChange={onTabsChange} className={styles.tabs} ariaLabel={'Sing In tabs'} tabs={tabs} />
      </UIFlexBox>
    </Simple>
  );
};

export default SignIn;
