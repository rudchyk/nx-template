import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { setActiveErrorNotification, setActiveSuccessNotification } from '@client/reducers';
import { FeaturesEnum, SignInTabsEnum, ConfigEnum } from '@constants';
import { ConfigData } from '@interfaces';
import merge from 'lodash/merge';
import { getErrorMessage } from '@client/utils';
import { getConfigAPI, setConfigAPI, UpdateConfigData } from '@client/services';

export const CONFIG_KEY = 'config';

export interface ConfigState {
  features: FeaturesEnum[];
  config: ConfigData;
  activeSignInTab: SignInTabsEnum;
  isConfigLoading: boolean;
  isConfigLoaded: boolean;
  isConfigUpdated: boolean;
}

const defaultConfig = {
  [ConfigEnum.ENABLED_FEATURES]: [],
};

const initialState: ConfigState = {
  features: Object.values(FeaturesEnum),
  config: defaultConfig,
  activeSignInTab: SignInTabsEnum.LOGIN,
  isConfigLoading: false,
  isConfigLoaded: false,
  isConfigUpdated: false,
};

export const selectConfigState = (state: RootState): ConfigState => state[CONFIG_KEY];

export const getConfig = createAsyncThunk(`${CONFIG_KEY}/fetchConfig`, async (userId: string, { rejectWithValue, dispatch }) => {
  try {
    const { data } = await getConfigAPI(userId);
    return data.config;
  } catch (error) {
    dispatch(setActiveErrorNotification(getErrorMessage(error)));

    return rejectWithValue(null);
  }
});

export const setConfig = createAsyncThunk(`${CONFIG_KEY}/setConfig`, async (configData: UpdateConfigData, { rejectWithValue, dispatch }) => {
  try {
    const { data } = await setConfigAPI(configData);
    dispatch(setActiveSuccessNotification(data.message));
    return null;
  } catch (error) {
    dispatch(setActiveErrorNotification(getErrorMessage(error)));

    return rejectWithValue(null);
  }
});

export const configSlice = createSlice({
  name: CONFIG_KEY,
  initialState,
  reducers: {
    enableFeature: (state, { payload }: PayloadAction<FeaturesEnum>) => {
      if (!state.config.enabledFeatures.includes(payload)) {
        state.config.enabledFeatures.push(payload);
      } else {
        console.warn(`${payload} is not enabled, please, check your code`);
      }
    },
    setIsConfigUpdated: (state, { payload }: PayloadAction<boolean>) => {
      state.isConfigUpdated = payload;
    },
    disableFeature: (state, { payload }: PayloadAction<FeaturesEnum>) => {
      if (state.config.enabledFeatures.includes(payload)) {
        state.config.enabledFeatures = state.config.enabledFeatures.filter((feature) => feature !== payload);
      } else {
        console.warn(`${payload} is disabled, please, check your code`);
      }
    },
    setActiveSignInTab: (state, { payload }: PayloadAction<SignInTabsEnum>) => {
      state.activeSignInTab = payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getConfig.pending, (state) => {
        state.isConfigLoading = true;
      })
      .addCase(getConfig.fulfilled, (state, { payload }) => {
        if (payload) {
          state.config = merge(state.config, payload);
        }
        state.isConfigLoaded = true;
        state.isConfigLoading = false;
      });
    builder.addCase(setConfig.fulfilled, (state) => {
      state.isConfigUpdated = false;
    });
  },
});

export const { setActiveSignInTab, enableFeature, disableFeature, setIsConfigUpdated } = configSlice.actions;

export const configReducer = configSlice.reducer;
