import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { Notification } from '@interfaces';

export const NOTIFICATIONS_KEY = 'notifications';

export interface NotificationsState {
  activeNotification: Notification | null;
}

const initialState: NotificationsState = {
  activeNotification: null,
};

export const selectNotificationsState = (state: RootState): NotificationsState => state[NOTIFICATIONS_KEY];

export const notificationsSlice = createSlice({
  name: NOTIFICATIONS_KEY,
  initialState,
  reducers: {
    clearActiveNotification: (state) => {
      state.activeNotification = null;
    },
    setActiveNotification: (state, { payload }: PayloadAction<Notification>) => {
      state.activeNotification = payload;
    },
    setActiveSuccessNotification: (state, { payload }: PayloadAction<string>) => {
      state.activeNotification = {
        severity: 'success',
        message: payload,
      };
    },
    setActiveInfoNotification: (state, { payload }: PayloadAction<string>) => {
      state.activeNotification = {
        severity: 'info',
        message: payload,
      };
    },
    setActiveWarningNotification: (state, { payload }: PayloadAction<string>) => {
      state.activeNotification = {
        severity: 'warning',
        message: payload,
      };
    },
    setActiveErrorNotification: (state, { payload }: PayloadAction<string>) => {
      state.activeNotification = {
        severity: 'error',
        message: payload,
      };
    },
  },
});

export const {
  clearActiveNotification,
  setActiveNotification,
  setActiveSuccessNotification,
  setActiveInfoNotification,
  setActiveWarningNotification,
  setActiveErrorNotification,
} = notificationsSlice.actions;

export const notificationsReducer = notificationsSlice.reducer;
