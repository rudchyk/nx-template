import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { getPostsApi } from '@client/services';
import { AxiosError } from 'axios';
import { PostAPIInterface } from '@interfaces';
import { getErrorMessage } from '@client/utils';

export const POSTS_KEY = 'posts';

export interface PostsState {
  isPostsLoading: boolean;
  isPostsLoaded: boolean;
  posts: PostAPIInterface[] | null;
  postsErrorMessage: string | null;
}

const initialState: PostsState = {
  isPostsLoading: false,
  isPostsLoaded: false,
  posts: null,
  postsErrorMessage: null,
};

export const fetchPosts = createAsyncThunk(`${POSTS_KEY}/fetchPosts`, async (limit: number | undefined, { rejectWithValue }) => {
  try {
    const { data } = await getPostsApi(limit);
    return data;
  } catch (error) {
    return rejectWithValue(getErrorMessage(error));
  }
});

export const selectPostsState = (state: RootState): PostsState => state[POSTS_KEY];

export const postsSlice = createSlice({
  name: POSTS_KEY,
  initialState,
  reducers: {
    clearPosts: (state) => {
      state.posts = null;
      state.isPostsLoaded = false;
      state.isPostsLoading = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchPosts.pending, (state) => {
        state.isPostsLoading = true;
        state.isPostsLoaded = false;
      })
      .addCase(fetchPosts.fulfilled, (state, { payload }) => {
        state.posts = payload;
        state.isPostsLoaded = true;
        state.isPostsLoading = false;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.isPostsLoading = false;
        state.postsErrorMessage = action.error.message || null;
      });
  },
});

export const { clearPosts } = postsSlice.actions;

export const postsReducer = postsSlice.reducer;
