import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { LanguagesKeysEnum, defaultLanguage, ThemeModesEnum, defaultThemeMode } from '@constants';

export const SETTINGS_KEY = 'settings';

export interface SettingsState {
  themeMode: ThemeModesEnum;
  isCookiesAccepted: boolean | undefined;
  language: LanguagesKeysEnum;
}

const initialState: SettingsState = {
  themeMode: defaultThemeMode,
  isCookiesAccepted: undefined,
  language: defaultLanguage,
};

export const selectSettingsState = (state: RootState): SettingsState => state[SETTINGS_KEY];

export const settingsSlice = createSlice({
  name: SETTINGS_KEY,
  initialState,
  reducers: {
    setLanguage: (state, { payload }: PayloadAction<LanguagesKeysEnum>) => {
      state.language = payload;
    },
    setThemeMode: (state, { payload }: PayloadAction<ThemeModesEnum>) => {
      state.themeMode = payload;
    },
    setCookiesPolicy: (state, { payload }: PayloadAction<boolean>) => {
      state.isCookiesAccepted = payload;
    },
  },
});

export const { setLanguage, setThemeMode, setCookiesPolicy } = settingsSlice.actions;

export const settingsReducer = settingsSlice.reducer;
