import i18n from 'i18next';
import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { logInApi, signUpApi, authApi } from '@client/services';
import { setUser, setActiveSuccessNotification } from '@client/reducers';
import { getErrorMessage } from '@client/utils';
import { AuthAPIData } from '@interfaces';
import { AuthTypesEnum } from '@constants';

export const AUTH_KEY = 'auth';

export interface SignInData {
  email: string;
  password: string;
}

export const authorization = createAsyncThunk<
  AuthAPIData,
  string,
  {
    rejectValue: string;
  }
>(`${AUTH_KEY}/Auth`, async (token, { rejectWithValue, dispatch }) => {
  try {
    const { data } = await authApi(token);

    dispatch(setUser(data.user));

    return data;
  } catch (error) {
    return rejectWithValue(getErrorMessage(error));
  }
});

export const logIn = createAsyncThunk<
  AuthAPIData,
  string,
  {
    rejectValue: string;
  }
>(`${AUTH_KEY}/LogIn`, async (token, { rejectWithValue, dispatch }) => {
  try {
    const { data } = await logInApi(token);

    dispatch(setUser(data.user));
    dispatch(setActiveSuccessNotification(i18n.t('User is logged in', { email: data.user.email })));

    return data;
  } catch (error) {
    return rejectWithValue(getErrorMessage(error));
  }
});

export const signUp = createAsyncThunk<
  AuthAPIData,
  string,
  {
    rejectValue: string;
  }
>(`${AUTH_KEY}/SignUp`, async (token, { rejectWithValue, dispatch }) => {
  try {
    const { data } = await signUpApi(token);

    dispatch(setUser(data.user));
    dispatch(setActiveSuccessNotification(i18n.t('User is registered', { email: data.user.email })));

    return data;
  } catch (error) {
    return rejectWithValue(getErrorMessage(error));
  }
});

export interface AuthError {
  [key: string]: string;
}

export interface AuthState {
  token: string | null;
  isRemember: boolean;
  isLoggingIn: boolean;
  isLoggedIn: boolean;
  authError: AuthError;
}

export const initialAuthState: AuthState = {
  token: null,
  isRemember: false,
  isLoggingIn: false,
  isLoggedIn: false,
  authError: {},
};

export const authSlice = createSlice({
  name: AUTH_KEY,
  initialState: initialAuthState,
  reducers: {
    clearAuthError: (state, { payload }: PayloadAction<AuthTypesEnum>) => {
      delete state.authError[payload];
    },
    signOut: (state) => {
      state.isLoggedIn = false;
      state.token = null;
      state.isRemember = false;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(logIn.pending, (state: AuthState) => {
        state.isLoggingIn = true;
      })
      .addCase(logIn.fulfilled, (state: AuthState, { payload }) => {
        state.isLoggingIn = false;
        state.isLoggedIn = true;
        state.isRemember = payload.remember;
        state.token = payload.token;
      })
      .addCase(logIn.rejected, (state: AuthState, { payload }: PayloadAction<any>) => {
        state.isLoggingIn = false;
        state.authError[AuthTypesEnum.LOGIN] = payload;
      });
    builder
      .addCase(signUp.pending, (state: AuthState) => {
        state.isLoggingIn = true;
      })
      .addCase(signUp.fulfilled, (state: AuthState, { payload }) => {
        state.isLoggingIn = false;
        state.isLoggedIn = true;
        state.isRemember = payload.remember;
        state.token = payload.token;
      })
      .addCase(signUp.rejected, (state: AuthState, { payload }: PayloadAction<any>) => {
        state.isLoggingIn = false;
        state.authError[AuthTypesEnum.REGISTRATION] = payload;
      });
    builder
      .addCase(authorization.pending, (state: AuthState) => {
        state.isLoggingIn = true;
      })
      .addCase(authorization.fulfilled, (state: AuthState, { payload }) => {
        state.isLoggingIn = false;
        state.isLoggedIn = true;
        state.isRemember = payload.remember;
        state.token = payload.token;
      })
      .addCase(authorization.rejected, (state: AuthState, { payload }: PayloadAction<any>) => {
        state.isLoggingIn = false;
      });
  },
});

export const authReducer = authSlice.reducer;

export const { clearAuthError, signOut } = authSlice.actions;

export const selectAuthState = (state: RootState): AuthState => {
  return state[AUTH_KEY];
};
