import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { User } from '@interfaces';

export const USER_KEY = 'user';
export interface UserState {
  user: User | null;
}

const initialState: UserState = {
  user: null,
};

export const selectUserState = (state: RootState): UserState => state[USER_KEY];

export const userSlice = createSlice({
  name: USER_KEY,
  initialState,
  reducers: {
    clearUser: (state) => {
      state.user = null;
    },
    setUser: (state, { payload }: PayloadAction<User>) => {
      state.user = payload;
    },
  },
});

export const { clearUser, setUser } = userSlice.actions;

export const userReducer = userSlice.reducer;
