import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { User } from '@interfaces';
import { getUsersApi, deleteUsersApi, addUserApi } from '@client/services';
import { getErrorMessage } from '@client/utils';
import { UsersMessageTypesEnum } from '@constants';
import { setActiveSuccessNotification, setActiveErrorNotification } from '@client/reducers';

export const USERS_KEY = 'users';

interface UsersErrorMessage {
  [key: string]: string;
}

export interface UsersState {
  isUsersLoading: boolean;
  isUsersLoaded: boolean;
  users: User[];
  usersErrorMessages: UsersErrorMessage;
  isUserAdded: boolean;
  isUserAdding: boolean;
}

const initialState: UsersState = {
  isUsersLoading: false,
  isUsersLoaded: false,
  users: [],
  usersErrorMessages: {},
  isUserAdded: false,
  isUserAdding: false,
};

export const fetchUsers = createAsyncThunk<User[]>(`${USERS_KEY}/fetchUsers`, async (_, { dispatch, rejectWithValue }) => {
  try {
    const { data } = await getUsersApi();
    return data.users;
  } catch (error) {
    dispatch(setActiveErrorNotification(getErrorMessage(error)));

    return rejectWithValue(null);
  }
});

export const addUser = createAsyncThunk<User[], string>(`${USERS_KEY}/addUser`, async (token, { rejectWithValue, dispatch }) => {
  try {
    const {
      data: { message, users },
    } = await addUserApi(token);

    dispatch(setActiveSuccessNotification(message));
    return users;
  } catch (error) {
    return rejectWithValue(getErrorMessage(error));
  }
});

export const deleteUsers = createAsyncThunk<User[], string[]>(`${USERS_KEY}/deleteUsers`, async (ids, { rejectWithValue, dispatch }) => {
  try {
    const {
      data: { message, users },
    } = await deleteUsersApi({ ids });

    dispatch(setActiveSuccessNotification(message));

    return users;
  } catch (error) {
    dispatch(setActiveErrorNotification(getErrorMessage(error)));

    return rejectWithValue(null);
  }
});

export const selectUsersState = (state: RootState): UsersState => state[USERS_KEY];

export const selectUsersById =
  (userId?: string) =>
  (state: RootState): User | undefined => {
    console.log(userId);

    return state[USERS_KEY].users.find((user) => user.id === userId);
  };

export const usersSlice = createSlice({
  name: USERS_KEY,
  initialState,
  reducers: {
    setIsUserAdded: (state, { payload }: PayloadAction<boolean>) => {
      state.isUserAdded = payload;
    },
    clearUsers: (state) => {
      state.users = [];
      state.isUsersLoaded = false;
      state.isUsersLoading = false;
    },
    cleanUsersErrorMessages: (state) => {
      state.usersErrorMessages = {};
    },
    cleanUsersErrorMessage: (state, { payload }: PayloadAction<UsersMessageTypesEnum>) => {
      delete state.usersErrorMessages[payload];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchUsers.pending, (state) => {
        state.isUsersLoading = true;
      })
      .addCase(fetchUsers.fulfilled, (state, { payload }) => {
        state.users = payload;
        state.isUsersLoaded = true;
        state.isUsersLoading = false;
      })
      .addCase(fetchUsers.rejected, (state, { payload }: any) => {
        state.isUsersLoading = false;
      });
    builder
      .addCase(deleteUsers.pending, (state) => {
        state.isUsersLoading = true;
      })
      .addCase(deleteUsers.fulfilled, (state, { payload }) => {
        state.users = payload;
        state.isUsersLoading = false;
      })
      .addCase(deleteUsers.rejected, (state, { payload }: any) => {
        state.isUsersLoading = false;
      });
    builder
      .addCase(addUser.pending, (state) => {
        state.isUserAdding = true;
        state.isUsersLoading = true;
      })
      .addCase(addUser.fulfilled, (state, { payload }) => {
        state.users = payload;
        state.isUserAdding = false;
        state.isUsersLoading = false;
        state.isUserAdded = true;
      })
      .addCase(addUser.rejected, (state, { payload }: any) => {
        state.isUserAdding = false;
        state.isUsersLoading = false;
        state.usersErrorMessages[UsersMessageTypesEnum.ADD] = payload;
      });
  },
});

export const { clearUsers, cleanUsersErrorMessages, cleanUsersErrorMessage, setIsUserAdded } = usersSlice.actions;

export const usersReducer = usersSlice.reducer;
