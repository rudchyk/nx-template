import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '@client/store';
import { DialogErrorData } from '@client/components';

export const ERRORS_KEY = 'errors';

export interface ErrorsState {
  activeError: DialogErrorData | null;
}

const initialState: ErrorsState = {
  activeError: null,
};

export const selectErrorsState = (state: RootState): ErrorsState => state[ERRORS_KEY];

export const errorsSlice = createSlice({
  name: ERRORS_KEY,
  initialState,
  reducers: {
    clearActiveError: (state) => {
      state.activeError = null;
    },
    setActiveError: (state, { payload }: PayloadAction<DialogErrorData>) => {
      state.activeError = payload;
    },
  },
});

export const { clearActiveError, setActiveError } = errorsSlice.actions;

export const errorsReducer = errorsSlice.reducer;
