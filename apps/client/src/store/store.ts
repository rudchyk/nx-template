import { configureStore } from '@reduxjs/toolkit';
import ReduxThunk from 'redux-thunk';
import { isDev } from '@utils';
import { reducers } from './reducers';

export type RootState = ReturnType<typeof store.getState>;

const store = configureStore({
  reducer: reducers,
  middleware: [ReduxThunk],
  devTools: isDev(),
});

export default store;
