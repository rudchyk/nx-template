import { combineReducers } from 'redux';
import {
  POSTS_KEY,
  PostsState,
  postsReducer,
  SETTINGS_KEY,
  SettingsState,
  settingsReducer,
  CONFIG_KEY,
  ConfigState,
  configReducer,
  AUTH_KEY,
  AuthState,
  authReducer,
  USER_KEY,
  UserState,
  userReducer,
  USERS_KEY,
  UsersState,
  usersReducer,
  ERRORS_KEY,
  ErrorsState,
  errorsReducer,
  NOTIFICATIONS_KEY,
  NotificationsState,
  notificationsReducer,
  // imports
} from '@client/reducers';

export interface StoreState {
  [POSTS_KEY]: PostsState;
  [SETTINGS_KEY]: SettingsState;
  [CONFIG_KEY]: ConfigState;
  [AUTH_KEY]: AuthState;
  [USER_KEY]: UserState;
  [USERS_KEY]: UsersState;
  [ERRORS_KEY]: ErrorsState;
  [NOTIFICATIONS_KEY]: NotificationsState;
  // state
}

export const reducers = combineReducers<StoreState>({
  [POSTS_KEY]: postsReducer,
  [SETTINGS_KEY]: settingsReducer,
  [CONFIG_KEY]: configReducer,
  [AUTH_KEY]: authReducer,
  [USER_KEY]: userReducer,
  [USERS_KEY]: usersReducer,
  [ERRORS_KEY]: errorsReducer,
  [NOTIFICATIONS_KEY]: notificationsReducer,
  // reducers
});
