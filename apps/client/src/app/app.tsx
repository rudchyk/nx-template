import { FeatureToggles } from '@paralleldrive/react-feature-toggles';
import { ErrorBoundary } from 'react-error-boundary';
import { useSelector } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { selectConfigState, selectSettingsState } from '@client/reducers';
import { UITheme, WithProgress } from '@ui';
import { palette, StoredCookiesEnum } from '@constants';
import { CookiesPolicyAlert } from '@client/components';
import { ErrorFallback } from '@client/pages';
import { useSetup } from '@client/hooks';
import { AppContent, Notifications, GlobalError } from '@client/modules';

(StoredCookiesEnum as any).COOKIES = `${window.location.host.replace(/:/g, '_')}_cookies`;

const AppContentWithProgress = WithProgress(AppContent);

const App = () => {
  const {
    config: { enabledFeatures },
  } = useSelector(selectConfigState);
  const { themeMode } = useSelector(selectSettingsState);
  const { isSetupFinished } = useSetup();
  const errorHandler = (error: Error, info: { componentStack: string }) => {
    console.log('React Error Handler', error.name, error.message);
  };
  const onResetErrorHandler = () => {
    console.log('React onReset Error Handler');
  };

  return (
    <UITheme themeMode={themeMode} palette={palette}>
      <ErrorBoundary onReset={onResetErrorHandler} FallbackComponent={ErrorFallback} onError={errorHandler}>
        <BrowserRouter>
          <FeatureToggles features={enabledFeatures}>
            <AppContentWithProgress isLoading={!isSetupFinished} />
          </FeatureToggles>
          <CookiesPolicyAlert />
          <Notifications />
          <GlobalError />
        </BrowserRouter>
      </ErrorBoundary>
    </UITheme>
  );
};

export default App;
