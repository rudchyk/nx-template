import { useState, useEffect, ChangeEvent } from 'react';
import { DialogForm } from '@client/components';
import { changeUserPasswordApi, ChangeUserPasswordData } from '@client/services';
import { getErrorMessage } from '@client/utils';
import { useTranslation } from 'react-i18next';
import { ChangePasswordForm } from '@client/forms';
import { setActiveSuccessNotification, setActiveErrorNotification } from '@client/reducers';
import { useDispatch } from 'react-redux';
import { UIButton } from '@ui';
import { ChangePasswordFormInputs, Notification, ChangeUserPasswordResponseAPIData } from '@interfaces';
import { ChangePasswordFormInputsEnum } from '@constants';
import { jwsSign } from '@utils';
import { useRichForm } from '@hooks';

interface ChangePasswordProps {
  userId: string;
}

export const ChangeUserPassword = ({ userId }: ChangePasswordProps) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const { onSubmit, alert, clearAlert, isLoading } = useRichForm({
    service: changeUserPasswordApi,
  });
  const onDeleteUserClick = () => {
    setIsDialogOpen(true);
  };
  const onFormSubmit = async (formData: ChangePasswordFormInputs) => {
    try {
      const data: any = { ...formData };

      delete data[ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD];

      const token = await jwsSign(data);

      onSubmit<ChangeUserPasswordData, ChangeUserPasswordResponseAPIData>({ id: userId, token }, (data) => {
        dispatch(setActiveSuccessNotification(data.message));
        onDialogClose();
      });
    } catch (error) {
      console.log('ChangePassword', 'onFormSubmit', error);
    }
  };
  const onDialogClose = () => {
    setIsDialogOpen(false);
    clearAlert();
  };

  useEffect(() => {
    clearAlert();
  }, [isDialogOpen]);

  return (
    <DialogForm
      trigger={
        <UIButton variant="outlined" aria-label={t('Change Password')} onClick={onDeleteUserClick}>
          {t('Change Password')}
        </UIButton>
      }
      onDialogClose={onDialogClose}
      isLoading={isLoading}
      buttonPrimaryText={t('Submit')}
      title={t('Change Password')}
      open={isDialogOpen}
      form={<ChangePasswordForm onChange={clearAlert} alert={alert} onSubmit={onFormSubmit} />}
    />
  );
};

export default ChangeUserPassword;
