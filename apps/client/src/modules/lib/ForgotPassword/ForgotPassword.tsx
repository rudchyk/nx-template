import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import { Grid } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { HomeButton } from '@client/components';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import { VerifyEmail, ResetPassword } from '@client/modules';

interface ResetPasswordProps {
  onBack: () => void;
}

export const ForgotPassword = ({ onBack }: ResetPasswordProps) => {
  const { t } = useTranslation();
  const [verifiedEmail, setVerifiedEmail] = useState<string | undefined>();
  const onVerifyForm = (email: string) => setVerifiedEmail(email);
  const handleOnReset = () => {
    setVerifiedEmail(undefined);
    onBack();
  };

  return (
    <>
      <Typography align="center" component="h2" variant="h6">
        {t('Reset password')}
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          {verifiedEmail ? <ResetPassword onReset={handleOnReset} verifiedEmail={verifiedEmail} /> : <VerifyEmail onVerify={onVerifyForm} />}
        </Grid>
        <Grid item xs={12} sm={6}>
          <HomeButton />
        </Grid>
        <Grid item xs={12} sm={6} sx={{ textAlign: 'right' }}>
          <Button size="small" onClick={onBack} startIcon={<ArrowBackIosNewIcon />}>
            {t('Back to the Log In')}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default ForgotPassword;
