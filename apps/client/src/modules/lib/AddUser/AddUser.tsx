import { Fab } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { useSelector, useDispatch } from 'react-redux';
import { DialogForm } from '@client/components';
import { useState } from 'react';
import { t } from 'i18next';
import { ChangeEvent, useEffect } from 'react';
import { jwsSign } from '@utils';
import { selectUsersState, addUser, cleanUsersErrorMessage, setIsUserAdded } from '@client/reducers';
import { AddUserForm } from '@client/forms';
import { AddUserFormInputsEnum, UsersMessageTypesEnum } from '@constants';
import { AddUserFormInputs, Notification } from '@interfaces';

export const AddUser = () => {
  const dispatch = useDispatch();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const { usersErrorMessages, isUserAdding, isUserAdded } = useSelector(selectUsersState);
  const onFormSubmit = async (formData: AddUserFormInputs) => {
    try {
      const data: any = { ...formData };

      delete data[AddUserFormInputsEnum.REPEATED_PASSWORD];

      const token = await jwsSign(data);

      dispatch(addUser(token));
    } catch (error) {
      console.log('AddUser', 'onFormSubmit', error);
    }
  };
  const cleanMessage = () => {
    if (usersErrorMessages[UsersMessageTypesEnum.ADD]) {
      dispatch(cleanUsersErrorMessage(UsersMessageTypesEnum.ADD));
    }
  };
  const onFormChange = (event: ChangeEvent<HTMLFormElement>) => cleanMessage();
  const onAddUserClick = () => setIsDialogOpen(true);
  const onDialogClose = () => {
    setIsDialogOpen(false);
    cleanMessage();
  };
  const getErrorMessage = (): Notification | undefined => {
    if (!usersErrorMessages[UsersMessageTypesEnum.ADD]) {
      return undefined;
    }

    return {
      severity: 'error',
      message: usersErrorMessages[UsersMessageTypesEnum.ADD],
    };
  };

  useEffect(() => {
    if (isUserAdded) {
      setIsDialogOpen(false);
      setIsUserAdded(false);
    }
  }, [isUserAdded]);

  return (
    <DialogForm
      trigger={
        <Fab color="primary" aria-label="add" onClick={onAddUserClick}>
          <AddIcon fontSize="large" />
        </Fab>
      }
      form={<AddUserForm onSubmit={onFormSubmit} onChange={onFormChange} alert={getErrorMessage()} />}
      buttonPrimaryText={t('Add')}
      buttonPrimaryProps={{
        loading: isUserAdding,
      }}
      title={t('Add User')}
      onDialogClose={onDialogClose}
      open={isDialogOpen}
    />
  );
};

export default AddUser;
