import { useTranslation } from 'react-i18next';
import { VerifyEmailForm } from '@client/forms';
import { useRichForm } from '@hooks';
import { VerifyEmailAPIData, VerifyEmailFormInputs } from '@interfaces';
import { forgotPasswordApi } from '@client/services';
import { VerifyEmailFormInputsEnum } from '@constants';

interface VerifyEmailProps {
  onVerify: (email: string) => void;
}

export const VerifyEmail = ({ onVerify }: VerifyEmailProps) => {
  const { isLoading, onSubmit, alert, clearAlert } = useRichForm({
    service: forgotPasswordApi,
  });
  const onFormSubmit = (formData: VerifyEmailFormInputs) => {
    onSubmit<string, VerifyEmailAPIData>(formData[VerifyEmailFormInputsEnum.EMAIL], (data) => {
      onVerify(data.email);
    });
  };

  return <VerifyEmailForm onChange={clearAlert} onSubmit={onFormSubmit} isLoading={isLoading} alert={alert} />;
};

export default VerifyEmail;
