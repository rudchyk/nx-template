import { useSelector } from 'react-redux';
import { selectErrorsState } from '@client/reducers';
import { DialogError } from '@client/components';

export const GlobalError = () => {
  const { activeError } = useSelector(selectErrorsState);

  return <DialogError open={Boolean(activeError)} errorData={activeError} />;
};

export default GlobalError;
