import { render } from '@testing-library/react';
import GlobalError from './GlobalError';

describe('GlobalError', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<GlobalError />);

    expect(baseElement).toBeTruthy();
  });
});
