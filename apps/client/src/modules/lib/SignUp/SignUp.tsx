import { SignUpForm } from '@client/forms';
import { signUp, selectSettingsState } from '@client/reducers';
import { SubmitHandler } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { SignUpFormInputs, Notification } from '@interfaces';
import { SignUpFormInputsEnum, AuthTypesEnum } from '@constants';
import { jwsSign } from '@utils';
import { useAuthForm } from '@client/hooks';
import { Grid } from '@mui/material';
import { HomeButton } from '@client/components';

export const SignUp = () => {
  const dispatch = useDispatch();
  const { isLoggingIn, cleanAuthError, getAuthError } = useAuthForm(AuthTypesEnum.REGISTRATION);
  const { isCookiesAccepted } = useSelector(selectSettingsState);
  const onFormSubmit: SubmitHandler<SignUpFormInputs> = async (formData) => {
    try {
      const data: any = { ...formData };

      delete data[SignUpFormInputsEnum.REPEATED_PASSWORD];

      const token = await jwsSign(data);

      dispatch(signUp(token));
    } catch (error) {
      console.log('SignUp', error);
    }
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <SignUpForm
          onSubmit={onFormSubmit}
          onChange={cleanAuthError}
          alert={getAuthError() as Notification}
          isCookiesAccepted={isCookiesAccepted}
          isLoggingIn={isLoggingIn}
        />
      </Grid>
      <Grid item xs={12} sx={{ textAlign: 'right' }}>
        <HomeButton />
      </Grid>
    </Grid>
  );
};
