import { useDispatch, useSelector } from 'react-redux';
import { selectConfigState, selectUserState, enableFeature, disableFeature, setIsConfigUpdated } from '@client/reducers';
import FormControlLabel from '@mui/material/FormControlLabel';
import { FeaturesEnum } from '@constants';
import { Box, Switch } from '@mui/material';
import { ChangeEvent } from 'react';
import { useFeatures } from '@paralleldrive/react-feature-toggles';

export const FeatureTogglesSwitcher = () => {
  const dispatch = useDispatch();
  const features = useFeatures();
  const { features: storedFeatures } = useSelector(selectConfigState);
  const { user } = useSelector(selectUserState);
  const isFeatureEnabled = (value: string) => features.includes(value);
  const getFeatureBtnText = (value: string) => `${isFeatureEnabled(value) ? 'Disable' : 'Enable'} ${value.toUpperCase()} Feature`;
  const onFeatureChange = ({ currentTarget: { value } }: ChangeEvent<HTMLInputElement>) => {
    if (isFeatureEnabled(value)) {
      dispatch(disableFeature(value as FeaturesEnum));
    } else {
      dispatch(enableFeature(value as FeaturesEnum));
    }
    dispatch(setIsConfigUpdated(true));
  };

  return (
    <Box>
      {storedFeatures.map((storedFeature) => (
        <FormControlLabel
          key={storedFeature}
          control={<Switch onChange={onFeatureChange} checked={isFeatureEnabled(storedFeature)} value={storedFeature} />}
          label={getFeatureBtnText(storedFeature)}
        />
      ))}
    </Box>
  );
};

export default FeatureTogglesSwitcher;
