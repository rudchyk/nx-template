import { useState } from 'react';
import { selectUserState } from '@client/reducers';
import { useDispatch, useSelector } from 'react-redux';
import { setUser, setActiveSuccessNotification, setActiveErrorNotification } from '@client/reducers';
import { UpdateProfileFormInputsEnum } from '@constants';
import { UpdateProfileFormInputs, AnyObj, UpdateUserResponseAPIData } from '@interfaces';
import { useRichForm } from '@hooks';
import { updateUserApi, UpdateUserData } from '@client/services';
import { UpdateProfileForm } from '@client/forms';

export const UpdateProfile = () => {
  const { user } = useSelector(selectUserState);
  const [isUserUpdated, setIsUserUpdated] = useState(false);
  const dispatch = useDispatch();
  const { onSubmit } = useRichForm({
    service: updateUserApi,
  });
  const onFormSubmit = (formData: UpdateProfileFormInputs) => {
    const dataToUpdate: AnyObj = {};

    for (const key in formData) {
      if (Object.prototype.hasOwnProperty.call(formData, key)) {
        const element = (formData as any)[key];
        if (element !== (user as any)[key]) {
          if (key === UpdateProfileFormInputsEnum.DOB && element) {
            dataToUpdate[key] = new Date(element);
          } else {
            dataToUpdate[key] = element;
          }
        }
      }
    }

    if (!user) {
      return;
    }

    onSubmit<UpdateUserData, UpdateUserResponseAPIData>(
      { id: user.id, data: dataToUpdate },
      (data) => {
        dispatch(setUser(data.user));
        dispatch(setActiveSuccessNotification(data.message));
        setIsUserUpdated(true);
      },
      (message) => {
        dispatch(setActiveErrorNotification(message));
      }
    );
  };

  return <UpdateProfileForm onSubmit={onFormSubmit} user={user} isUserUpdated={isUserUpdated} />;
};

export default UpdateProfile;
