import { ResetPasswordForm } from '@client/forms';
import { useRichForm } from '@hooks';
import { resetPasswordApi } from '@client/services';
import { ClientRoutesEnum, ResetPasswordFormInputsEnum } from '@constants';
import { ResetPasswordAPIData, ResetPasswordFormInputs } from '@interfaces';
import { jwsSign } from '@utils';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { setActiveSuccessNotification } from '@client/reducers';
import { useNavigate } from 'react-router-dom';

interface ResetPasswordProps {
  verifiedEmail?: string;
  onReset: () => void;
}

export const ResetPassword = ({ verifiedEmail, onReset }: ResetPasswordProps) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoading, onSubmit, alert, clearAlert } = useRichForm({
    service: resetPasswordApi,
  });
  const onFormSubmit = async (formData: ResetPasswordFormInputs) => {
    try {
      const data: any = { ...formData };

      delete data[ResetPasswordFormInputsEnum.REPEATED_PASSWORD];

      const token = await jwsSign(data);

      onSubmit<string, ResetPasswordAPIData>(token, (data) => {
        dispatch(setActiveSuccessNotification(t('password is reset', { email: data.email })));
        onReset();
      });
    } catch (error) {
      console.warn('ResetPassword', error);
    }
  };
  return <ResetPasswordForm isLoading={isLoading} onChange={clearAlert} onSubmit={onFormSubmit} verifiedEmail={verifiedEmail} alert={alert} />;
};

export default ResetPassword;
