import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { selectNotificationsState, clearActiveNotification } from '@client/reducers';
import { UISnackbar } from '@ui';

export const Notifications = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { activeNotification } = useSelector(selectNotificationsState);
  const onSnackbarClose = () => {
    dispatch(clearActiveNotification());
  };

  return (
    <UISnackbar open={Boolean(activeNotification)} security={activeNotification?.severity} message={activeNotification?.message} onClose={onSnackbarClose} />
  );
};

export default Notifications;
