import DeleteIcon from '@mui/icons-material/Delete';
import { useDispatch } from 'react-redux';
import { useState, useEffect } from 'react';
import { Fab } from '@mui/material';
import { DialogAreYouSure } from '@client/components';
import { deleteUserApi, logout } from '@client/services';
import { getErrorMessage } from '@client/utils';
import { setActiveSuccessNotification, setActiveErrorNotification } from '@client/reducers';

interface DeleteUserProps {
  userId?: string;
}

export const DeleteUser = ({ userId }: DeleteUserProps) => {
  const dispatch = useDispatch();
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const onDeleteUserClick = () => {
    setIsDialogOpen(true);
  };
  const onConfirmDialog = async () => {
    if (!userId) {
      return;
    }

    try {
      const { data } = await deleteUserApi(userId);
      if (data.message) {
        dispatch(setActiveSuccessNotification(data.message));
      }
      logout();
    } catch (error) {
      dispatch(setActiveErrorNotification(getErrorMessage(error)));
      console.log('DeleteUserPanel', 'onConfirmDialog', error);
    } finally {
      setIsDialogOpen(false);
    }
  };
  const onDialogClose = async () => {
    setIsDialogOpen(false);
  };

  return (
    <DialogAreYouSure
      trigger={
        <Fab color="error" aria-label="add" onClick={onDeleteUserClick}>
          <DeleteIcon fontSize="large" />
        </Fab>
      }
      open={isDialogOpen}
      onAgree={onConfirmDialog}
      onDialogClose={onDialogClose}
    />
  );
};

export default DeleteUser;
