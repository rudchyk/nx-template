import { useState } from 'react';
import { LogInForm } from '@client/forms';
import { ForgotPassword } from '@client/modules';
import { logIn } from '@client/reducers';
import { jwsSign } from '@utils';
import { SubmitHandler } from 'react-hook-form';
import { LogInFormInputs, Notification } from '@interfaces';
import { useAuthForm } from '@client/hooks';
import { AuthTypesEnum } from '@constants';
import { useDispatch } from 'react-redux';
import { Grid } from '@mui/material';
import { HomeButton } from '@client/components';

export const LogIn = () => {
  const dispatch = useDispatch();
  const { isLoggingIn, cleanAuthError, getAuthError } = useAuthForm(AuthTypesEnum.LOGIN);
  const [isResetPassword, setIsResetPassword] = useState(false);
  const onFormSubmit: SubmitHandler<LogInFormInputs> = async (formData) => {
    try {
      const token = await jwsSign(formData);
      dispatch(logIn(token));
    } catch (error) {
      console.log('LogInForm', error);
    }
  };

  return isResetPassword ? (
    <ForgotPassword onBack={() => setIsResetPassword(false)} />
  ) : (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <LogInForm
          onSubmit={onFormSubmit}
          onChange={cleanAuthError}
          alert={getAuthError() as Notification}
          isLoggingIn={isLoggingIn}
          onResetPassword={() => setIsResetPassword(true)}
        />
      </Grid>
      <Grid item xs={12} sx={{ textAlign: 'right' }}>
        <HomeButton />
      </Grid>
    </Grid>
  );
};
