import { Suspense, lazy } from 'react';
import { WithProgress } from '@ui';
import { Route, Routes } from 'react-router-dom';
import { selectAuthState, selectConfigState } from '@client/reducers';
import { useSelector } from 'react-redux';
import { ProtectedRoute } from '@client/components';
import { ClientRouteProtection } from '@interfaces';
import { ClientRoutesEnum, UserRolesEnum, ClientRouteProtectionTypesEnum, FeaturesEnum, AppRouteOptions } from '@constants';
import LinearProgress from '@mui/material/LinearProgress';

interface AppRoute {
  path: ClientRoutesEnum;
  element: JSX.Element;
  protection?: ClientRouteProtection;
  options?: AppRouteOptions;
}

const Home = lazy(() => import('../../../pages/lib/Home/Home'));
const Posts = lazy(() => import('../../../pages/lib/Posts/Posts'));
const NoMatch = lazy(() => import('../../../pages/lib/NoMatch/NoMatch'));
const SignIn = lazy(() => import('../../../pages/lib/SignIn/SignIn'));
const CookiesPolicy = lazy(() => import('../../../pages/lib/CookiesPolicy/CookiesPolicy'));
const ProtectedPage = lazy(() => import('../../../pages/lib/ProtectedPage/ProtectedPage'));
const Profile = lazy(() => import('../../../pages/lib/Profile/Profile'));
const Users = lazy(() => import('../../../pages/lib/Users/Users'));
const User = lazy(() => import('../../../pages/lib/User/User'));
const UsersList = lazy(() => import('../../../pages/lib/UsersList/UsersList'));
const Settings = lazy(() => import('../../../pages/lib/Settings/Settings'));
const Playground = lazy(() => import('../../../pages/lib/Playground/Playground'));

const AppContentWithProgress = WithProgress(() => {
  const routes: AppRoute[] = [
    {
      path: ClientRoutesEnum.HOME,
      element: <Home />,
    },
    {
      path: ClientRoutesEnum.POSTS,
      element: <Posts />,
    },
    {
      path: ClientRoutesEnum.SIGNIN,
      element: <SignIn />,
    },
    {
      path: ClientRoutesEnum.COOKIES,
      element: <CookiesPolicy />,
    },
    {
      path: ClientRoutesEnum.PROTECTED_PAGE,
      element: <ProtectedPage />,
      protection: UserRolesEnum.USER,
    },
    {
      path: ClientRoutesEnum.PROFILE,
      element: <Profile />,
      protection: UserRolesEnum.USER,
    },
    {
      path: ClientRoutesEnum.SETTINGS,
      element: <Settings />,
      protection: UserRolesEnum.USER,
    },
    {
      path: ClientRoutesEnum.PLAYGROUND,
      element: <Playground />,
      protection: ClientRouteProtectionTypesEnum.FEATURE,
      options: {
        feature: FeaturesEnum.PLAYGROUND,
      },
    },
  ];

  return (
    <Suspense fallback={<LinearProgress />}>
      <Routes>
        {routes.map(({ path, protection, element, options }) => (
          <Route
            key={path}
            path={path}
            element={
              <ProtectedRoute protection={protection} options={options}>
                {element}
              </ProtectedRoute>
            }
          />
        ))}
        <Route
          path={ClientRoutesEnum.USERS}
          element={
            <ProtectedRoute protection={UserRolesEnum.ADMIN}>
              <Users />
            </ProtectedRoute>
          }>
          <Route index element={<UsersList />} />
          <Route path=":userId" element={<User />} />
        </Route>
        <Route path="*" element={<NoMatch />} />
      </Routes>
    </Suspense>
  );
});

export const AppContent = () => {
  const { isLoggingIn } = useSelector(selectAuthState);
  const { isConfigLoading } = useSelector(selectConfigState);

  return <AppContentWithProgress isLoading={isLoggingIn || isConfigLoading} />;
};

export default AppContent;
