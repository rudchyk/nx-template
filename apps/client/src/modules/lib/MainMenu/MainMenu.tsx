import { ClientRoutesEnum } from '@constants';
import HomeIcon from '@mui/icons-material/Home';
import WysiwygIcon from '@mui/icons-material/Wysiwyg';
import ReportGmailerrorredIcon from '@mui/icons-material/ReportGmailerrorred';
import { useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import ShieldIcon from '@mui/icons-material/Shield';
import SupervisedUserCircleIcon from '@mui/icons-material/SupervisedUserCircle';
import { selectUserState } from '@client/reducers';
import { useSelector } from 'react-redux';
import { UserRolesEnum, FeaturesEnum } from '@constants';
import { Menu } from '@client/components';
import DashboardIcon from '@mui/icons-material/Dashboard';
import { useFeatures } from '@paralleldrive/react-feature-toggles';

export const MainMenu = () => {
  const { t } = useTranslation();
  const { user } = useSelector(selectUserState);
  const { pathname } = useLocation();
  const features = useFeatures();
  const menuItems = [
    {
      disabled: pathname === ClientRoutesEnum.HOME,
      text: t('Home'),
      to: ClientRoutesEnum.HOME,
      icon: HomeIcon,
    },
    {
      disabled: pathname === ClientRoutesEnum.POSTS,
      text: t('Posts'),
      to: ClientRoutesEnum.POSTS,
      icon: WysiwygIcon,
    },
    {
      disabled: pathname === ClientRoutesEnum.PROTECTED_PAGE,
      text: t('Protected Page'),
      to: ClientRoutesEnum.PROTECTED_PAGE,
      icon: ShieldIcon,
      isHidden: !user,
    },
    {
      disabled: pathname === ClientRoutesEnum.USERS,
      text: t('Users'),
      to: ClientRoutesEnum.USERS,
      icon: SupervisedUserCircleIcon,
      isHidden: !user || user.role !== UserRolesEnum.ADMIN,
    },
    {
      disabled: pathname === ClientRoutesEnum.PLAYGROUND,
      text: t('Playground'),
      to: ClientRoutesEnum.PLAYGROUND,
      icon: DashboardIcon,
      isHidden: !user || (user && !features.includes(FeaturesEnum.PLAYGROUND)),
    },
    {
      text: t('NoMatch'),
      to: '/sdfsdf',
      icon: ReportGmailerrorredIcon,
    },
  ];

  return <Menu items={menuItems} />;
};

export default MainMenu;
