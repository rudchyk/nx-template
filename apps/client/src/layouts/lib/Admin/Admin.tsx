import { useState, ReactNode } from 'react';
import { Default } from '@client/layouts';
import { Page } from '@client/templates';
import { DataGrid, GridColDef, GridRowId, GridSelectionModel, GridCellParams, GridRowParams } from '@mui/x-data-grid';
import { EnhancedTableToolbar } from '@client/components';
import DeleteIcon from '@mui/icons-material/Delete';

enum NotificationStatusEnum {
  error = 'error',
  success = 'success',
}

interface Notification {
  message: string;
  status: NotificationStatusEnum;
}

interface AdminProps {
  items?: any[];
  isLoading?: boolean;
  columns: GridColDef[];
  title: string;
  onDelete: (ids: string[]) => void;
  pageName: string;
  noItemsMsg: string;
  restrictedPromptItems?: string[];
  onCellClick?: (params: GridCellParams) => void;
  isRowSelectable?: (params: GridRowParams) => boolean;
  titleActions?: ReactNode;
  notification?: Notification;
}

export const Admin = ({
  columns,
  title,
  onDelete,
  noItemsMsg,
  restrictedPromptItems = [],
  onCellClick,
  isRowSelectable,
  titleActions,
  items = [],
  isLoading = false,
  notification,
}: AdminProps) => {
  const [selectedItems, setSelectedItems] = useState<GridRowId[]>([]);
  const onSelectionModelChange = (model: GridSelectionModel) => {
    setSelectedItems(model);
  };
  const onCellClickAction = (params: GridCellParams) => {
    if (params.value && !restrictedPromptItems.includes(params.field)) {
      prompt(params.field, String(params.value));
    }
    onCellClick && onCellClick(params);
  };
  const isItemsExist = () => Boolean(items && items.length);
  const onDeleteAction = () => {
    onDelete(selectedItems as string[]);
  };

  return (
    <Default>
      <Page title={title} titleActions={titleActions}>
        {!isItemsExist() && noItemsMsg}
        <section>
          <EnhancedTableToolbar count={selectedItems.length} title="selected" onClick={onDeleteAction} icon={<DeleteIcon />} />
          <DataGrid
            loading={isLoading}
            onCellClick={onCellClickAction}
            rows={items}
            columns={columns}
            autoHeight
            disableSelectionOnClick
            checkboxSelection
            onSelectionModelChange={onSelectionModelChange}
            isRowSelectable={isRowSelectable}
          />
        </section>
      </Page>
    </Default>
  );
};

export default Admin;
