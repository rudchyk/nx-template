import { render } from '@testing-library/react';
import Default from './Default';

describe('Default', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <Default>
        <div>111</div>
      </Default>
    );

    expect(baseElement).toBeTruthy();
  });
});
