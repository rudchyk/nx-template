import { ReactNode, useState } from 'react';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Container from '@mui/material/Container';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import { Header, Footer } from '@client/components';
import { MainMenu } from '@client/modules';
import DefaultBar from './components/DefaultBar';
import Drawer from './components/Drawer';
export interface DefaultProps {
  children: ReactNode;
}

export const Default = ({ children }: DefaultProps) => {
  const [isBarOpened, setIsBarOpened] = useState(true);
  const toggleDrawer = () => {
    setIsBarOpened(!isBarOpened);
  };

  return (
    <Box sx={{ display: 'flex', height: '100%' }}>
      <DefaultBar position="absolute" open={isBarOpened}>
        <Header isBarOpened={isBarOpened} onToggleDrawer={toggleDrawer} />
      </DefaultBar>
      <Drawer variant="permanent" open={isBarOpened}>
        <Toolbar
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            px: [1],
          }}>
          <IconButton onClick={toggleDrawer}>
            <ChevronLeftIcon />
          </IconButton>
        </Toolbar>
        <Divider />
        <MainMenu />
      </Drawer>
      <Box
        className="container"
        sx={{
          backgroundColor: (theme) => (theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900]),
        }}>
        <Toolbar />
        <Container component="main" maxWidth="lg" sx={{ mt: 4, mb: 4, flexGrow: 1 }}>
          {children}
        </Container>
        <Footer />
      </Box>
    </Box>
  );
};

export default Default;
