import { ReactNode } from 'react';
import MuiAppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import { Header, Footer } from '@client/components';
import { Breakpoint } from '@mui/material';

export interface ErrorProps {
  children: ReactNode;
  maxWidth?: Breakpoint;
  containerSx?: any;
}

export const Simple = ({ children, containerSx, maxWidth }: ErrorProps) => {
  return (
    <Box className="container">
      <MuiAppBar position="relative">
        <Header isRightNav={false} isDrawer={false} />
      </MuiAppBar>
      <Container
        component="main"
        maxWidth={maxWidth}
        sx={{
          py: 3,
          flexGrow: 1,
          ...containerSx,
        }}>
        {children}
      </Container>
      <Footer />
    </Box>
  );
};

export default Simple;
