import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectAuthState, clearAuthError } from '@client/reducers';
import { AuthTypesEnum } from '@constants';

export function useAuthForm(type: AuthTypesEnum) {
  const dispatch = useDispatch();
  const { authError, isLoggingIn } = useSelector(selectAuthState);
  const cleanAuthError = () => {
    if (authError && authError[type]) {
      dispatch(clearAuthError(type));
    }
  };
  const getAuthError = () => {
    if (!authError[type]) {
      return undefined;
    }

    return {
      severity: 'error',
      message: authError[type],
    };
  };

  useEffect(() => {
    return () => {
      cleanAuthError();
    };
  }, []);

  return { isLoggingIn, cleanAuthError, getAuthError };
}

export default useAuthForm;
