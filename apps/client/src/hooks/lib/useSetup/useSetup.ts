import { useEffect, useState } from 'react';
import {
  selectSettingsState,
  selectAuthState,
  setLanguage,
  setThemeMode,
  authorization,
  setCookiesPolicy,
  getConfig,
  selectUserState,
  selectConfigState,
  setConfig,
} from '@client/reducers';
import { useSelector, useDispatch } from 'react-redux';
import Cookies from 'js-cookie';
import { StoredCookiesEnum, ThemeModesEnum, LanguagesKeysEnum } from '@constants';
import { useTranslation } from 'react-i18next';

export interface UseSeSetup {
  isSetupFinished: boolean;
}

export function useSetup(): UseSeSetup {
  const { i18n } = useTranslation();
  const dispatch = useDispatch();
  const [isSetupFinished, setIsSetupFinished] = useState(false);
  const { isCookiesAccepted, themeMode, language } = useSelector(selectSettingsState);
  const { token, isRemember, isLoggedIn } = useSelector(selectAuthState);
  const { user } = useSelector(selectUserState);
  const { isConfigUpdated, config } = useSelector(selectConfigState);
  const initFromCookies = () => {
    const cookies = Cookies.get();
    const storedCookiesList: string[] = Object.values(StoredCookiesEnum);
    const filteredCookiesList = Object.keys(cookies).filter((cookieItem) => !storedCookiesList.includes(cookieItem));

    storedCookiesList.forEach(async (storedCookie) => {
      const cookieValue = cookies[storedCookie];

      if (cookieValue) {
        switch (storedCookie) {
          case StoredCookiesEnum.LANGUAGE:
            if (cookieValue !== language) {
              dispatch(setLanguage(cookieValue as LanguagesKeysEnum));
            }
            break;
          case StoredCookiesEnum.THEME_MODE:
            if (cookieValue !== themeMode) {
              dispatch(setThemeMode(cookieValue as ThemeModesEnum));
            }
            break;
          case StoredCookiesEnum.TOKEN:
            dispatch(authorization(cookieValue));
            break;
          case StoredCookiesEnum.COOKIES:
            if (Boolean(cookieValue) !== isCookiesAccepted) {
              dispatch(setCookiesPolicy(Boolean(cookieValue)));
            }
            break;
          default:
            break;
        }
      }
    });

    if (filteredCookiesList.length) {
      filteredCookiesList.forEach(async (key) => {
        await Cookies.remove(key);
      });
    }
  };

  useEffect(() => {
    initFromCookies();
    console.log('Initialization...');
    setIsSetupFinished(true);
  }, []);

  useEffect(() => {
    if (isSetupFinished) {
      console.log('Setup is finished!');
    }
  }, [isSetupFinished]);

  useEffect(() => {
    if (isLoggedIn && user) {
      dispatch(getConfig(user.id));
    }
  }, [isLoggedIn]);

  useEffect(() => {
    i18n.changeLanguage(language);
    if (isCookiesAccepted) {
      Cookies.set(StoredCookiesEnum.LANGUAGE, language);
    }
  }, [language]);

  useEffect(() => {
    if (isCookiesAccepted) {
      if (themeMode === ThemeModesEnum.DARK) {
        Cookies.set(StoredCookiesEnum.THEME_MODE, themeMode);
      } else {
        Cookies.remove(StoredCookiesEnum.THEME_MODE);
      }
    }
  }, [themeMode]);

  useEffect(() => {
    if (isCookiesAccepted) {
      if (token && isRemember) {
        Cookies.set(StoredCookiesEnum.TOKEN, token);
      }

      if (!token) {
        Cookies.remove(StoredCookiesEnum.TOKEN);
      }
    }
  }, [token, isRemember]);

  useEffect(() => {
    if (isConfigUpdated && user) {
      dispatch(setConfig({ userId: user.id, data: config }));
    }
  }, [isConfigUpdated]);

  return {
    isSetupFinished,
  };
}

export default useSetup;
