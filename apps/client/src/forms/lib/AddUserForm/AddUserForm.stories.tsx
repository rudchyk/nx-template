import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import AddUserFormComponent from './AddUserForm';
import '../../../plugins/i18n';

export default {
  component: AddUserFormComponent,
  title: 'Client Forms/Add User Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('AddUserForm') }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => (
  <StoryForm isDefaultButton>
    <AddUserFormComponent {...args} />
  </StoryForm>
);

export const AddUserForm = Template.bind({});
AddUserForm.args = {};
