import { Grid } from '@mui/material';
import { t } from 'i18next';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { UIFormPasswordField, UIFormTextField, UIFormSelect, UIAlert, WithRichForm } from '@ui';
import { UserRolesEnum, AddUserFormInputsEnum } from '@constants';
import { AddUserFormInputs } from '@interfaces';
import { isDev } from '@utils';
import { faker } from '@faker-js/faker';

export const AddUserForm = WithRichForm<AddUserFormInputs>(({ onChange, onSubmit, alert, onError }) => {
  const defaultValues: AddUserFormInputs = {
    [AddUserFormInputsEnum.FIRST_NAME]: '',
    [AddUserFormInputsEnum.LAST_NAME]: '',
    [AddUserFormInputsEnum.EMAIL]: '',
    [AddUserFormInputsEnum.PASSWORD]: '',
    [AddUserFormInputsEnum.REPEATED_PASSWORD]: '',
    [AddUserFormInputsEnum.ROLE]: UserRolesEnum.USER,
  };

  if (isDev()) {
    defaultValues[AddUserFormInputsEnum.FIRST_NAME] = faker.name.firstName();
    defaultValues[AddUserFormInputsEnum.LAST_NAME] = faker.name.lastName();
    defaultValues[AddUserFormInputsEnum.EMAIL] = faker.internet.email();
    defaultValues[AddUserFormInputsEnum.PASSWORD] = '1111';
    defaultValues[AddUserFormInputsEnum.REPEATED_PASSWORD] = '1111';
  }

  const validationSchema = yup.object().shape({
    [AddUserFormInputsEnum.FIRST_NAME]: yup.string().required(),
    [AddUserFormInputsEnum.LAST_NAME]: yup.string(),
    [AddUserFormInputsEnum.EMAIL]: yup.string().required().email(),
    [AddUserFormInputsEnum.PASSWORD]: yup.string().required().min(4),
    [AddUserFormInputsEnum.REPEATED_PASSWORD]: yup
      .string()
      .min(4)
      .required()
      .test(AddUserFormInputsEnum.REPEATED_PASSWORD, t('password must be the same'), (value, context) => value === context.parent.password),
    [AddUserFormInputsEnum.ROLE]: yup.string(),
  });
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<AddUserFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });
  const list = Object.values(UserRolesEnum).map((item) => ({
    value: item,
    label: item.toUpperCase(),
  }));

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <UIFormTextField
            fieldKey={AddUserFormInputsEnum.FIRST_NAME}
            margin="none"
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('First Name')}
            autoFocus
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormTextField fieldKey={AddUserFormInputsEnum.LAST_NAME} margin="none" fullWidth label={t('Last Name')} control={control} errors={errors} />
        </Grid>
        <Grid item xs={12}>
          <UIFormTextField
            fieldKey={AddUserFormInputsEnum.EMAIL}
            margin="none"
            control={control}
            errors={errors}
            fullWidth
            label={t('Email Address')}
            autoComplete="email"
            type="email"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField
            fieldKey={AddUserFormInputsEnum.PASSWORD}
            margin="none"
            control={control}
            errors={errors}
            label={t('Password')}
            required
            fullWidth
            autoComplete="new-password"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField
            fieldKey={AddUserFormInputsEnum.REPEATED_PASSWORD}
            margin="none"
            control={control}
            errors={errors}
            label={t('Repeated password')}
            autoComplete="repeated-password"
            required
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <UIFormSelect
            control={control}
            onChange={onChange as any}
            fieldKey={AddUserFormInputsEnum.ROLE}
            margin="none"
            errors={errors}
            label={t('Role')}
            required
            fullWidth
            list={list}
          />
        </Grid>
      </Grid>
    </form>
  );
});

export default AddUserForm;
