import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import DateAdapter from '@mui/lab/AdapterMoment';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import VerifyEmailFormComponent from './VerifyEmailForm';

export default {
  component: VerifyEmailFormComponent,
  title: 'Client Forms/Verify Email Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('VerifyEmailForm') }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => (
  <LocalizationProvider dateAdapter={DateAdapter}>
    <StoryForm>
      <VerifyEmailFormComponent {...args} />
    </StoryForm>
  </LocalizationProvider>
);

export const VerifyEmailForm = Template.bind({});
VerifyEmailForm.args = {};
