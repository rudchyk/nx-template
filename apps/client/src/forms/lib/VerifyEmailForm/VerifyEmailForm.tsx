import Typography from '@mui/material/Typography';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { UIFormTextField } from '@ui';
import { useTranslation } from 'react-i18next';
import { Grid } from '@mui/material';
import { VerifyEmailFormInputsEnum } from '@constants';
import { WithRichForm, UIAlert, UIButton } from '@ui';
import { VerifyEmailFormInputs } from '@interfaces';

export const VerifyEmailForm = WithRichForm<VerifyEmailFormInputs>(({ onSubmit, onChange, onError, alert, isLoading }) => {
  const { t } = useTranslation();
  const validationSchema = yup.object().shape({
    [VerifyEmailFormInputsEnum.EMAIL]: yup.string().email().required(),
  });
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<VerifyEmailFormInputs>({
    resolver: yupResolver(validationSchema),
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert sx={{ my: 2 }} severity={alert?.severity}>
        {alert?.message}
      </UIAlert>
      <Typography sx={{ mt: 1, mb: 2 }} component="p">
        {t('Please, enter your email')}:
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField
            margin="none"
            fieldKey={VerifyEmailFormInputsEnum.EMAIL}
            control={control}
            errors={errors}
            autoFocus
            required
            fullWidth
            label={t('Email Address')}
          />
        </Grid>
        <Grid item xs={12}>
          <UIButton loading={isLoading} type="submit" fullWidth variant="contained">
            {t('Submit')}
          </UIButton>
        </Grid>
      </Grid>
    </form>
  );
});

export default VerifyEmailForm;
