import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import SignUpFormComponent from './SignUpForm';
import '../../../plugins/i18n';

export default {
  component: SignUpFormComponent,
  title: 'Client Forms/Sign Up Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('SignUpForm') }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => {
  return (
    <StoryForm>
      <SignUpFormComponent {...args} />
    </StoryForm>
  );
};

export const SignUpForm = Template.bind({});
SignUpForm.args = {
  isCookiesAccepted: true,
};
