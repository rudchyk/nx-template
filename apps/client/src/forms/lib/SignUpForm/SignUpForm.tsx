import Grid from '@mui/material/Grid';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { UIFormPasswordField, UIFormTextField, UIFormLabeledCheckbox, UIButton } from '@ui';
import { useTranslation } from 'react-i18next';
import { SignUpFormInputs } from '@interfaces';
import { SignUpFormInputsEnum } from '@constants';
import { faker } from '@faker-js/faker';
import { isDev } from '@utils';
import { WithRichForm, UIAlert } from '@ui';

export const SignUpForm = WithRichForm<SignUpFormInputs>(({ onSubmit, onChange, alert, isCookiesAccepted, onError, isLoggingIn }) => {
  const { t } = useTranslation();
  const validationSchema = yup.object().shape({
    [SignUpFormInputsEnum.FIRST_NAME]: yup.string().required(),
    [SignUpFormInputsEnum.LAST_NAME]: yup.string(),
    [SignUpFormInputsEnum.EMAIL]: yup.string().required().email(),
    [SignUpFormInputsEnum.PASSWORD]: yup.string().required().min(4),
    [SignUpFormInputsEnum.REPEATED_PASSWORD]: yup
      .string()
      .min(4)
      .required()
      .test(SignUpFormInputsEnum.REPEATED_PASSWORD, t('password must be the same'), (value, context) => value === context.parent.password),
    [SignUpFormInputsEnum.REMEMBER]: yup.boolean(),
    [SignUpFormInputsEnum.MARKETING_PROMOTIONS]: yup.boolean(),
  });
  const defaultValues: SignUpFormInputs = {
    [SignUpFormInputsEnum.FIRST_NAME]: '',
    [SignUpFormInputsEnum.LAST_NAME]: '',
    [SignUpFormInputsEnum.EMAIL]: '',
    [SignUpFormInputsEnum.PASSWORD]: '',
    [SignUpFormInputsEnum.REPEATED_PASSWORD]: '',
    [SignUpFormInputsEnum.REMEMBER]: true,
    [SignUpFormInputsEnum.MARKETING_PROMOTIONS]: false,
  };

  if (isDev()) {
    defaultValues[SignUpFormInputsEnum.FIRST_NAME] = faker.name.firstName();
    defaultValues[SignUpFormInputsEnum.LAST_NAME] = faker.name.lastName();
    defaultValues[SignUpFormInputsEnum.EMAIL] = faker.internet.email();
    defaultValues[SignUpFormInputsEnum.PASSWORD] = '1111';
    defaultValues[SignUpFormInputsEnum.REPEATED_PASSWORD] = '1111';
  }

  const {
    handleSubmit,
    control,
    setValue,
    formState: { errors },
  } = useForm<SignUpFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });

  useEffect(() => {
    if (!isCookiesAccepted) {
      setValue(SignUpFormInputsEnum.REMEMBER, false);
    }
  }, [isCookiesAccepted]);

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <UIFormTextField
            fieldKey={SignUpFormInputsEnum.FIRST_NAME}
            margin="none"
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('First Name')}
            autoFocus
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormTextField fieldKey={SignUpFormInputsEnum.LAST_NAME} margin="none" fullWidth label={t('Last Name')} control={control} errors={errors} />
        </Grid>
        <Grid item xs={12}>
          <UIFormTextField
            fieldKey={SignUpFormInputsEnum.EMAIL}
            margin="none"
            control={control}
            errors={errors}
            fullWidth
            label={t('Email Address')}
            autoComplete="email"
            type="email"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField
            fieldKey={SignUpFormInputsEnum.PASSWORD}
            margin="none"
            control={control}
            errors={errors}
            label={t('Password')}
            required
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField
            fieldKey={SignUpFormInputsEnum.REPEATED_PASSWORD}
            margin="none"
            control={control}
            errors={errors}
            label={t('Repeated password')}
            required
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          {isCookiesAccepted && <UIFormLabeledCheckbox fieldKey={SignUpFormInputsEnum.REMEMBER} errors={errors} label={t('Remember me')} control={control} />}
          <UIFormLabeledCheckbox fieldKey={SignUpFormInputsEnum.MARKETING_PROMOTIONS} errors={errors} label={t('marketingPromotions')} control={control} />
        </Grid>
        <Grid item xs={12}>
          <UIButton loading={isLoggingIn} type="submit" fullWidth variant="contained">
            {t('Sign In')}
          </UIButton>
        </Grid>
      </Grid>
    </form>
  );
});

export default SignUpForm;
