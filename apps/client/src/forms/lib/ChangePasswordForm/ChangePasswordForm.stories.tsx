import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import ChangePasswordFormComponent from './ChangePasswordForm';
import '../../../plugins/i18n';

export default {
  component: ChangePasswordFormComponent,
  title: 'Client Forms/Change Password Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('ChangePasswordForm') }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => {
  return (
    <StoryForm isDefaultButton>
      <ChangePasswordFormComponent {...args} />
    </StoryForm>
  );
};

export const ChangePasswordForm = Template.bind({});
ChangePasswordForm.args = {};
