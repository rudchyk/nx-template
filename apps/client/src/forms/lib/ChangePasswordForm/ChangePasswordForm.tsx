import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Grid } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { ChangePasswordFormInputsEnum } from '@constants';
import { ChangePasswordFormInputs } from '@interfaces';
import { UIFormPasswordField, WithRichForm, UIAlert } from '@ui';

export const ChangePasswordForm = WithRichForm<ChangePasswordFormInputs>(({ onSubmit, onChange, onError, alert }) => {
  const { t } = useTranslation();
  const defaultValues: ChangePasswordFormInputs = {
    [ChangePasswordFormInputsEnum.NEW_PASSWORD]: '',
    [ChangePasswordFormInputsEnum.OPD_PASSWORD]: '',
    [ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD]: '',
  };
  const validationSchema = yup.object().shape({
    [ChangePasswordFormInputsEnum.OPD_PASSWORD]: yup.string().min(4).required(),
    [ChangePasswordFormInputsEnum.NEW_PASSWORD]: yup.string().min(4).required(),
    [ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD]: yup
      .string()
      .min(4)
      .required()
      .test(ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD, t('password must be the same'), (value, context) => value === context.parent.password),
  });
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<ChangePasswordFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={ChangePasswordFormInputsEnum.OPD_PASSWORD}
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('Old Password')}
          />
        </Grid>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={ChangePasswordFormInputsEnum.NEW_PASSWORD}
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('New Password')}
          />
        </Grid>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD}
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('Repeated new password')}
          />
        </Grid>
      </Grid>
    </form>
  );
});

export default ChangePasswordForm;
