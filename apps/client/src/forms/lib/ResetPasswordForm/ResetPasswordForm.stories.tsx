import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import ResetPasswordFormComponent from './ResetPasswordForm';

export default {
  component: ResetPasswordFormComponent,
  title: 'Client Forms/Reset Password Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('ResetPasswordForm'), showPanel: true }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => (
  <StoryForm>
    <ResetPasswordFormComponent {...args} />
  </StoryForm>
);

export const ResetPasswordForm = Template.bind({});
ResetPasswordForm.args = {
  verifiedEmail: 'test@test.com',
};
