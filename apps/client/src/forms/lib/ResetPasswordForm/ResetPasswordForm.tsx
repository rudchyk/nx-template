import Typography from '@mui/material/Typography';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import { UIFormPasswordField } from '@ui';
import { Grid } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { ResetPasswordFormInputsEnum } from '@constants';
import { ResetPasswordFormInputs } from '@interfaces';
import { WithRichForm, UIAlert, UIButton } from '@ui';

export const ResetPasswordForm = WithRichForm<ResetPasswordFormInputs>(({ verifiedEmail, onSubmit, onChange, onError, alert, isLoading }) => {
  const { t } = useTranslation();
  const validationSchema = yup.object().shape({
    [ResetPasswordFormInputsEnum.EMAIL]: yup.string().email().required(),
    [ResetPasswordFormInputsEnum.PASSWORD]: yup.string().min(4).required(),
    [ResetPasswordFormInputsEnum.REPEATED_PASSWORD]: yup
      .string()
      .min(4)
      .required()
      .test(ResetPasswordFormInputsEnum.REPEATED_PASSWORD, t('password must be the same'), (value, context) => value === context.parent.password),
  });
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<ResetPasswordFormInputs>({
    resolver: yupResolver(validationSchema),
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <input type="hidden" value={verifiedEmail || ''} {...register(ResetPasswordFormInputsEnum.EMAIL)} />
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Typography sx={{ mt: 1, mb: 2 }} component="p">
        {t('Please, enter your new password and repeat it')}:
      </Typography>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={ResetPasswordFormInputsEnum.PASSWORD}
            control={control}
            errors={errors}
            label={t('Password')}
            fullWidth
            required
            autoComplete="current-password"
          />
        </Grid>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={ResetPasswordFormInputsEnum.REPEATED_PASSWORD}
            control={control}
            errors={errors}
            label={t('Repeats password')}
            fullWidth
            required
          />
        </Grid>
        <Grid item xs={12}>
          <UIButton loading={isLoading} type="submit" fullWidth variant="contained">
            {t('Reset')}
          </UIButton>
        </Grid>
      </Grid>
    </form>
  );
});

export default ResetPasswordForm;
