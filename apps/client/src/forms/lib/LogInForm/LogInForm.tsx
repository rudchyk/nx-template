import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { UIFormTextField, UIFormPasswordField, UIButton, UIFormLabeledCheckbox } from '@ui';
import { useForm } from 'react-hook-form';
import { Grid } from '@mui/material';
import { useTranslation } from 'react-i18next';
import { LogInFormInputs } from '@interfaces';
import { LogInFormInputsEnum } from '@constants';
import { isDev } from '@utils';
import { WithRichForm, UIAlert } from '@ui';

export const LogInForm = WithRichForm<LogInFormInputs>(({ onSubmit, onChange, alert, onResetPassword, onError, isLoggingIn }) => {
  const { t } = useTranslation();
  const defaultValues: LogInFormInputs = {
    [LogInFormInputsEnum.EMAIL]: '',
    [LogInFormInputsEnum.PASSWORD]: '',
    [LogInFormInputsEnum.REMEMBER]: true,
  };

  if (isDev()) {
    defaultValues[LogInFormInputsEnum.EMAIL] = 'sergii.rudchyk@gmail.com';
    defaultValues[LogInFormInputsEnum.PASSWORD] = '11111';
  }

  const validationSchema = yup.object().shape({
    [LogInFormInputsEnum.EMAIL]: yup.string().email().required(),
    [LogInFormInputsEnum.PASSWORD]: yup.string().min(4).required(),
    [LogInFormInputsEnum.REMEMBER]: yup.boolean(),
  });
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<LogInFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField
            margin="none"
            fieldKey={LogInFormInputsEnum.EMAIL}
            control={control}
            errors={errors}
            autoFocus
            type="email"
            required
            fullWidth
            label={t('Email Address')}
            autoComplete="email"
          />
        </Grid>
        <Grid item xs={12}>
          <UIFormPasswordField
            margin="none"
            fieldKey={LogInFormInputsEnum.PASSWORD}
            control={control}
            errors={errors}
            label={t('Password')}
            fullWidth
            required
            autoComplete="current-password"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormLabeledCheckbox control={control} fieldKey={LogInFormInputsEnum.REMEMBER} errors={errors} label={t('Remember me')} />
        </Grid>
        <Grid item xs={12} sm={6} sx={{ display: 'flex', justifyContent: 'end', alignItems: 'center' }}>
          <UIButton size="small" onClick={onResetPassword}>
            {t('Forgot password?')}
          </UIButton>
        </Grid>
        <Grid item xs={12}>
          <UIButton loading={isLoggingIn} type="submit" fullWidth variant="contained">
            {t('Log In')}
          </UIButton>
        </Grid>
      </Grid>
    </form>
  );
});

export default LogInForm;
