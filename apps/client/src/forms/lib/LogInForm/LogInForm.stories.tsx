import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import LogInFormComponent from './LogInForm';

export default {
  component: LogInFormComponent,
  title: 'Client Forms/Log In Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('LogInForm') }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => (
  <StoryForm>
    <LogInFormComponent {...args} />
  </StoryForm>
);

export const LogInForm = Template.bind({});
LogInForm.args = {
  onResetPassword: () => alert('Reset Password Action!'),
};
