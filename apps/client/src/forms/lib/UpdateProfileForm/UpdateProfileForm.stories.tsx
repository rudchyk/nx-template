import { Story, Meta } from '@storybook/react';
import { withCustomDocs, getFormSnippet } from '@storybook/utils';
import { StoryForm } from '@storybook/components';
import UpdateProfileFormComponent from './UpdateProfileForm';

export default {
  component: UpdateProfileFormComponent,
  title: 'Client Forms/Update Profile Form',
  parameters: {
    ...withCustomDocs({ code: getFormSnippet('UpdateProfileForm'), showPanel: true }),
  },
  argTypes: {},
} as Meta;

const Template: Story = (args: any) => (
  <StoryForm>
    <UpdateProfileFormComponent {...args} />
  </StoryForm>
);

export const UpdateProfileForm = Template.bind({});
UpdateProfileForm.args = {
  user: {
    firstName: 'Oran',
    lastName: 'Gutmann',
    phone: '+380 (34) 534 53 45',
    dob: 544959532000,
  },
};
