import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect } from 'react';
import { Button, Grid, Stack } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { UpdateProfileFormInputsEnum } from '@constants';
import { UpdateProfileFormInputs } from '@interfaces';
import { UIFormTextField, UIFormPhoneNumberField, UIFormDateTimePickerField, UIFormNumberField } from '@ui';
import SaveIcon from '@mui/icons-material/Save';
import { WithRichForm } from '@ui';

export const UpdateProfileForm = WithRichForm<UpdateProfileFormInputs>(({ onSubmit, onChange, onError, user, isUserUpdated }) => {
  const { t } = useTranslation();
  const getDefaultValues = (): UpdateProfileFormInputs => ({
    [UpdateProfileFormInputsEnum.FIRST_NAME]: user ? user[UpdateProfileFormInputsEnum.FIRST_NAME] : '',
    [UpdateProfileFormInputsEnum.LAST_NAME]: user ? user[UpdateProfileFormInputsEnum.LAST_NAME] : '',
    [UpdateProfileFormInputsEnum.PHONE]: user && user[UpdateProfileFormInputsEnum.PHONE] ? user[UpdateProfileFormInputsEnum.PHONE] : '',
    [UpdateProfileFormInputsEnum.DOB]: user && user[UpdateProfileFormInputsEnum.DOB] ? user[UpdateProfileFormInputsEnum.DOB] : null,
  });
  const validationSchema = yup.object().shape({
    [UpdateProfileFormInputsEnum.FIRST_NAME]: yup.string(),
    [UpdateProfileFormInputsEnum.LAST_NAME]: yup.string(),
    [UpdateProfileFormInputsEnum.PHONE]: yup.string(),
    [UpdateProfileFormInputsEnum.DOB]: yup.string().nullable(),
  });
  const {
    register,
    handleSubmit,
    reset,
    control,
    formState: { errors, isDirty },
  } = useForm<UpdateProfileFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues: getDefaultValues(),
  });

  useEffect(() => {
    if (isUserUpdated) {
      reset(getDefaultValues());
    }
  }, [isUserUpdated]);

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <UIFormTextField
            margin="none"
            fieldKey={UpdateProfileFormInputsEnum.FIRST_NAME}
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('First Name')}
          />
        </Grid>
        <Grid item xs={6}>
          <UIFormTextField
            margin="none"
            fieldKey={UpdateProfileFormInputsEnum.LAST_NAME}
            control={control}
            errors={errors}
            required
            fullWidth
            label={t('Last Name')}
          />
        </Grid>
        <Grid item xs={6}>
          <UIFormPhoneNumberField
            defaultCountry="ua"
            margin="none"
            fieldKey={UpdateProfileFormInputsEnum.PHONE}
            errors={errors}
            control={control}
            onChange={onChange as any}
            fullWidth
            label={t('Phone')}
          />
        </Grid>
        <Grid item xs={6}>
          <UIFormDateTimePickerField
            fieldKey={UpdateProfileFormInputsEnum.DOB}
            errors={errors}
            control={control}
            onChange={onChange as any}
            fullWidth
            margin="none"
            label={t('Date of birth')}
          />
        </Grid>
        <Grid item xs={12} sx={{ textAlign: 'right' }}>
          <Stack direction="row" justifyContent="end" alignItems="center">
            <Button disabled={!isDirty} type="submit" variant="contained" startIcon={<SaveIcon />}>
              {t('Save')}
            </Button>
          </Stack>
        </Grid>
      </Grid>
    </form>
  );
});

export default UpdateProfileForm;
