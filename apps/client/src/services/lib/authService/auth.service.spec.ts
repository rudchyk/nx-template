import { authInit } from './auth.service';

describe('auth service', () => {
  it('should work', () => {
    expect(authInit()).toHaveBeenCalled();
  });
});
