import store from '@client/store';
import { signOut, clearUser } from '@client/reducers';
import Cookies from 'js-cookie';
import { StoredCookiesEnum } from '@constants';

export const logout = () => {
  const { dispatch } = store;

  dispatch(signOut());
  dispatch(clearUser());

  if (Cookies.get(StoredCookiesEnum.TOKEN)) {
    Cookies.remove(StoredCookiesEnum.TOKEN);
  }
};
