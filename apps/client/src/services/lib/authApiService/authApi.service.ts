import axios, { AxiosResponse } from 'axios';
import { APIRoutesEnum } from '@constants';
import { AuthAPIData, VerifyEmailAPIData, ResetPasswordAPIData } from '@interfaces';

export const authApi = (token: string): Promise<AxiosResponse<AuthAPIData>> => {
  return axios.post(APIRoutesEnum.AUTH, {}, { headers: { authorization: token } });
};

export const logInApi = (token: string): Promise<AxiosResponse<AuthAPIData>> => {
  return axios.post(APIRoutesEnum.LOGIN, { token });
};

export const signUpApi = (token: string): Promise<AxiosResponse<AuthAPIData>> => {
  return axios.post(APIRoutesEnum.REGISTER, { token });
};

export const forgotPasswordApi = (email: string): Promise<AxiosResponse<VerifyEmailAPIData>> => {
  return axios.post(APIRoutesEnum.FORGOT_PASSWORD, { email });
};

export const resetPasswordApi = (token: string): Promise<AxiosResponse<ResetPasswordAPIData>> => {
  return axios.post(APIRoutesEnum.RESET_PASSWORD, { token });
};
