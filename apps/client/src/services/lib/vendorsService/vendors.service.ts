import axios, { AxiosResponse } from 'axios';
import { PostAPIInterface } from '@interfaces';

export const postsApiDefaultLimit = 10;

export const getPostsApi = (limit = postsApiDefaultLimit): Promise<AxiosResponse<PostAPIInterface[]>> => {
  const path = `https://jsonplaceholder.typicode.com/posts/?_limit=${limit}`;
  return axios.get(path);
};
