import { getPostsApi } from './vendors.service';

describe('api test', () => {
  it('getPostsApi should work', () => {
    expect(getPostsApi()).toHaveBeenCalled();
  });
});
