import axios, { AxiosResponse } from 'axios';
import { UpdateConfigResponseAPIData, GetConfigResponseAPIData, ConfigData } from '@interfaces';
import { APIRoutesEnum, ConfigPathParamsEnum } from '@constants';
import { getRouteUrl } from '@client/utils';

export interface ConfigParams {
  [ConfigPathParamsEnum.userId]: string;
}

const configPath = getRouteUrl<ConfigParams>(APIRoutesEnum.CONFIG);

export const getConfigAPI = (userId: string): Promise<AxiosResponse<GetConfigResponseAPIData>> => {
  return axios.get(configPath({ userId }));
};

export interface UpdateConfigData {
  userId: string;
  data: ConfigData;
}

export const setConfigAPI = (data: UpdateConfigData): Promise<AxiosResponse<UpdateConfigResponseAPIData>> => {
  return axios.put(configPath({ userId: data.userId }), { data: data.data });
};
