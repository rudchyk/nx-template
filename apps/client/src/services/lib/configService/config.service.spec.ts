import { configInit } from './config.service';

describe('config service', () => {
  it('should work', () => {
    expect(configInit()).toHaveBeenCalled();
  });
});
