import { getUsersApi } from './usersApi.service';

describe('users service', () => {
  it('should work', () => {
    expect(getUsersApi()).toHaveBeenCalled();
  });
});
