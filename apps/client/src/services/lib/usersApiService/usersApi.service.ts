import axios, { AxiosResponse } from 'axios';
import { APIRoutesEnum, UserPathParamsEnum } from '@constants';
import { getRouteUrl } from '@client/utils';
import {
  DeleteUsersResponseAPIData,
  UsersResponseAPIData,
  DeleteItemsRequestData,
  DeleteUserResponseAPIData,
  UpdateUserResponseAPIData,
  AddUserAPIData,
  ChangeUserPasswordResponseAPIData,
} from '@interfaces';

export const getUsersApi = (): Promise<AxiosResponse<UsersResponseAPIData>> => {
  return axios.get(APIRoutesEnum.USERS);
};

export const deleteUsersApi = (data: DeleteItemsRequestData): Promise<AxiosResponse<DeleteUsersResponseAPIData>> => {
  return axios.delete(APIRoutesEnum.USERS, { data });
};

export const addUserApi = (token: string): Promise<AxiosResponse<AddUserAPIData>> => {
  return axios.post(APIRoutesEnum.USERS, { token });
};

export interface UserParams {
  [UserPathParamsEnum.id]: string;
}

export interface UpdateUserData {
  id: string;
  data: {
    [key: string]: string;
  };
}

const userPath = getRouteUrl<UserParams>(APIRoutesEnum.USER);

export const updateUserApi = (data: UpdateUserData): Promise<AxiosResponse<UpdateUserResponseAPIData>> => {
  return axios.put(userPath({ id: data.id }), { data: data.data });
};

const changeUserPasswordPath = getRouteUrl<UserParams>(APIRoutesEnum.USER_CHANGE_PASSWORD);

export interface ChangeUserPasswordData {
  id: string;
  token: string;
}

export const changeUserPasswordApi = ({ id, token }: ChangeUserPasswordData): Promise<AxiosResponse<ChangeUserPasswordResponseAPIData>> => {
  return axios.post(changeUserPasswordPath({ id }), { token });
};

export const deleteUserApi = (id: string): Promise<AxiosResponse<DeleteUserResponseAPIData>> => {
  return axios.delete(userPath({ id }));
};
