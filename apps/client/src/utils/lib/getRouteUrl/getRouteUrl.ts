export const getRouteUrl = <T>(path: string) => {
  return (params: T) => {
    const paramIdentifier = ':';

    if (path.indexOf(paramIdentifier) === -1) {
      return path;
    }

    let url = path;

    for (const paramKey in params) {
      url = url.replace(`${paramIdentifier}${paramKey}`, (params as any)[paramKey]);
    }

    return url;
  };
};

export default getRouteUrl;
