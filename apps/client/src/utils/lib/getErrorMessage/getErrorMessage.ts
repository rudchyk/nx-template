export const getErrorMessage = (err: any): string => {
  if (err?.response?.data?.message) {
    return err.response.data.message;
  }

  return err.message;
};

export default getErrorMessage;
