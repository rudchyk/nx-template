import { useSelector } from 'react-redux';
import { ClientRoutesEnum, UserRolesEnum, ClientRouteProtectionTypesEnum, AppRouteOptions } from '@constants';
import { ClientRouteProtection } from '@interfaces';
import { selectAuthState, selectUserState } from '@client/reducers';
import { Navigate } from 'react-router-dom';
import { useFeatures } from '@paralleldrive/react-feature-toggles';

export interface ProtectedRouteProps {
  protection?: ClientRouteProtection;
  options?: AppRouteOptions;
  children: JSX.Element;
}

export const ProtectedRoute = ({ protection, children, options }: ProtectedRouteProps) => {
  const { isLoggedIn } = useSelector(selectAuthState);
  const { user } = useSelector(selectUserState);
  const features = useFeatures();

  if (protection === UserRolesEnum.USER && !isLoggedIn) {
    return <Navigate to={ClientRoutesEnum.SIGNIN} replace />;
  }

  if (protection === ClientRouteProtectionTypesEnum.FEATURE && (!user || (user && !features.includes(options?.feature || '')))) {
    return <Navigate to={ClientRoutesEnum.HOME} replace />;
  }

  if (protection === UserRolesEnum.ADMIN && (!user || user.role !== UserRolesEnum.ADMIN)) {
    return <Navigate to={ClientRoutesEnum.HOME} replace />;
  }

  return children;
};

export default ProtectedRoute;
