import { useTranslation } from 'react-i18next';
import { UIFooter } from '@ui';
import styles from './Footer.module.sass';

export const Footer = () => {
  const { t } = useTranslation();

  return <UIFooter text={t('nxTemplate')} TypographyProps={{ className: styles.footer }} />;
};

export default Footer;
