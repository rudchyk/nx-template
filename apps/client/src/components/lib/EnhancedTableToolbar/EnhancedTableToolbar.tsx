import { IconButton, Toolbar, Tooltip, Typography } from '@mui/material';
import { ReactElement } from 'react';

interface EnhancedTableToolbarProps {
  count: number;
  title: string;
  icon: ReactElement;
  onClick: () => void;
}

export const EnhancedTableToolbar = (props: EnhancedTableToolbarProps) => {
  return (
    <Toolbar
      sx={{
        display: props.count > 0 ? 'flex' : 'none',
        justifyContent: 'space-between',
        background: (theme) => theme.palette.grey[200],
        borderRadius: '5px 5px 0 0',
      }}>
      <Typography color="inherit" variant="h5" component="div">
        {props.count} {props.title}
      </Typography>
      <Tooltip title={props.title}>
        <IconButton onClick={props.onClick}>{props.icon}</IconButton>
      </Tooltip>
    </Toolbar>
  );
};

export default EnhancedTableToolbar;
