import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import List from '@mui/material/List';
import { MouseEvent } from 'react';
import { Link as RouteLink } from 'react-router-dom';
import ListItemButton from '@mui/material/ListItemButton';

export interface MenuItem {
  isHidden?: boolean;
  text: string;
  to?: string;
  icon?: any;
  disabled?: boolean;
  onClick?: (event: MouseEvent<HTMLButtonElement>) => void;
}

export interface MenuItemProps {
  item: MenuItem;
}

const MenuItem = ({ item }: MenuItemProps) => {
  const props: any = {};

  if (item.to) {
    props.component = RouteLink;
    props.to = item.to;
  }

  if (item.onClick) {
    props.onClick = item.onClick;
  }

  return (
    <ListItemButton disabled={item.disabled} {...props}>
      {item.icon && (
        <ListItemIcon>
          <item.icon />
        </ListItemIcon>
      )}
      <ListItemText primary={item.text} />
    </ListItemButton>
  );
};

export interface MenuProps {
  items: MenuItem[];
}

export const Menu = ({ items }: MenuProps) => {
  return <List component="nav">{items.map((item) => !item.isHidden && <MenuItem item={item} key={item.text} />)}</List>;
};

export default Menu;
