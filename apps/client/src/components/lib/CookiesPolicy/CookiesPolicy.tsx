import { useDispatch, useSelector } from 'react-redux';
import { setCookiesPolicy } from '@client/reducers';
import { Trans, useTranslation } from 'react-i18next';
import { UICookiesPolicy, UILink } from '@ui';
import { selectSettingsState, selectAuthState, setLanguage, setThemeMode } from '@client/reducers';
import Cookies from 'js-cookie';
import { logout } from '@client/services';
import { StoredCookiesEnum, LanguagesKeysEnum, ThemeModesEnum, defaultLanguage, defaultThemeMode, ClientRoutesEnum } from '@constants';

export const CookiesPolicyAlert = () => {
  const dispatch = useDispatch();
  const { themeMode, language, isCookiesAccepted } = useSelector(selectSettingsState);
  const { token } = useSelector(selectAuthState);
  const { t } = useTranslation();
  const removeAllCookies = () => {
    Object.values(StoredCookiesEnum).forEach(async (storedCookie) => {
      switch (storedCookie) {
        case StoredCookiesEnum.LANGUAGE:
          if (language !== defaultLanguage) {
            dispatch(setLanguage(defaultLanguage as LanguagesKeysEnum));
          }
          break;
        case StoredCookiesEnum.THEME_MODE:
          if (themeMode !== defaultThemeMode) {
            dispatch(setThemeMode(defaultThemeMode as ThemeModesEnum));
          }
          break;
        case StoredCookiesEnum.TOKEN:
          if (token) {
            logout();
          }
          break;
        default:
          break;
      }
      Cookies.remove(storedCookie);
    });
  };
  const acceptCookies = () => {
    const cookies = Cookies.get();

    Object.values(StoredCookiesEnum).forEach(async (storedCookie) => {
      if (!cookies[storedCookie]) {
        switch (storedCookie) {
          case StoredCookiesEnum.LANGUAGE:
            await Cookies.set(storedCookie, language);
            break;
          case StoredCookiesEnum.THEME_MODE:
            await Cookies.set(storedCookie, themeMode);
            break;
          case StoredCookiesEnum.TOKEN:
            if (token) {
              await Cookies.set(storedCookie, token);
            }
            break;
          case StoredCookiesEnum.COOKIES:
            await Cookies.set(storedCookie, 'true');
            break;
          default:
            break;
        }
      }
    });
  };
  const onDecline = () => {
    dispatch(setCookiesPolicy(false));
    removeAllCookies();
  };
  const onAccept = () => {
    dispatch(setCookiesPolicy(true));
    acceptCookies();
  };

  if (isCookiesAccepted) {
    return null;
  }

  return (
    <UICookiesPolicy acceptButtonText={t('Accept')} declineButtonText={t('Decline')} onDecline={onDecline} onAccept={onAccept} title={t('Cookies Policy')}>
      <Trans i18nKey="Cookies Policy Content" components={[<UILink color="inherit" to={ClientRoutesEnum.COOKIES} title={t('Cookies Policy')} />]} />
    </UICookiesPolicy>
  );
};

export default CookiesPolicyAlert;
