import { UIButton, UIHeader, UILocalizationSwitcher } from '@ui';
import { useTranslation } from 'react-i18next';
import { selectSettingsState } from '@client/reducers';
import { useDispatch, useSelector } from 'react-redux';
import { setLanguage, selectAuthState } from '@client/reducers';
import LoginIcon from '@mui/icons-material/Login';
import { ClientRoutesEnum, LanguagesKeysEnum } from '@constants';
import Stack from '@mui/material/Stack';
import { ThemeModeSwitcher } from '@client/components';
import { AccountMenu } from '@client/modules';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import styles from './Header.module.sass';

export interface HeaderProps {
  isBarOpened?: boolean;
  onToggleDrawer?: () => void;
  isRightNav?: boolean;
  isDrawer?: boolean;
}

export const Header = ({ isBarOpened, onToggleDrawer, isDrawer = true, isRightNav = true }: HeaderProps) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { language } = useSelector(selectSettingsState);
  const { isLoggedIn } = useSelector(selectAuthState);

  const changeLanguage = (language: string) => dispatch(setLanguage(language as LanguagesKeysEnum));
  const languagesList = Object.values(LanguagesKeysEnum);
  const leftBar = (
    <IconButton
      edge="start"
      color="inherit"
      aria-label="open drawer"
      onClick={onToggleDrawer}
      sx={{
        mr: '36px',
        ...(isBarOpened && { display: 'none' }),
      }}>
      <MenuIcon />
    </IconButton>
  );

  return (
    <UIHeader
      sx={{
        pr: '24px',
      }}
      title={t('nxTemplate')}
      leftBar={isDrawer ? leftBar : null}>
      <Stack direction="row" alignItems="center" spacing={1}>
        <UILocalizationSwitcher className={styles.localization} languagesList={languagesList} onChangeLanguage={changeLanguage} language={language} />
        <ThemeModeSwitcher />
        {isRightNav &&
          (isLoggedIn ? (
            <AccountMenu />
          ) : (
            <UIButton icon title={t('Log in')} color="inherit" to={ClientRoutesEnum.SIGNIN}>
              <LoginIcon />
            </UIButton>
          ))}
      </Stack>
    </UIHeader>
  );
};

export default Header;
