import { UIButton } from '@ui';
import { useTranslation } from 'react-i18next';
import { ClientRoutesEnum } from '@constants';
import HomeIcon from '@mui/icons-material/Home';

export const HomeButton = ({ ...other }) => {
  const { t } = useTranslation();

  return (
    <UIButton size="small" to={ClientRoutesEnum.HOME} startIcon={<HomeIcon />} {...other}>
      {t('homeButton')}
    </UIButton>
  );
};

export default HomeButton;
