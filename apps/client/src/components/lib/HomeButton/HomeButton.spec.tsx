import { render } from '@testing-library/react';
import HomeButton from './HomeButton';

describe('HomeBtn', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<HomeButton />);

    expect(baseElement).toBeTruthy();
  });
});
