import { render } from '@testing-library/react';
import DialogDefault from './DialogDefault';

describe('Dialog', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <DialogDefault
        onButtonPrimaryClick={() => console.log('PrimaryClick')}
        title="hello"
        open={false}
        onDialogClose={() => console.log('Close')}
        trigger={<button>Trigger</button>}
      />
    );

    expect(baseElement).toBeTruthy();
  });
});
