import { Button } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import DialogDefaultComponent, { DialogDefaultProps } from './DialogDefault';
import { Docs } from '@storybook/components';

export default {
  component: DialogDefaultComponent,
  title: 'Client Components/Dialog Default',
  parameters: {
    layout: 'centered',
  },
} as Meta;

const code = `
import { useState } from 'react';
import { DialogDefault } from '@client/components';

const App = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };
  const trigger = (
    <Button color="success" variant="contained" onClick={handleDialogOpen}>
      Open default dialog
    </Button>
  );

  return (
    <DialogDefault
    title="Hello world!!!"
    open={isDialogOpen}
    onDialogClose={handleDialogClose}
    onButtonPrimaryClick={handleDialogClose}
    trigger={trigger}>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis similique quae labore est eius?
  </DialogDefault>
  );
};
`;

const Template: Story<DialogDefaultProps> = (args, cx) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };
  const trigger = (
    <Button color="success" variant="contained" onClick={handleDialogOpen}>
      Open default dialog
    </Button>
  );

  cx.parameters.docs.page = () => <Docs onLoad={handleDialogClose} code={code} />;

  return <DialogDefaultComponent {...args} open={isDialogOpen} onDialogClose={handleDialogClose} onButtonPrimaryClick={handleDialogClose} trigger={trigger} />;
};

export const DialogDefault = Template.bind({});
DialogDefault.args = {
  title: 'Hello world!!!',
  children: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis similique quae labore est eius?`,
};
