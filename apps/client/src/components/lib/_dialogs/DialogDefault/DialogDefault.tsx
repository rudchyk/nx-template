import { UIDialog, UIDialogProps } from '@ui';
import { useTranslation } from 'react-i18next';

export type DialogDefaultProps = UIDialogProps;

export const DialogDefault = ({ ...other }: DialogDefaultProps) => {
  const { t } = useTranslation();

  return (
    <UIDialog
      buttonCancelText={t('Cancel')}
      buttonCloseLabel={t('close')}
      {...other}
      dialogContentProps={{
        dividers: true,
        ...other.dialogContentProps,
      }}
    />
  );
};

export default DialogDefault;
