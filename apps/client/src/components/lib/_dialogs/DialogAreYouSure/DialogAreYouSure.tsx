import { useTranslation } from 'react-i18next';
import { UIDialog, UIDialogProps } from '@ui';

export interface DialogAreYouSureProps extends UIDialogProps {
  onAgree: () => void;
  onDisagree?: () => void;
  buttonDisagreeText?: string;
  buttonAgreeText?: string;
}

export const DialogAreYouSure = ({ title, onAgree, onDisagree, buttonDisagreeText, buttonAgreeText, ...other }: DialogAreYouSureProps) => {
  const { t } = useTranslation();

  return (
    <UIDialog
      title={title || t('Are you sure?')}
      {...other}
      onButtonCancelClick={onDisagree}
      buttonCancelText={buttonDisagreeText || t('Disagree')}
      onButtonPrimaryClick={onAgree}
      buttonPrimaryText={buttonAgreeText || t('Agree')}
    />
  );
};

export default DialogAreYouSure;
