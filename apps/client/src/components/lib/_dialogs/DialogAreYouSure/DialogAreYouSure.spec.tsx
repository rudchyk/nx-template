import { render } from '@testing-library/react';
import DialogAreYouSure from './DialogAreYouSure';

describe('AreYouSure', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <DialogAreYouSure
        open={false}
        onDialogClose={() => console.log('Close')}
        trigger={<button>Trigger</button>}
        onDisagree={() => console.log('Disagree')}
        onAgree={() => console.log('Agree')}
      />
    );

    expect(baseElement).toBeTruthy();
  });
});
