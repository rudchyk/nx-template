import { Button } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { Docs } from '@storybook/components';
import { useState } from 'react';
import DialogAreYouSureComponent, { DialogAreYouSureProps } from './DialogAreYouSure';

const code = `
import { DialogAreYouSure } from '@client/components';

const App = () => {
  const [isDialogAreYouSureOpen, setIsDialogAreYouSureOpen] = useState(false);
  const handleDialogAreYouSureOpen = () => {
    setIsDialogAreYouSureOpen(true);
  };
  const handleDialogAreYouSureClose = () => {
    setIsDialogAreYouSureOpen(false);
  };
  const handleDisagreeAction = () => {
    alert('Disagree');
    handleDialogAreYouSureClose();
  };
  const handleAgreeAction = () => {
    alert('Agree');
    handleDialogAreYouSureClose();
  };
  const trigger = (
    <Button color="error" variant="contained" onClick={handleDialogAreYouSureOpen}>
      Delete
    </Button>
  );

  return (
    <DialogAreYouSure
      open={isDialogAreYouSureOpen}
      onDialogClose={handleDialogAreYouSureClose}
      trigger={trigger}
      onDisagree={handleDisagreeAction}
      onAgree={handleAgreeAction}
    />
  );
};
`;

export default {
  component: DialogAreYouSureComponent,
  title: 'Client Components/Dialog Are You Sure',
  parameters: {
    layout: 'centered',
  },
} as Meta;

const Template: Story<DialogAreYouSureProps> = (args, cx) => {
  const [isDialogAreYouSureOpen, setIsDialogAreYouSureOpen] = useState(false);
  const handleDialogAreYouSureOpen = () => {
    setIsDialogAreYouSureOpen(true);
  };
  const handleDialogAreYouSureClose = () => {
    setIsDialogAreYouSureOpen(false);
  };
  const handleDisagreeAction = () => {
    alert('Disagree');
    handleDialogAreYouSureClose();
  };
  const handleAgreeAction = () => {
    alert('Agree');
    handleDialogAreYouSureClose();
  };
  const trigger = (
    <Button color="error" variant="contained" onClick={handleDialogAreYouSureOpen}>
      Delete
    </Button>
  );

  cx.parameters.docs.page = () => <Docs onLoad={handleDialogAreYouSureClose} code={code} />;

  return (
    <DialogAreYouSureComponent
      {...args}
      open={isDialogAreYouSureOpen}
      onDialogClose={handleDialogAreYouSureClose}
      trigger={trigger}
      onDisagree={handleDisagreeAction}
      onAgree={handleAgreeAction}
    />
  );
};

export const DialogAreYouSure = Template.bind({});
DialogAreYouSure.args = {
  title: 'Are you sure?',
  buttonDisagreeText: 'Disagree',
  buttonAgreeText: 'Agree',
};
