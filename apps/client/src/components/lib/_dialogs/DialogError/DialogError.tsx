import { useTranslation } from 'react-i18next';
import { UIDialog, UIDialogProps, UIFlexBox, UICodeBlock } from '@ui';
import ErrorIcon from '@mui/icons-material/Error';
import GppBadIcon from '@mui/icons-material/GppBad';
import PublicIcon from '@mui/icons-material/Public';
import { Typography } from '@mui/material';

export enum ErrorIconsEnum {
  error = 'error',
  type = 'type',
  globe = 'globe',
}

type ErrorIcon = keyof typeof ErrorIconsEnum | null;

export interface DialogErrorData {
  name: string;
  message: string;
  status?: number;
  stack?: string;
  icon?: ErrorIcon;
}

export interface DialogErrorProps extends UIDialogProps {
  buttonReloadText?: string;
  errorData: DialogErrorData | null;
}

export const DialogError = ({ buttonReloadText, onButtonPrimaryClick, errorData, children, ...other }: DialogErrorProps) => {
  const { t } = useTranslation();
  const handleReload = () => {
    onButtonPrimaryClick && onButtonPrimaryClick();
    window.location.reload();
  };
  const getErrorIcon = () => {
    if (!errorData) {
      return;
    }

    const { icon } = errorData;

    if (icon === null) {
      return null;
    }

    let IconElement;

    switch (icon) {
      case ErrorIconsEnum.type:
        IconElement = GppBadIcon;
        break;
      case ErrorIconsEnum.globe:
        IconElement = PublicIcon;
        break;
      case ErrorIconsEnum.error:
      default:
        IconElement = ErrorIcon;
        break;
    }

    return (
      <UIFlexBox sx={{ my: 2 }}>
        <IconElement fontSize="large" color="error" />
      </UIFlexBox>
    );
  };

  return (
    <UIDialog
      title={errorData?.name}
      isButtonCancel={false}
      isButtonClose={false}
      buttonPrimaryText={buttonReloadText || t('Reload')}
      {...other}
      dialogTitleProps={{
        sx: {
          justifyContent: 'center',
        },
      }}
      dialogContentProps={{
        dividers: true,
        ...other.dialogContentProps,
      }}
      onButtonPrimaryClick={handleReload}>
      {getErrorIcon()}
      {errorData && (
        <>
          {errorData.status && (
            <Typography sx={{ mb: 2 }} variant="h5" component="p" align="center">
              {errorData.status}
            </Typography>
          )}
          <Typography sx={{ mb: errorData.stack ? 2 : null }} variant="body1" component="p" align="center">
            {errorData.message}
          </Typography>
          {errorData.stack && <UICodeBlock code={errorData.stack} lines={false} />}
        </>
      )}
      {children}
    </UIDialog>
  );
};

export default DialogError;
