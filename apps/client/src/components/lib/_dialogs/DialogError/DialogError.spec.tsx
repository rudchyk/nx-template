import { render } from '@testing-library/react';
import DialogError from './DialogError';

describe('DialogForm', () => {
  it('should render successfully', () => {
    const errorData = {
      name: 'Error',
      message: 'Error message',
    };
    const { baseElement } = render(<DialogError errorData={errorData} open={false} />);

    expect(baseElement).toBeTruthy();
  });
});
