import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import { Stack } from '@mui/material';
import { UIButton } from '@ui';
import axios, { AxiosError } from 'axios';
import { Docs } from '@storybook/components';
import DialogErrorComponent, { DialogErrorProps, DialogErrorData } from './DialogError';

export default {
  component: DialogErrorComponent,
  title: 'Client Components/Dialog Error',
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    errorData: {
      control: {
        type: null,
      },
    },
  },
} as Meta;

const code = `
import { useState } from 'react';
import axios, { AxiosError } from 'axios';
import { DialogError } from '@client/components';

const App = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogErrorData, setDialogErrorData] = useState<DialogErrorData | null>(null);
  const handleDialogOpen = (dialogErrorData: DialogErrorData) => {
    setIsDialogOpen(true);
    setDialogErrorData(dialogErrorData);
  };
  const get404 = async () => {
    try {
      await axios.get('/sdfsdf');
    } catch (error) {
      const err = error as AxiosError;
      const { status, stack, message, name }: any = err.toJSON();

      handleDialogOpen({ status, message, stack, name });
    }
  };
  const getApiError = async () => {
    try {
      await axios.get('https://httpstat.us/500');
    } catch (error) {
      const err = error as AxiosError;
      const { status, message, name }: any = err.toJSON();

      handleDialogOpen({ status, message, name, icon: 'globe' });
    }
  };
  const getTypeError = () => {
    try {
      const box = (isDialogOpen as any).toJSON();
    } catch (error) {
      const { message, name, stack } = error as Error;

      handleDialogOpen({ message, stack, name, icon: 'type' });
    }
  };
  const getError = () => {
    try {
      throw new Error('Short error message');
    } catch (error) {
      const { message, name } = error as Error;

      handleDialogOpen({ message, name, icon: null });
    }
  };

  return (
    <>
      <Stack spacing={2}>
        <UIButton color="error" variant="contained" onClick={get404}>
          full 404 error
        </UIButton>
        <UIButton color="info" variant="contained" onClick={getApiError}>
          Api error without stack
        </UIButton>
        <UIButton color="warning" variant="contained" onClick={getTypeError}>
          TypeError without status
        </UIButton>
        <UIButton color="inherit" variant="contained" onClick={getError}>
          Error without stack and status
        </UIButton>
      </Stack>
      <DialogError errorData={dialogErrorData} open={isDialogOpen} />
    </>
  );
};
`;

const Template: Story<DialogErrorProps> = (args, cx) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogErrorData, setDialogErrorData] = useState<DialogErrorData | null>(null);
  const handleDialogOpen = (dialogErrorData: DialogErrorData) => {
    setIsDialogOpen(true);
    setDialogErrorData(dialogErrorData);
  };
  const get404 = async () => {
    try {
      await axios.get('/sdfsdf');
    } catch (error) {
      const err = error as AxiosError;
      const { status, stack, message, name }: any = err.toJSON();

      handleDialogOpen({ status, message, stack, name });
    }
  };
  const getApiError = async () => {
    try {
      await axios.get('https://httpstat.us/500');
    } catch (error) {
      const err = error as AxiosError;
      const { status, message, name }: any = err.toJSON();

      handleDialogOpen({ status, message, name, icon: 'globe' });
    }
  };
  const getTypeError = () => {
    try {
      const box = (isDialogOpen as any).toJSON();
    } catch (error) {
      const { message, name, stack } = error as Error;

      handleDialogOpen({ message, stack, name, icon: 'type' });
    }
  };
  const getError = () => {
    try {
      throw new Error('Short error message');
    } catch (error) {
      const { message, name } = error as Error;

      handleDialogOpen({ message, name, icon: null });
    }
  };

  cx.parameters.docs.page = () => <Docs onLoad={() => setIsDialogOpen(false)} code={code} />;

  return (
    <>
      <Stack spacing={2}>
        <UIButton color="error" variant="contained" onClick={get404}>
          full 404 error
        </UIButton>
        <UIButton color="info" variant="contained" onClick={getApiError}>
          Api error without stack
        </UIButton>
        <UIButton color="warning" variant="contained" onClick={getTypeError}>
          TypeError without status
        </UIButton>
        <UIButton color="inherit" variant="contained" onClick={getError}>
          Error without stack and status
        </UIButton>
      </Stack>
      <DialogErrorComponent errorData={dialogErrorData} open={isDialogOpen} />
    </>
  );
};

export const DialogError = Template.bind({});
DialogError.args = {
  buttonReloadText: 'Reload',
};
