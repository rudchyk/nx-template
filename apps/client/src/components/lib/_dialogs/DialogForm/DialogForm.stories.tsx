import { Story, Meta } from '@storybook/react';
import { Docs } from '@storybook/components';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';
import { Grid } from '@mui/material';
import { SubmitHandler, useForm } from 'react-hook-form';
import { UIFormTextField, UIButton, WithRichForm } from '@ui';
import DialogFormComponent, { DialogFormProps } from './DialogForm';

export default {
  component: DialogFormComponent,
  title: 'Client Components/Dialog Form',
  parameters: {
    layout: 'centered',
  },
  argTypes: {
    form: {
      control: {
        type: null,
      },
      description: 'The form element',
    },
  },
} as Meta;

const code = `
import { useState } from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Grid } from '@mui/material';
import { DialogForm, UIFormTextField, UIButton, WithRichForm } from '@client/components';

interface TestFormInputs {
  nama: string;
}
interface TestFormProps {
  onSubmit: (formData: TestFormInputs) => void;
  submitTrigger?: ReactElement;
}

const TestForm = WithRichForm<TestFormInputs>(({ onSubmit }) => {
  const validationSchema = yup.object().shape({
    name: yup.string().required(),
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<TestFormInputs>({
    resolver: yupResolver(validationSchema),
  });
  const onFormSubmit: SubmitHandler<TestFormInputs> = (formData) => {
    onSubmit(formData);
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)} noValidate>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField margin="none" fieldKey="name" register={register} errors={errors} autoFocus required fullWidth label="User name" />
        </Grid>
      </Grid>
    </form>
  );
});

const App = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };
  const handleFormSubmit = (formData: TestFormInputs) => {
    // TODO
    handleDialogClose();
  };
  const trigger = (
    <Button color="secondary" variant="contained" onClick={handleDialogOpen}>
      Add user
    </Button>
  );

  return (
    <DialogFormComponent
      title="Add User"
      form={<TestForm onSubmit={handleFormSubmit} />}
      open={isDialogOpen}
      onDialogClose={handleDialogClose}
      onButtonPrimaryClick={handleDialogClose}
      trigger={trigger}
    />
  );
};
`;

interface TestFormInputs {
  nama: string;
}

const TestForm = WithRichForm<TestFormInputs>(({ onSubmit }) => {
  const validationSchema = yup.object().shape({
    name: yup.string().required(),
  });
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<TestFormInputs>({
    resolver: yupResolver(validationSchema),
  });
  const onFormSubmit: SubmitHandler<TestFormInputs> = (formData) => {
    onSubmit(formData);
  };

  return (
    <form onSubmit={handleSubmit(onFormSubmit)} noValidate>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField margin="none" fieldKey="name" control={control} errors={errors} autoFocus required fullWidth label="User name" />
        </Grid>
      </Grid>
    </form>
  );
});

const Template: Story<DialogFormProps> = (args, cx) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleDialogOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };
  const handleFormSubmit = (formData: TestFormInputs) => {
    alert(`formData: ${JSON.stringify(formData)}`);
    handleDialogClose();
  };
  const trigger = (
    <UIButton color="secondary" variant="contained" onClick={handleDialogOpen}>
      Add user
    </UIButton>
  );

  cx.parameters.docs.page = () => <Docs onLoad={handleDialogClose} code={code} />;

  return (
    <DialogFormComponent
      {...args}
      form={<TestForm onSubmit={handleFormSubmit} />}
      open={isDialogOpen}
      onDialogClose={handleDialogClose}
      onButtonPrimaryClick={handleDialogClose}
      trigger={trigger}
    />
  );
};

export const DialogForm = Template.bind({});
DialogForm.args = {
  title: 'Add User',
  buttonSubmitText: 'Submit',
};
