import { useTranslation } from 'react-i18next';
import { UIDialog, UIDialogProps } from '@ui';
import { ReactElement, useRef } from 'react';

export interface DialogFormProps extends UIDialogProps {
  onSubmit?: () => void;
  buttonSubmitText?: string;
  form: ReactElement;
  isDefaultSubmit?: boolean;
  isLoading?: boolean;
}

export const DialogForm = ({ form, buttonSubmitText, onSubmit, isDefaultSubmit = true, isLoading = false, ...other }: DialogFormProps) => {
  const { t } = useTranslation();
  const dialogEl = useRef<HTMLDivElement>(null);
  const handleSubmitClick = () => {
    if (isDefaultSubmit) {
      const formElement = dialogEl.current?.querySelector('form');
      formElement?.dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }));
    }
    onSubmit && onSubmit();
  };

  return (
    <UIDialog
      ref={dialogEl}
      buttonCancelText={t('Cancel')}
      buttonCloseLabel={t('close')}
      buttonPrimaryProps={{ ...other.buttonPrimaryProps, loading: isLoading }}
      buttonPrimaryText={buttonSubmitText || t('Submit')}
      dialogContentProps={{
        dividers: true,
        ...other.dialogContentProps,
      }}
      onButtonPrimaryClick={handleSubmitClick}
      {...other}>
      {form}
    </UIDialog>
  );
};

export default DialogForm;
