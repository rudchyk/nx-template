import { render } from '@testing-library/react';
import DialogForm from './DialogForm';

describe('DialogForm', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <DialogForm
        title="Add User"
        form={<form />}
        open={false}
        onDialogClose={() => console.log('PrimaryClick')}
        onButtonPrimaryClick={() => console.log('PrimaryClick')}
        trigger={<button>Trigger</button>}
      />
    );

    expect(baseElement).toBeTruthy();
  });
});
