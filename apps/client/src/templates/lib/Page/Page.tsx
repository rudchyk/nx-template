import { ReactNode } from 'react';
import { UIPageTitle, UIFlexBox } from '@ui';
import { Paper } from '@mui/material';
import { Link as RouteLink, useLocation } from 'react-router-dom';

export interface PageProps {
  title: string | ReactNode;
  children: ReactNode;
  panel?: ReactNode;
  titleActions?: ReactNode;
  titleJustify?: string;
  alignItems?: string;
  flexDirection?: string;
}

export const Page = ({ title, children, panel, titleActions, alignItems = 'center', flexDirection = 'row', titleJustify = 'space-between' }: PageProps) => {
  return (
    <div>
      <UIPageTitle sx={{ overflow: 'visible', alignItems, flexDirection, justifyContent: titleActions ? titleJustify : null }}>
        {title}
        {titleActions}
      </UIPageTitle>
      <Paper
        sx={{
          p: 2,
          display: 'flex',
          flexDirection: 'column',
        }}>
        {panel && <UIFlexBox>{panel}</UIFlexBox>}
        {children}
      </Paper>
    </div>
  );
};

export default Page;
