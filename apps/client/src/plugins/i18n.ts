import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { defaultLanguage, LanguagesKeysEnum } from '@constants';
import translations from '@translations';
import { isDev } from '@utils';

export const resources = {
  en: {
    translation: translations.en,
  },
  ua: {
    translation: translations.ua,
  },
};

const getLangValue = () => {
  const langValues: string[] = Object.values(LanguagesKeysEnum);

  if (langValues.includes(navigator.language)) {
    return navigator.language;
  }

  return defaultLanguage;
};

i18n.use(initReactI18next).init({
  lng: getLangValue(),
  fallbackLng: defaultLanguage,
  debug: isDev(),
  interpolation: {
    escapeValue: false,
  },
  resources,
});

export default i18n;
