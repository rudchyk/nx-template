import axios from 'axios';
import store from '@client/store';
import { logout } from '@client/services';
import { setActiveError } from '@client/reducers';

axios.interceptors.request.use(
  (request) => {
    const {
      auth: { token },
    } = store.getState();

    if (token && request?.headers && !request?.headers?.authorization) {
      request.headers.authorization = token;
    }

    return request;
  },
  (error) => {
    // console.log('axios.interceptors.request Error:', error);
    return Promise.reject(error);
  }
);

axios.interceptors.response.use(
  (response) => response,
  async (error) => {
    try {
      const { response, name, message } = error;
      const { status } = response;

      // console.log('axios.interceptors.response Error:', error);

      if (status === 401) {
        store.dispatch(
          setActiveError({
            name,
            message,
            status,
          })
        );
        logout();
      }

      return Promise.reject(error);
    } catch (error) {
      // console.log('axios.interceptors.response Error Error:', error);
      return Promise.reject(error);
    }
  }
);
