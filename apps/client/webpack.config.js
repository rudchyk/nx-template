const getConfig = require('@nrwl/react/plugins/webpack');
const addSassResources = require('../../tools/utils/addSassResources');

module.exports = (config, context) => {
  config = getConfig(config);

  addSassResources(config.module.rules);

  return config;
};
