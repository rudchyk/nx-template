export enum UsersMessageTypesEnum {
  ADD = 'add',
  DELETE = 'delete',
  FETCH = 'fetch',
}
