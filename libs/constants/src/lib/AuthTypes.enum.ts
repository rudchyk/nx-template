export enum AuthTypesEnum {
  REGISTRATION = 'registration',
  LOGIN = 'login',
}
