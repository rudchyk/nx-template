export enum StoredCookiesEnum {
  THEME_MODE = 'themeMode',
  LANGUAGE = 'language',
  TOKEN = 'token',
  COOKIES = 'cookies',
}
