export const palette = {
  neutral: {
    main: '#64748B',
    contrastText: '#fff',
  },
};
