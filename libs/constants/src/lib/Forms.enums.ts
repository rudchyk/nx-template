export enum AddUserFormInputsEnum {
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  EMAIL = 'email',
  PASSWORD = 'password',
  REPEATED_PASSWORD = 'repeatedPassword',
  ROLE = 'role',
}

export enum ChangePasswordFormInputsEnum {
  OPD_PASSWORD = 'oldPassword',
  NEW_PASSWORD = 'password',
  REPEATED_NEW_PASSWORD = 'repeatedNewPassword',
}

export enum LogInFormInputsEnum {
  EMAIL = 'email',
  PASSWORD = 'password',
  REMEMBER = 'remember',
}

export enum ResetPasswordFormInputsEnum {
  EMAIL = 'email',
  PASSWORD = 'password',
  REPEATED_PASSWORD = 'repeatedPassword',
}

export enum SignUpFormInputsEnum {
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  EMAIL = 'email',
  PASSWORD = 'password',
  REPEATED_PASSWORD = 'repeatedPassword',
  REMEMBER = 'remember',
  MARKETING_PROMOTIONS = 'marketingPromotions',
}

export enum UpdateProfileFormInputsEnum {
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  PHONE = 'phone',
  DOB = 'dob',
}

export enum VerifyEmailFormInputsEnum {
  EMAIL = 'email',
}
