export enum APIRoutesEnum {
  API = '/api',
  AUTH = '/api/auth',
  LOGIN = '/api/auth/login',
  REGISTER = '/api/auth/register',
  RESET_PASSWORD = '/api/auth/resetPassword',
  FORGOT_PASSWORD = '/api/auth/forgotPassword',
  USERS = '/api/users',
  USER = '/api/users/:id',
  USER_CHANGE_PASSWORD = '/api/users/:id/changePassword',
  CONFIG = '/api/config/:userId',
}

export enum ConfigPathParamsEnum {
  userId = 'userId',
}

export enum UserPathParamsEnum {
  id = 'id',
}
