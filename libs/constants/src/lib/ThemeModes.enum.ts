export enum ThemeModesEnum {
  DARK = 'dark',
  LIGHT = 'light',
}

export const defaultThemeMode = ThemeModesEnum.LIGHT;
