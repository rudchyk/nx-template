import { FeaturesEnum } from '@constants';

export interface AppRouteOptions {
  feature?: FeaturesEnum;
}
