export enum ClientRoutesEnum {
  HOME = '/',
  POSTS = '/posts',
  SIGNIN = '/signIn',
  COOKIES = '/info/cookies',
  PROTECTED_PAGE = '/protectedPage',
  PLAYGROUND = '/playground',
  PROFILE = '/profile',
  SETTINGS = '/settings',
  USERS = '/admin/users',
}

export enum ClientRouteProtectionTypesEnum {
  FEATURE = 'feature',
}
