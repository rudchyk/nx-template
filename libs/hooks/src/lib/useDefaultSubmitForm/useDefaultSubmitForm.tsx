import { UIButton } from '@ui';
import { cloneElement, ReactElement, useState, useEffect, useRef } from 'react';
import { unmountComponentAtNode } from 'react-dom';

export const useDefaultSubmitForm = (submitTrigger?: ReactElement) => {
  const [submitRef, setSubmitRef] = useState<HTMLElement | null>(null);
  const formElement: any = useRef(null);
  // const defaultSubmit = submitTrigger ? cloneElement(submitTrigger, { ref: setSubmitRef }) : <input type="submit" hidden ref={setSubmitRef} />;
  // const defaultSubmit = <input type="submit" hidden ref={setSubmitRef} />;
  const defaultSubmit = <input type="submit" ref={setSubmitRef} />;

  const handleDefaultSubmitClick = () => {
    if (submitRef) {
      submitRef.click();
    }
  };

  // useEffect(() => {
  //   if (submitRef) {
  //     const { parentNode } = submitRef;
  //     formElement.current = parentNode?.querySelector('form');

  //     if (formElement.current) {
  //       formElement.current.appendChild(submitRef);
  //     }
  //   }
  //   return () => {
  //     debugger;
  //     console.log('useDefaultSubmitForm');
  //   };
  // }, [submitRef]);

  return { defaultSubmit, handleDefaultSubmitClick };
};

export default useDefaultSubmitForm;
