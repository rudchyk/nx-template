import { useState } from 'react';
import { getErrorMessage } from '@client/utils';
import { Notification } from '@interfaces';

export interface UseRichFormProps {
  service: any;
}

export const useRichForm = <T, D>({ service }: UseRichFormProps) => {
  const [isLoading, setIsLoading] = useState(false);
  const [alert, setAlert] = useState<null | Notification>(null);
  const onSubmit = async <T, D>(formData: T, resolve?: (data: D) => void, reject?: (message: string) => void) => {
    setIsLoading(true);
    try {
      const { data } = await service(formData);

      if (data.message) {
        setAlert({
          severity: 'success',
          message: data.message,
        });
      }

      resolve && resolve(data);
    } catch (error) {
      const message = getErrorMessage(error);

      setAlert({
        severity: 'error',
        message: message,
      });

      reject && reject(message);
    } finally {
      setIsLoading(false);
    }
  };
  const clearAlert = () => {
    if (alert) {
      setAlert(null);
    }
  };
  return { isLoading, alert, onSubmit, clearAlert };
};

export default useRichForm;
