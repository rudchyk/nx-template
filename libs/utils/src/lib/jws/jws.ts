import { s } from '@constants';
import { KJUR } from 'jsrsasign';

export const jwsSign = async (data: any): Promise<string> => {
  const alg = 'HS256';
  const header = { alg, typ: 'JWT' };

  try {
    const signature = await KJUR.jws.JWS.sign('HS256', JSON.stringify(header), JSON.stringify(data), s.client);
    return signature;
  } catch (error) {
    const err = error as any;
    return err.message;
  }
};

export const jwsDecode = async (data: any): Promise<any> => {
  try {
    return await KJUR.jws.JWS.parse(data);
  } catch (error) {
    const err = error as any;
    return err.message;
  }
};

const jws = {
  jwsSign,
  jwsDecode,
};

export default jws;
