export interface ManualChangeEvent<T> {
  target: {
    name: string;
    value: T;
  };
}

export function getManualChangeEvent<T>(name: string, value: T): ManualChangeEvent<T> {
  return {
    target: {
      name,
      value,
    },
  };
}
