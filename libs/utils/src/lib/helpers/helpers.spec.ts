import { isDev } from './helpers';

describe('helpers', () => {
  it('isDev() should work', () => {
    const testIsDev = process.env.NODE_ENV !== 'production';

    expect(isDev()).toBe(testIsDev);
  });
});
