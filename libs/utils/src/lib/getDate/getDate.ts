import moment from 'moment';

export const getDate = (date?: Date) => {
  if (!date) {
    return '';
  }

  return moment(date).format('DD/MM/YYYY, h:mm:ss');
};

export default getDate;
