import getFirstLetter from './getFirstLetter';

describe('getFirstLetter', () => {
  it('should work', () => {
    expect(getFirstLetter()).toBe('getFirstLetter');
  });
});
