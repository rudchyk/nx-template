import { palette } from '@constants';
import { createTheme } from '@mui/material/styles';
import { ThemeProvider } from 'emotion-theming';
import { UITheme } from '../src/components';

const theme = createTheme();

export const decorators = [
  (Story) => (
    <UITheme isDarkMode={false} palette={palette}>
      <ThemeProvider theme={theme}>{Story()}</ThemeProvider>
    </UITheme>
  ),
];
