import { render } from '@testing-library/react';
import UIFooter from './UIFooter';

describe('UIFooter', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UIFooter text="text" />);

    expect(baseElement).toBeTruthy();
  });
});
