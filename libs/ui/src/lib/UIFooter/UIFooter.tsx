import Typography from '@mui/material/Typography';
import { useMemo } from 'react';

export interface UIFooterProps {
  TypographyProps?: {
    [key: string]: unknown;
  };
  text: string;
}

export const UIFooter = ({ text, TypographyProps = {} }: UIFooterProps) =>
  useMemo(() => {
    const content = `Copyright © ${text} ${new Date().getFullYear()}.`;

    return (
      <Typography component="footer" variant="body2" color="text.secondary" align="center" {...TypographyProps}>
        {content}
      </Typography>
    );
  }, [text, TypographyProps]);

export default UIFooter;
