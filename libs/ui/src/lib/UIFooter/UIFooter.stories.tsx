import { Story, Meta } from '@storybook/react';
import UIFooterComponent, { UIFooterProps } from './UIFooter';

export default {
  component: UIFooterComponent,
  title: 'Components/UI Footer',
} as Meta;

const Template: Story<UIFooterProps> = (args) => <UIFooterComponent {...args} />;

export const UIFooter = Template.bind({});
UIFooter.args = {
  text: 'text',
};
