import { AlertColor, Button, Stack } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import { ArgsTable, PRIMARY_STORY } from '@storybook/addon-docs';
import UISnackbarComponent, { UISnackbarProps } from './UISnackbar';
import { withCustomDocs } from '../../../../storiesHelpers/utils';
import UIButton from '../UIButton/UIButton';

const code = `
import { useState } from 'react';
import { UISnackbar, UIButton } from '@ui';

interface Notification {
  severity: AlertColor;
  message: string;
}

const App = () => {
  const [notification, setNotification] = useState<Notification | null>(null);
  const handleSnackbarClose = () => {
    setNotification(null);
  };
  const buttons: Notification[] = [
    {
      severity: 'error',
      message: 'This is an error message!',
    },
    {
      severity: 'warning',
      message: 'This is a warning message!',
    },
    {
      severity: 'info',
      message: 'This is an information message!',
    },
    {
      severity: 'success',
      message: 'This is a success message!',
    },
  ];

  return (
    <>
      <Stack spacing={2}>
        {buttons.map(({ severity, message }) => (
          <UIButton
            key={message}
            color={severity}
            variant="contained"
            onClick={() => setNotification({ severity, message })}>
              Open {severity} snackbar
          </UIButton>
        ))}
      </Stack>
      <UISnackbarComponent
        open={Boolean(notification)}
        key={notification?.message || ''}
        message={notification?.message}
        severity={notification?.severity}
        onClose={handleSnackbarClose}
      />
    </>
  );
};
`;

export default {
  component: UISnackbarComponent,
  title: 'Components/UI Snackbar',
  parameters: {
    actions: { disable: true },
    layout: 'centered',
    options: {
      showPanel: false,
    },
    ...withCustomDocs({ code }),
  },
} as Meta;

export interface Notification {
  severity: AlertColor;
  message: string;
}

const Template: Story<UISnackbarProps> = (args, cx) => {
  const [notification, setNotification] = useState<Notification | null>(null);
  const handleSnackbarClose = () => {
    setNotification(null);
  };
  const buttons: Notification[] = [
    {
      severity: 'error',
      message: 'This is an error message!',
    },
    {
      severity: 'warning',
      message: 'This is a warning message!',
    },
    {
      severity: 'info',
      message: 'This is an information message!',
    },
    {
      severity: 'success',
      message: 'This is a success message!',
    },
  ];

  return (
    <>
      <Stack spacing={2}>
        {buttons.map(({ severity, message }) => (
          <UIButton key={message} color={severity} variant="contained" onClick={() => setNotification({ severity, message })}>
            Open {severity} snackbar
          </UIButton>
        ))}
      </Stack>
      <UISnackbarComponent
        open={Boolean(notification)}
        key={notification?.message || ''}
        message={notification?.message}
        severity={notification?.severity}
        onClose={handleSnackbarClose}
      />
    </>
  );
};

export const UISnackbar = Template.bind({});
