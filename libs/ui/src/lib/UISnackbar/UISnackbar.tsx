import { Alert, Snackbar, SnackbarProps, AlertColor } from '@mui/material';

export interface UISnackbarProps extends SnackbarProps {
  width?: number | string;
  message?: string;
  severity?: AlertColor;
}

export const UISnackbar = ({ message, severity, autoHideDuration = 2000, width = '100%', ...other }: UISnackbarProps) => {
  return (
    <Snackbar {...other} autoHideDuration={autoHideDuration}>
      <Alert severity={severity} sx={{ width }}>
        {message}
      </Alert>
    </Snackbar>
  );
};

export default UISnackbar;
