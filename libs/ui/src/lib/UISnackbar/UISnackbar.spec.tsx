import { render } from '@testing-library/react';
import UISnackbar from './UISnackbar';

describe('UISnackbar', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UISnackbar />);

    expect(baseElement).toBeTruthy();
  });
});
