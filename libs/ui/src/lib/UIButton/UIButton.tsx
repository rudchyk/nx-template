import { LoadingButton } from '@mui/lab';
import { Button as MUIButton, ButtonProps as MUIButtonProps, IconButton } from '@mui/material';
import { forwardRef } from 'react';
import { Link as RouteLink } from 'react-router-dom';

export interface UIButtonProps extends MUIButtonProps {
  to?: string;
  icon?: boolean;
  loading?: boolean;
}

export const UIButton = forwardRef<HTMLButtonElement, UIButtonProps>(({ to, icon, loading, ...other }, ref) => {
  const props: any = { ...other };

  if (to) {
    props.component = RouteLink;
    props.to = to;
  }

  if (icon) {
    return <IconButton {...props} />;
  }

  if (loading) {
    return <LoadingButton loading={loading} {...props} />;
  }

  return <MUIButton ref={ref} {...props} />;
});

export default UIButton;
