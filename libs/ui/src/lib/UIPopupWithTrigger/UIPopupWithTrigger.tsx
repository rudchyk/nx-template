import { ReactElement, useState, cloneElement, useEffect, MouseEvent } from 'react';
import UIPopup, { UIPopupProps } from '../UIPopup/UIPopup';

export interface UIPopupWithTriggerProps extends UIPopupProps {
  trigger: ReactElement;
  awayClickHide?: boolean;
  innerTrigger?: HTMLElement | null;
  onHover?: boolean;
}

export const UIPopupWithTrigger = ({ trigger, innerTrigger, awayClickHide = false, onHover = false, ...other }: UIPopupWithTriggerProps) => {
  const [anchorElement, setAnchorElement] = useState<HTMLElement | null>(null);
  const [open, setOpen] = useState(false);
  const onElementClick = () => {
    if (!onHover) {
      setOpen(!open);
    }
    trigger.props.onClick && trigger.props.onClick();
  };
  const hidePopup = () => {
    setOpen(false);
  };
  const onPopupClose = () => {
    awayClickHide && hidePopup();
    other.onClose && other.onClose();
  };
  const additionalProps: any = {};

  if (onHover) {
    additionalProps.onMouseOver = (e: MouseEvent<HTMLElement>) => {
      !open && setOpen(true);
    };
    additionalProps.onMouseLeave = (e: MouseEvent<HTMLElement>) => {
      open && setOpen(false);
    };
  }

  useEffect(() => {
    if (innerTrigger) {
      innerTrigger.addEventListener('click', hidePopup);
    }
    return () => {
      if (innerTrigger) {
        innerTrigger.removeEventListener('click', hidePopup);
      }
    };
  }, [innerTrigger]);

  return (
    <div {...additionalProps}>
      {cloneElement(trigger, { ...trigger.props, onClick: onElementClick, ref: setAnchorElement })}
      <UIPopup {...other} onClose={onPopupClose} anchorEl={anchorElement} open={open} />
    </div>
  );
};

export default UIPopupWithTrigger;
