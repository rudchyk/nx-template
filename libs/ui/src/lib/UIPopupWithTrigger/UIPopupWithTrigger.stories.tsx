import { Story, Meta } from '@storybook/react';
import InfoIcon from '@mui/icons-material/Info';
import { IconButton, Button } from '@mui/material';
import { Title, ArgsTable, Wrapper, Subheading, PRIMARY_STORY } from '@storybook/addon-docs';
import { GlobalStyles } from '@mui/material';
import { useState } from 'react';
import UIPopupWithTriggerComponent, { UIPopupWithTriggerProps } from './UIPopupWithTrigger';
import { UICodeBlock } from '../UICodeBlock/UICodeBlock';
import { uiPopupPlacementList } from '../../../../storiesHelpers/constants';

const code = `
import { IconButton } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import { UIPopupWithTrigger } from '@ui';

const App = () => (
  <UIPopupWithTrigger
    trigger={
      <IconButton>
        <InfoIcon />
      </IconButton>
    }>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, deleniti.
  </UIPopupWithTrigger>
);
`;

const codeCloseFromInnerClick = `
import { IconButton } from '@mui/material';
import InfoIcon from '@mui/icons-material/Info';
import { UIPopupWithTrigger } from '@ui';

const App = () => {
  const [innerTriggerToHide, setInnerTriggerToHide] = useState<HTMLElement | null>(null);

  return (
    <UIPopupWithTrigger
      innerTrigger={innerTriggerToHide}
      trigger={
        <IconButton>
          <InfoIcon />
        </IconButton>
    }>
      <div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis, consequatur.</p>
        <p>
          <Button variant="contained" id="OKButton" ref={setInnerTriggerToHide}>
            OK
          </Button>
        </p>
      </div>
    </UIPopupWithTrigger>
  );
};
`;

export default {
  component: UIPopupWithTriggerComponent,
  title: 'Components/UI Popup With Trigger',
  parameters: {
    actions: { disable: true },
    layout: 'centered',
    options: { showPanel: true },
    docs: {
      page: () => (
        <Wrapper>
          <GlobalStyles styles={{ '[data-story-id="UIElementWithPopup"]': { display: 'none' } }} />
          <Title />
          <Subheading>Default</Subheading>
          <UICodeBlock code={code} />
          <Subheading>Close From Inner Click</Subheading>
          <UICodeBlock code={codeCloseFromInnerClick} />
          <ArgsTable story={PRIMARY_STORY} />
        </Wrapper>
      ),
    },
  },
  controls: { expanded: true },
  argTypes: {
    trigger: {
      control: {
        type: null,
      },
      description: `An HTML element, virtualElement, or a function that returns either. It's used to set the position of the popper. The return value will passed as the reference object of the Popper instance.`,
    },
    innerTrigger: {
      control: {
        type: null,
      },
      description: `An HTML element selector which trigger popup to hide`,
    },
    children: {
      control: {
        type: null,
      },
      description: `Popup render function or node.`,
    },
    placement: {
      type: {
        name: 'enum',
        value: uiPopupPlacementList,
      },
      defaultValue: 'bottom',
      table: {
        defaultValue: { summary: 'bottom' },
      },
      control: 'radio',
      options: uiPopupPlacementList,
      description: `Popper placement.`,
    },
    onClose: {
      description: 'On popup close callback',
    },
    arrow: {
      type: {
        name: 'boolean',
      },
      defaultValue: true,
      table: {
        defaultValue: { summary: true },
      },
      description: 'Does popup have an arrow',
    },
    awayClickHide: {
      type: {
        name: 'boolean',
      },
      defaultValue: false,
      table: {
        defaultValue: { summary: false },
      },
      description: 'Hide the popup on click away',
    },
    onHover: {
      type: {
        name: 'boolean',
      },
      defaultValue: false,
      table: {
        defaultValue: { summary: false },
      },
      description: 'Show/Hide the popup on hover',
    },
    arrowWidth: {
      defaultValue: 16,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 16 },
      },
      description: "An Arrow's width",
    },
    modifiers: {
      defaultValue: [],
      type: {
        name: 'array',
        value: {
          name: 'object',
          value: {
            name: {
              name: 'string',
            },
            enabled: {
              name: 'boolean',
            },
            options: {
              name: 'other',
              value: 'any',
            },
          },
        },
      },
      description: `Popper.js is based on a "plugin-like" architecture, most of its features are fully encapsulated "modifiers".
      A modifier is a function that is called each time Popper.js needs to compute the position of the popper. For this reason, modifiers should be very performant to avoid bottlenecks. To learn how to create a modifier, [read the modifiers documentation](https://popper.js.org/docs/v2/modifiers/).`,
    },
    fadeTimeout: {
      defaultValue: 350,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 350 },
      },
      description: `The duration for the transition, in milliseconds. You may specify a single timeout for all transitions, or individually with an object.`,
    },
    maxWidth: {
      defaultValue: 450,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 450 },
      },
      description: `The popup's max width`,
    },
    contentPadding: {
      defaultValue: 2,
      type: {
        name: 'number',
      },
      description: "The popup's content padding",
      table: {
        defaultValue: { summary: 2 },
      },
    },
    elevation: {
      defaultValue: 3,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 3 },
      },
      description: 'Shadow depth, corresponds to dp in the spec. It accepts values between 0 and 24 inclusive. [Paper API](https://mui.com/api/paper/)',
    },
  },
} as Meta;

const Template: Story<UIPopupWithTriggerProps> = (args) => <UIPopupWithTriggerComponent {...args} data-story-id="UIElementWithPopup" />;

export const Default = Template.bind({});
Default.args = {
  trigger: (
    <IconButton>
      <InfoIcon />
    </IconButton>
  ),
  children: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem, deleniti.`,
};

const TemplateCloseFromInnerClick: Story<UIPopupWithTriggerProps> = (args) => {
  const [innerTriggerToHide, setInnerTriggerToHide] = useState<HTMLElement | null>(null);

  return (
    <UIPopupWithTriggerComponent {...args} innerTrigger={innerTriggerToHide} data-story-id="UIElementWithPopup">
      <div>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis, consequatur.</p>
        <p style={{ textAlign: 'right' }}>
          <Button variant="contained" id="OKButton" ref={setInnerTriggerToHide}>
            OK
          </Button>
        </p>
      </div>
    </UIPopupWithTriggerComponent>
  );
};
export const CloseFromInnerClick = TemplateCloseFromInnerClick.bind({});
CloseFromInnerClick.args = {
  trigger: (
    <IconButton>
      <InfoIcon />
    </IconButton>
  ),
};
