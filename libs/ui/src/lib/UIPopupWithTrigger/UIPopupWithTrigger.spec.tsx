import { render } from '@testing-library/react';
import InfoIcon from '@mui/icons-material/Info';
import { IconButton } from '@mui/material';
import UIPopupWithTrigger from './UIPopupWithTrigger';

describe('UIPopupClicked', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <UIPopupWithTrigger
        trigger={
          <IconButton onClick={() => alert('info button clicked!')}>
            <InfoIcon />
          </IconButton>
        }>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, commodi?
      </UIPopupWithTrigger>
    );

    expect(baseElement).toBeTruthy();
  });
});
