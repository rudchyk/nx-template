import { render } from '@testing-library/react';
import UIPageTitle from './UIPageTitle';

describe('UIPageTitle', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UIPageTitle>Hello</UIPageTitle>);

    expect(baseElement).toBeTruthy();
  });
});
