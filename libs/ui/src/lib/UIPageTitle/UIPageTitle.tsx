import Typography from '@mui/material/Typography';

export const UIPageTitle = (props: any) => (
  <Typography {...props} component="h1" variant="h3" color="inherit" noWrap sx={{ ...props.sx, flexGrow: 1, mb: 3, display: 'flex', alignItems: 'center' }} />
);

export default UIPageTitle;
