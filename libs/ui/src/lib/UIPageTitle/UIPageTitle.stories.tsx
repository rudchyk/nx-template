import { Story, Meta } from '@storybook/react';
import UIPageTitleComponent from './UIPageTitle';

export default {
  title: 'Components/UI Page Title',
} as Meta;

const Template: Story = (args) => <UIPageTitleComponent {...args} />;

export const UIPageTitle = Template.bind({});
UIPageTitle.args = {
  children: 'Hello World!',
};
