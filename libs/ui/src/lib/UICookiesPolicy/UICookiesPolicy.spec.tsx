import { render } from '@testing-library/react';
import UICookiesPolicy from './UICookiesPolicy';

describe('CookiesPolicy', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <UICookiesPolicy onAccept={() => alert('Cookies are accepted!')} onDecline={() => alert('Cookies are declined!')}>
        Cookies Policy Content
      </UICookiesPolicy>
    );

    expect(baseElement).toBeTruthy();
  });
});
