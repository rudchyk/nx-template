import { Story, Meta } from '@storybook/react';
import UICookiesPolicyComponent, { UICookiesPolicyProps } from './UICookiesPolicy';

export default {
  title: 'Components/UI Cookies Policy',
} as Meta;

const Template: Story<UICookiesPolicyProps> = (args) => <UICookiesPolicyComponent {...args} />;

export const UICookiesPolicy = Template.bind({});
UICookiesPolicy.args = {
  title: 'Cookies Policy',
  children: 'Cookies Policy Content',
  onDecline: () => alert('Cookies are declined!'),
  onAccept: () => alert('Cookies are accepted!'),
};
