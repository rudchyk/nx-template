import { forwardRef, useState, ReactNode } from 'react';
import Button from '@mui/material/Button';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertColor, AlertProps } from '@mui/material/Alert';
import { AlertTitle } from '@mui/material';
import { UIFlexBox } from '../UIFlexBox/UIFlexBox';

const Alert = forwardRef<HTMLDivElement, AlertProps>((props, ref) => <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />);

export interface UICookiesPolicyProps {
  title?: string;
  children: ReactNode;
  severity?: AlertColor;
  onDecline: () => void;
  declineButtonText?: string;
  onAccept: () => void;
  acceptButtonText?: string;
}

export const UICookiesPolicy = ({
  title,
  children,
  severity = 'info',
  onDecline,
  onAccept,
  declineButtonText = 'Decline',
  acceptButtonText = 'Accept',
}: UICookiesPolicyProps) => {
  const [isOpen, setIsOpen] = useState(true);
  const onClose = () => setIsOpen(false);
  const onDeclineAction = () => {
    onDecline();
    onClose();
  };
  const onAcceptAction = () => {
    onAccept();
    onClose();
  };

  const actions = (
    <UIFlexBox>
      <Button onClick={onDeclineAction} disableElevation color="inherit" variant="outlined" size="small" sx={{ mr: 2 }}>
        {declineButtonText}
      </Button>
      <Button onClick={onAcceptAction} disableElevation color="inherit" variant="outlined" size="small">
        {acceptButtonText}
      </Button>
    </UIFlexBox>
  );

  return (
    <Snackbar open={isOpen}>
      <Alert severity={severity} action={actions}>
        {title && <AlertTitle>{title}</AlertTitle>}
        {children}
      </Alert>
    </Snackbar>
  );
};

export default UICookiesPolicy;
