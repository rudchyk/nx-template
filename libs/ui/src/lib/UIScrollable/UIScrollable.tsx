import classnames from 'classnames';
import ReactTimeout, { ReactTimeoutProps } from 'react-timeout';
import { UIEvent, useRef, useEffect, useState } from 'react';
import useScrollableStyles, { cssClassNames } from './useScrollableStyles';
import { Box, BoxProps } from '@mui/material';

export interface UIScrollableProps extends ReactTimeoutProps, BoxProps {
  children: React.ReactNode;
  height?: number | string;
  overlayTopHeight?: number;
  overlayBottomHeight?: number;
}

export const UIScrollable: any = ReactTimeout(
  ({ setTimeout, children, height = 400, className, sx = {}, overlayTopHeight = 112, overlayBottomHeight = 112, ...other }: UIScrollableProps) => {
    useScrollableStyles();
    const [isHideBottom, setIsHideBottom] = useState(true);
    const [isHideTop, setIsHideTop] = useState(true);
    const scrollElement = useRef<HTMLDivElement>(null);
    const childrenElement = useRef<HTMLDivElement>(null);
    const limit = 5;

    const onScroll = ({ currentTarget: { scrollTop, children, clientHeight } }: UIEvent<HTMLDivElement>) => {
      setTimeout &&
        setTimeout(() => {
          if (scrollTop < limit) {
            setIsHideBottom(false);
            setIsHideTop(true);
          } else if (childrenElement.current && scrollTop > childrenElement.current.clientHeight - clientHeight - limit) {
            setIsHideBottom(true);
            setIsHideTop(false);
          } else {
            setIsHideBottom(false);
            setIsHideTop(false);
          }
        }, 100);
    };

    useEffect(() => {
      if (
        childrenElement.current?.clientHeight &&
        scrollElement.current?.clientHeight &&
        childrenElement.current?.clientHeight > scrollElement.current?.clientHeight
      ) {
        setIsHideBottom(false);
      }
    }, []);

    return (
      <Box {...other} sx={{ ...sx, overflow: 'hidden', position: 'relative', height }}>
        <div
          onScroll={onScroll}
          ref={scrollElement}
          className={classnames(cssClassNames.body, {
            [cssClassNames.bodyHideTop]: isHideTop,
            [cssClassNames.bodyHideBottom]: isHideBottom,
          })}>
          <div ref={childrenElement}>{children}</div>
          <Box
            className={classnames(cssClassNames.overlay, cssClassNames.overlayTop)}
            sx={{
              top: 0,
              height: overlayTopHeight,
              background: (theme) =>
                theme.palette.mode === 'dark' ? 'linear-gradient(#142033,rgba(255,255,255,0))' : 'linear-gradient(#fff, rgba(255, 255, 255, 0))',
            }}
          />
          <Box
            className={classnames(cssClassNames.overlay, cssClassNames.overlayBottom)}
            sx={{
              bottom: 0,
              height: overlayBottomHeight,
              background: (theme) =>
                theme.palette.mode === 'dark' ? 'linear-gradient(rgba(255,255,255,0),#142033)' : 'linear-gradient(rgba(255, 255, 255, 0), #fff)',
            }}
          />
        </div>
      </Box>
    );
  }
);

export default UIScrollable;
