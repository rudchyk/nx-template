import { Story, Meta } from '@storybook/react';
import UIScrollableComponent, { UIScrollableProps } from './UIScrollable';

export default {
  component: UIScrollableComponent,
  title: 'Components/UI Scrollable',
  parameters: {
    docs: {
      source: {
        code: `
          <Scrollable height={400}>Lorem ipsum dolor sit amet consectetur adipisicing elit.</Scrollable>
        `,
      },
    },
  },
  argTypes: {
    height: {
      type: { name: 'number' },
      defaultValue: 400,
      description: 'the height of the scrollable component',
    },
    overlayTopHeight: {
      type: { name: 'number' },
      defaultValue: 112,
    },
    overlayBottomHeight: {
      type: { name: 'number' },
      defaultValue: 112,
    },
  },
} as Meta;

const Template: Story<UIScrollableProps> = (args) => <UIScrollableComponent {...args} />;

export const UIScrollable = Template.bind({});
UIScrollable.args = {
  children:
    'Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe consequatur eos deleniti eum rem dolore! Doloremque accusantium accusamus enim voluptatum ducimus adipisci nisi itaque nam eaque quisquam quo numquam sint sunt, nemo, iure corporis animi labore vel eum incidunt asperiores pariatur ut blanditiis! Vel sed mollitia dolorem ex. Reprehenderit ratione culpa nemo dolorem, est distinctio adipisci inventore nam quisquam, voluptatem molestiae tempore ex asperiores praesentium maiores odio numquam eum placeat harum assumenda quo! Aliquid unde, recusandae eos aliquam, et corrupti odio cumque corporis autem minima vitae cum tempora culpa soluta molestiae, eligendi laudantium quas nisi architecto ullam voluptates placeat. Recusandae id obcaecati modi dolor quas soluta culpa consequatur deserunt! Quod quibusdam harum recusandae, excepturi qui quam quo praesentium, magni doloribus ex ad sequi. Labore corrupti ipsum odio natus voluptate beatae, dicta modi, optio ducimus iste, delectus possimus id quos sequi. Quas quasi hic perferendis, reprehenderit harum quis doloremque eos quaerat voluptatum dolores at officia impedit ratione minima consequuntur saepe necessitatibus blanditiis vero nobis itaque! Debitis eos eaque fugiat tempore ut officia amet soluta ex quia voluptate? Error consequuntur, vel dolores sed voluptate similique enim velit. Officia dolorem nisi illo dolores natus consequuntur nobis eum provident libero, numquam rerum beatae perspiciatis illum officiis nostrum porro facere asperiores incidunt vel repudiandae veniam quasi eaque laudantium? Veritatis ducimus, iure porro eligendi qui officia quae corrupti quasi nobis, labore repudiandae voluptatibus nulla voluptate vero? Beatae, totam rem porro exercitationem repudiandae ipsa. Provident ratione corrupti quia exercitationem vero molestias dolore labore maxime voluptatibus totam deleniti sequi similique et molestiae aspernatur amet, ab incidunt accusantium recusandae voluptate animi suscipit. Saepe nisi facilis culpa, sequi totam, amet reiciendis quo quia velit impedit dolor dolores accusamus? Aperiam perspiciatis quasi adipisci reprehenderit libero facere beatae eveniet voluptates quae est soluta vero, delectus placeat ab cumque ipsum enim omnis illum impedit eos. Et quas unde dolorum eius ipsa repellat, possimus exercitationem veniam officiis accusantium sit adipisci sint alias architecto sapiente quasi, voluptates sunt excepturi laudantium impedit in voluptatem facere quis dolor. Delectus doloremque laboriosam voluptatibus excepturi sequi, qui alias dignissimos consequuntur beatae exercitationem earum harum sint nisi voluptates ea, id at adipisci fuga voluptas? Cumque delectus voluptas, perspiciatis consequuntur at excepturi quo tenetur modi sit a, inventore, molestiae sapiente mollitia culpa? Optio rerum quidem itaque dolorum fugiat incidunt inventore. Minima totam cumque quasi laudantium! Aperiam dignissimos delectus voluptas iste odio? Mollitia, quae libero tempora autem labore quisquam cupiditate porro, eius error quidem nesciunt asperiores quod, quibusdam eveniet voluptas deleniti! Optio, sint? Voluptatum at minima error voluptatem laborum. Explicabo voluptates mollitia sed, impedit necessitatibus laboriosam earum illum deserunt dolorum corporis quos veniam omnis enim aperiam corrupti neque dolorem. Ipsum quo provident tempore neque tempora, at vero maxime voluptatibus quasi, similique amet odio perspiciatis. Nemo earum tenetur nam ipsa voluptates aspernatur corporis illo deserunt. Dolorum officiis maiores ducimus laudantium, delectus, voluptatum placeat aperiam laborum culpa, doloribus asperiores dolorem incidunt! Et, sit ducimus veniam eos perferendis repudiandae, aperiam quidem dolorem mollitia qui dolore quam, ipsam officiis. Ad odit optio dolor magni sapiente? Numquam.',
};
