import { Theme } from '@mui/material';
import { makeStyles, createStyles } from '@mui/styles';

const rootCssClassName = 'scrollable';
const bodyCssClassName = `${rootCssClassName}__body`;
const overlayCssClass = `${rootCssClassName}__overlay`;

export const cssClassNames = {
  root: rootCssClassName,
  body: bodyCssClassName,
  bodyHideTop: `${bodyCssClassName}-hide-top`,
  bodyHideBottom: `${bodyCssClassName}-hide-bottom`,
  overlay: overlayCssClass,
  overlayTop: `${overlayCssClass}-top`,
  overlayBottom: `${overlayCssClass}-bottom`,
};

const useScrollableStyles = makeStyles((theme: Theme) =>
  createStyles({
    '@global': {
      [`.${cssClassNames.body}`]: {
        overflow: 'hidden scroll',
        height: 'inherit',
        maxHeight: 'inherit',
        width: 'calc(100% + 20px)',
      },
      [`.${cssClassNames.bodyHideTop} .${cssClassNames.overlayTop}`]: {
        opacity: 0,
        height: 0,
      },
      [`.${cssClassNames.bodyHideBottom} .${cssClassNames.overlayBottom}`]: {
        opacity: 1,
        height: 0,
      },
      [`.${cssClassNames.overlay}`]: {
        position: 'absolute',
        left: '0',
        width: 'inherit',
        zIndex: 9,
        opacity: 1,
        transition: 'all 0.3s ease-in',
        pointerEvents: 'none',
      },
    },
  })
);

export default useScrollableStyles;
