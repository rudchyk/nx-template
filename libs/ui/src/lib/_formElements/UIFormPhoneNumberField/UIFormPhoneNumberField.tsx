import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UIPhoneNumberField, UIPhoneNumberFieldProps } from './components/UIPhoneNumberField';

export interface PhoneNumberCountry {
  countryCode: string;
  dialCode: string;
  name: string;
}

export interface UIFormPhoneNumberFieldChangeEvent {
  target: {
    value: string;
    name: string;
    country: {
      countryCode: string;
      dialCode: string;
      name: string;
    };
  };
}

export type UIFormPhoneNumberFieldProps = UIPhoneNumberFieldProps & {
  control?: Control<any, any>;
  errors?: FieldErrors<any>;
};

export const UIFormPhoneNumberField = ({ fieldKey, errors = {}, control, ...other }: UIFormPhoneNumberFieldProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UIPhoneNumberField {...props} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={other.value || other.defaultValue || ''}
      render={({ field: { onChange, value } }) => <UIPhoneNumberField {...props} controllerOnChange={onChange} value={value} />}
    />
  );
};

export default UIFormPhoneNumberField;
