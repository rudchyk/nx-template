import MuiPhoneNumber, { MuiPhoneNumberProps } from 'material-ui-phone-number';
import { ChangeEvent, ChangeEventHandler, forwardRef } from 'react';
import { getManualChangeEvent, ManualChangeEvent } from '@utils';

export interface PhoneNumberCountry {
  countryCode: string;
  dialCode: string;
  name: string;
}

export type UIPhoneNumberFieldProps = Omit<MuiPhoneNumberProps, 'variant' | 'onChange'> & {
  fieldKey: string;
  error?: string;
  onPhoneNumberChange?: (e: ManualChangeEvent<string>) => void;
  onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement> | string) => void;
  controllerOnChange?: (...event: any[]) => void;
};

export const UIPhoneNumberField = forwardRef(
  ({ fieldKey, error, label = fieldKey, onChange, onPhoneNumberChange, controllerOnChange, margin = 'normal', ...other }: UIPhoneNumberFieldProps, ref) => {
    return (
      <MuiPhoneNumber
        ref={ref as any}
        variant="outlined"
        margin={margin}
        id={fieldKey}
        label={label}
        onChange={(...args) => {
          onChange && onChange(...args);
          let val = '';
          if (typeof args[0] === 'string') {
            val = args[0];
          } else {
            val = args[0]?.target?.value || '';
          }
          onPhoneNumberChange && onPhoneNumberChange(getManualChangeEvent(fieldKey, val));
          controllerOnChange && controllerOnChange(...args);
        }}
        error={Boolean(error)}
        helperText={error}
        {...other}
      />
    );
  }
);

export default UIPhoneNumberField;
