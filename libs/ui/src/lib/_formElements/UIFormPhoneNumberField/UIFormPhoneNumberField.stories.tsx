import { Story, Meta } from '@storybook/react';
import UIFormPhoneNumberField, { UIFormPhoneNumberFieldProps } from './UIFormPhoneNumberField';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';

export default {
  component: UIFormPhoneNumberField,
  title: 'Form Elements/UI Form Phone Number Field',
} as Meta;

const Template: Story<UIFormPhoneNumberFieldProps> = (args) => {
  const [value, setValue] = useState<string>((args.value as string) || '');
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormPhoneNumberField {...args} value={value} onPhoneNumberChange={({ target: { value } }) => setValue(value)} />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'phone';
const defaultProps = {
  fieldKey,
  defaultCountry: 'ua',
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};

export const Filled = Template.bind({});
Filled.args = {
  ...defaultProps,
  value: '+380975528901',
};

export const Errors = Template.bind({});
Errors.args = {
  ...defaultProps,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
