import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UIUrlField, UIUrlFieldProps } from './components/UIUrlField';

export type UIFormUrlFieldProps = UIUrlFieldProps & {
  control?: Control<any, any>;
  errors?: FieldErrors<any>;
};

export const UIFormUrlField = ({ fieldKey, errors = {}, control, ...other }: UIFormUrlFieldProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };
  if (!control) {
    return <UIUrlField {...props} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={other.value || other.defaultValue || ''}
      render={({ field: { onChange, value } }) => <UIUrlField {...props} controllerOnChange={onChange} value={value} />}
    />
  );
};

export default UIFormUrlField;
