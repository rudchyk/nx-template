import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import { UIFormUrlField, UIFormUrlFieldProps } from './UIFormUrlField';

export default {
  component: UIFormUrlField,
  title: 'Form Elements/UI Form Url Field',
} as Meta;

const Template: Story<UIFormUrlFieldProps> = (args) => {
  const [value, setValue] = useState<string | ''>((args.value as string) || '');
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormUrlField {...args} onChange={({ target: { value } }) => setValue(value)} />
    </StoryFormElementPreview>
  );
};
const fieldKey = 'url';

export const Default = Template.bind({});
Default.args = {
  fieldKey,
};

export const Filled = Template.bind({});
Filled.args = {
  fieldKey,
  value: 'https://www.google.com/',
};

export const Errors = Template.bind({});
Errors.args = {
  fieldKey,
  value: 'google',
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid url`,
    },
  },
};
