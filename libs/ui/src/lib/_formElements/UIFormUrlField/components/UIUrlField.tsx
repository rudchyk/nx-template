import { IconButton } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';
import LinkIcon from '@mui/icons-material/Link';
import PublicIcon from '@mui/icons-material/Public';
import { ChangeEvent, useEffect, useState, useRef } from 'react';
import * as yup from 'yup';
import { TextField, TextFieldProps } from '@mui/material';

export type UIUrlFieldProps = TextFieldProps & {
  fieldKey: string;
  error?: string;
  onChange?: (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void;
  controllerOnChange?: (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void;
};

export const UIUrlField = ({
  fieldKey,
  onChange,
  controllerOnChange,
  error,
  label = fieldKey,
  margin = 'normal',
  inputProps,
  value,
  ...other
}: UIUrlFieldProps) => {
  const [url, setUrl] = useState('');
  const inputRef = useRef<HTMLInputElement>(null);
  const getUrl = async (value?: string) => {
    if (!value) {
      return '';
    }

    const urlSchema = yup.string().url();

    try {
      if (await urlSchema.validate(value)) {
        return value;
      }
    } catch (error) {
      return '';
    }

    return '';
  };
  const setValue = async (value: string | undefined) => {
    if (!value) {
      return;
    }

    const url = await getUrl(value);

    setUrl(url);
  };
  const onUrlChange = ({ target: { value } }: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setValue(value);
  const handleOnChange = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    onChange && onChange(event);
    controllerOnChange && controllerOnChange(event);
    onUrlChange(event);
  };

  useEffect(() => {
    setValue(inputRef.current?.value);
  }, []);

  return (
    <TextField
      {...other}
      value={value}
      margin={margin}
      inputRef={inputRef}
      id={fieldKey}
      label={label}
      error={Boolean(error)}
      name={fieldKey}
      onChange={handleOnChange}
      helperText={error}
      type="url"
      InputProps={{
        ...((inputProps as any) || {}),
        startAdornment: (
          <InputAdornment position="start">
            <LinkIcon />
          </InputAdornment>
        ),
        endAdornment: url && (
          <InputAdornment position="end">
            <IconButton target="_blank" href={url}>
              <PublicIcon />
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  );
};

export default UIUrlField;
