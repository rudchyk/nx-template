import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import { UIFormPasswordField, UIFormPasswordFieldProps } from './UIFormPasswordField';

export default {
  component: UIFormPasswordField,
  title: 'Form Elements/UI Form Password Field',
} as Meta;

const Template: Story<UIFormPasswordFieldProps> = (args) => {
  const [value, setValue] = useState<string>((args.value as string) || '');
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormPasswordField {...args} value={value} onChange={({ target: { value } }) => setValue(value)} />
    </StoryFormElementPreview>
  );
};
const fieldKey = 'password';

export const Default = Template.bind({});
Default.args = {
  fieldKey,
};

export const Filled = Template.bind({});
Filled.args = {
  fieldKey,
  value: '2222',
};

export const Errors = Template.bind({});
Errors.args = {
  fieldKey,
  value: '111',
  errors: {
    password: {
      message: 'password must be at least 4 characters',
    },
  },
};
