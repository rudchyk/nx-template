import { Story, Meta } from '@storybook/react';
import { UIFormNumberField, UIFormNumberFieldProps } from './UIFormNumberField';
import { withDescriptionLink } from '@storybook/utils';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import { NumberFormatValues } from 'react-number-format';

const returnedValueTypeList = ['floatValue', 'formattedValue', 'value'];

export default {
  component: UIFormNumberField,
  title: 'Form Elements/UI Form Number Field',
  ...withDescriptionLink({ text: 'react-number-format', url: 'https://github.com/s-yadav/react-number-format#readme' }),
  argTypes: {
    returnedValueType: {
      type: {
        name: 'enum',
        value: returnedValueTypeList,
      },
      defaultValue: 'floatValue',
      table: {
        defaultValue: { summary: 'floatValue' },
      },
      control: 'radio',
      options: returnedValueTypeList,
      description: `Returned Value Type`,
    },
  },
} as Meta;

const Template: Story<UIFormNumberFieldProps> = (args) => {
  const [value, setValue] = useState<string | number | undefined>(args.value || undefined);
  const [values, setValues] = useState<NumberFormatValues | undefined>();
  const getView = () => (
    <>
      <p>{String(value)}</p>
      <p>{values && 'NumberFormatValues:'}</p>
      <p>{values && JSON.stringify(values, null, 2)}</p>
    </>
  );
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={getView()}>
      <UIFormNumberField
        {...args}
        onNumberChange={({ target: { value } }) => {
          setValue(value);
        }}
        onValueChange={(values) => {
          setValues(values);
        }}
        value={value}
      />
    </StoryFormElementPreview>
  );
};
const fieldKey = 'number';

export const Default = Template.bind({});
Default.args = {
  fieldKey,
  returnedValueType: 'floatValue',
};

export const Filled = Template.bind({});
Filled.args = {
  fieldKey,
  value: 1111111,
};

export const Formatted = Template.bind({});
Formatted.args = {
  fieldKey,
  value: 1111111111111111,
  format: '#### #### #### ####',
};

export const WithMask = Template.bind({});
WithMask.args = {
  fieldKey,
  format: '+1 (###) ###-####',
  value: 12333,
  mask: '_',
};

export const WithSeparator = Template.bind({});
WithSeparator.args = {
  fieldKey,
  thousandSeparator: true,
  value: 12333,
  prefix: '$',
};

export const Errors = Template.bind({});
Errors.parameters = {
  actions: { disable: true },
};
Errors.args = {
  fieldKey,
  value: 113,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid number`,
    },
  },
};
