import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UINumberField, UINumberFieldProps } from './components/UINumberField';

export interface UIFormNumberFieldProps extends UINumberFieldProps {
  control?: Control<any, any>;
  errors?: FieldErrors<any>;
}

export const UIFormNumberField = ({ fieldKey, errors = {}, control, ...other }: UIFormNumberFieldProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UINumberField {...props} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={other.value || other.defaultValue || ''}
      render={({ field: { value, onChange } }) => <UINumberField {...props} value={value} controllerOnChange={onChange} />}
    />
  );
};

export default UIFormNumberField;
