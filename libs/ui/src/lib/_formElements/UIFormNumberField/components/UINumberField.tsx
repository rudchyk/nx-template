import { TextField, TextFieldProps } from '@mui/material';
import NumberFormat, { NumberFormatProps, NumberFormatValues, SourceInfo } from 'react-number-format';
import { getManualChangeEvent, ManualChangeEvent } from '@utils';

export enum ReturnedValueTypesEnum {
  formattedValue = 'formattedValue',
  value = 'value',
  floatValue = 'floatValue',
}

export type UINumberFieldProps = NumberFormatProps<TextFieldProps> & {
  fieldKey: string;
  error?: string;
  returnedValueType?: keyof NumberFormatValues;
  controllerOnChange?: (...event: any[]) => void;
  onNumberChange?: (e: ManualChangeEvent<number | string | undefined>) => void;
};

export const UINumberField = ({
  fieldKey,
  error,
  label = fieldKey,
  margin = 'normal',
  value,
  returnedValueType = 'floatValue',
  onValueChange,
  onNumberChange,
  controllerOnChange,
  ...other
}: UINumberFieldProps) => {
  return (
    <NumberFormat
      {...other}
      value={value}
      onValueChange={(...args) => {
        onValueChange && onValueChange(...args);
        onNumberChange && onNumberChange(getManualChangeEvent(fieldKey, args[0][returnedValueType]));
        controllerOnChange && controllerOnChange(...args);
      }}
      id={fieldKey}
      margin={margin}
      label={label}
      error={Boolean(error)}
      helperText={error}
      customInput={TextField}
    />
  );
};

export default UINumberField;
