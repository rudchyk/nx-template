import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import { UIFormTextField, UIFormTextFieldProps } from './UIFormTextField';

export default {
  component: UIFormTextField,
  title: 'Form Elements/UI Form Text Field',
} as Meta;

const Template: Story<UIFormTextFieldProps> = (args) => {
  const [value, setValue] = useState<string>((args.value as string) || '');

  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormTextField {...args} value={value} onChange={({ target: { value } }) => setValue(value)} />
    </StoryFormElementPreview>
  );
};
const fieldKey = 'email';
const defaultProps = {
  fieldKey,
  type: 'email',
};

export const Default = Template.bind({});
Default.args = defaultProps;

export const Filled = Template.bind({});
Filled.args = {
  ...defaultProps,
  value: 'test@gmail.com',
};

export const Errors = Template.bind({});
Errors.args = {
  ...defaultProps,
  value: 'dsdfs',
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid email`,
    },
  },
};
