import { FormHelperText, FormControl, FormLabel, RadioGroup, Radio } from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import { ChangeEvent } from 'react';

type LabelPlacement = 'end' | 'start' | 'top' | 'bottom';

export interface UILabeledRadioOption {
  label: string;
  value: string;
  isDisabled?: boolean;
  color?: 'error' | 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'default';
  labelPlacement?: LabelPlacement;
}

export interface UILabeledRadioProps {
  fieldKey: string;
  error?: string;
  label?: string;
  value?: any;
  ariaLabel?: string;
  options: UILabeledRadioOption[];
  isRow?: boolean;
  controllerOnChange?: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
  onChange?: (event: ChangeEvent<HTMLInputElement>, value: string) => void;
}

export const UILabeledRadio = ({
  fieldKey,
  ariaLabel,
  error,
  label = fieldKey,
  options,
  value,
  isRow = false,
  onChange,
  controllerOnChange,
}: UILabeledRadioProps) => {
  return (
    <FormControl component="fieldset">
      {label && <FormLabel component="legend">{label}</FormLabel>}
      <RadioGroup
        name={fieldKey}
        id={fieldKey}
        row={isRow}
        aria-label={ariaLabel}
        value={value}
        onChange={(...args) => {
          onChange && onChange(...args);
          controllerOnChange && controllerOnChange(...args);
        }}>
        {options.map(({ label, value, color = 'primary', labelPlacement = 'end', isDisabled = false }: UILabeledRadioOption) => (
          <FormControlLabel
            disabled={isDisabled}
            key={label}
            value={value}
            label={label}
            labelPlacement={labelPlacement}
            control={<Radio color={error ? 'error' : color} />}
          />
        ))}
      </RadioGroup>
      {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
    </FormControl>
  );
};

export default UILabeledRadio;
