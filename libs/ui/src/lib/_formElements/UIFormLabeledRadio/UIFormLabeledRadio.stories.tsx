import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import UIFormLabeledRadio, { UIFormLabeledRadioProps, UIFormLabeledRadioOption } from './UIFormLabeledRadio';

export default {
  component: UIFormLabeledRadio,
  title: 'Form Elements/UI Form Labeled Radio',
} as Meta;

const Template: Story<UIFormLabeledRadioProps> = (args) => {
  const [value, setValue] = useState<string>(args.value || args.options[0].value);
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormLabeledRadio
        {...args}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
        value={value}
      />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'gender';
const defaultProps = {
  fieldKey,
  label: 'Gender',
};
const defaultOptions: UIFormLabeledRadioOption[] = [
  {
    label: 'Female',
    value: 'female',
  },
  {
    label: 'Male',
    value: 'male',
  },
];
const otherOption = {
  label: 'Other',
  value: 'other',
};
const options: UIFormLabeledRadioOption[] = [...defaultOptions, otherOption];

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
  options,
};

export const Selected = Template.bind({});
Selected.args = {
  ...defaultProps,
  options,
  value: 'male',
};

export const Row = Template.bind({});
Row.args = {
  ...defaultProps,
  options,
  isRow: true,
};

export const DifferentPlacement = Template.bind({});
DifferentPlacement.args = {
  ...defaultProps,
  options: [...defaultOptions, { ...otherOption, labelPlacement: 'start' }],
};

export const DisabledItem = Template.bind({});
DisabledItem.args = {
  ...defaultProps,
  options: [...defaultOptions, { ...otherOption, isDisabled: true }],
};

export const Errors = Template.bind({});
Errors.args = {
  ...defaultProps,
  options,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
