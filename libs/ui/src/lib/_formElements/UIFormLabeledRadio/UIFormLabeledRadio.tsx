import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UILabeledRadio, UILabeledRadioProps } from './components/UILabeledRadio';

type LabelPlacement = 'end' | 'start' | 'top' | 'bottom';

export interface UIFormLabeledRadioOption {
  label: string;
  value: string;
  isDisabled?: boolean;
  color?: 'error' | 'primary' | 'secondary' | 'info' | 'success' | 'warning' | 'default';
  labelPlacement?: LabelPlacement;
}

export interface UIFormLabeledRadioProps extends UILabeledRadioProps {
  errors?: FieldErrors<any>;
  control: Control<any, any>;
}

export const UIFormLabeledRadio = ({
  fieldKey,
  errors = {},
  control,
  options,
  value = options.length ? options[0].value : '',
  ...other
}: UIFormLabeledRadioProps) => {
  const props = {
    fieldKey,
    options,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UILabeledRadio {...props} value={value} />;
  }
  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={value}
      render={({ field: { onChange, value } }) => <UILabeledRadio {...props} controllerOnChange={onChange} value={value}></UILabeledRadio>}
    />
  );
};

export default UIFormLabeledRadio;
