import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import { StoryFormElementPreview } from '@storybook/components';
import { FilmOption, top100Films } from '@ui/constants';
import { withDescriptionLink } from '@storybook/utils';
import { UIFormAutocomplete, UIFormAutocompleteProps } from './UIFormAutocomplete';

export default {
  component: UIFormAutocomplete,
  title: 'Form Elements/UI Autocomplete',
  ...withDescriptionLink({ text: 'Autocomplete API', url: 'https://mui.com/material-ui/api/autocomplete/' }),
} as Meta;

const Template: Story<UIFormAutocompleteProps<FilmOption>> = (args) => {
  const [value, setValue] = useState<FilmOption | null>(args.defaultValue || args.value || null);

  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={JSON.stringify(value, null, 2)}>
      <UIFormAutocomplete<FilmOption>
        {...args}
        onChange={(event, newValue) => {
          setValue(newValue);
        }}
      />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'movie';
const defaultProps = {
  fieldKey,
  disableCloseOnSelect: true,
  clearOnEscape: true,
  options: top100Films,
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};

export const Filled = Template.bind({});
Filled.args = {
  ...defaultProps,
  defaultValue: top100Films[5],
};

export const Errors = Template.bind({});
Errors.args = {
  ...defaultProps,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
