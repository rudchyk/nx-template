import { ChipTypeMap, TextField } from '@mui/material';
import Autocomplete, { AutocompleteProps } from '@mui/material/Autocomplete';
import { getManualChangeEvent } from '@utils';
import { MUIMarginType } from '@interfaces';

export type UIAutocompleteProps<T> = Omit<AutocompleteProps<T, undefined, undefined, undefined, ChipTypeMap['defaultComponent']>, 'renderInput'> & {
  fieldKey: string;
  error?: string;
  label?: string;
  margin?: MUIMarginType;
  controllerOnChange?: (...event: any[]) => void;
  onAutocompleteChange?: (...event: any[]) => void;
};

export const UIAutocomplete = <T,>({
  fieldKey,
  fullWidth,
  error,
  label = fieldKey,
  onChange,
  controllerOnChange,
  onAutocompleteChange,
  margin = 'normal',
  sx,
  options,
  ...other
}: UIAutocompleteProps<T>) => {
  let styles = {};

  if (!fullWidth) {
    styles = {
      ...sx,
      width: 210,
    };
  }

  return (
    <Autocomplete<T>
      disablePortal
      {...other}
      id={fieldKey}
      options={options}
      sx={styles}
      onChange={(...args) => {
        const changeData = getManualChangeEvent<T>(fieldKey, args[1] as T);
        onChange && onChange(...args);
        onAutocompleteChange && onAutocompleteChange(changeData);
        controllerOnChange && controllerOnChange(changeData);
      }}
      renderInput={(params) => <TextField {...params} margin={margin} label={label} name={fieldKey} error={Boolean(error)} helperText={error} />}
    />
  );
};

export default UIAutocomplete;
