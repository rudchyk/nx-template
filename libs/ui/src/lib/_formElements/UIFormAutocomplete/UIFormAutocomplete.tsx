import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UIAutocomplete, UIAutocompleteProps } from './components/UIAutocomplete';

export type UIFormAutocompleteProps<T> = UIAutocompleteProps<T> & {
  control?: Control<any, any>;
  errors?: FieldErrors<any>;
};

export const UIFormAutocomplete = <T,>({ fieldKey, errors = {}, control, ...other }: UIFormAutocompleteProps<T>) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UIAutocomplete {...props} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={other.defaultValue || other.value || ''}
      render={({ field: { onChange, value } }) => <UIAutocomplete {...props} controllerOnChange={onChange} value={value} />}
    />
  );
};

export default UIFormAutocomplete;
