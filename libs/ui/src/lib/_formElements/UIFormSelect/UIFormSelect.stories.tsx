import { Story, Meta } from '@storybook/react';
import { getDisabledControls } from '@storybook/utils';
import { StoryFormElementPreview } from '@storybook/components';
import { UIFormSelect, UIFormSelectProps } from './UIFormSelect';
import { useState } from 'react';

export default {
  component: UIFormSelect,
  title: 'Form Elements/UI Form Select',
  argTypes: {
    ...getDisabledControls(['value', 'fieldKey', 'ref', 'focused', 'onSelectChange', 'control', 'hiddenLabel']),
    margin: {
      table: {
        defaultValue: { summary: 'normal' },
      },
    },
  },
} as Meta;

const Template: Story<UIFormSelectProps> = (args) => {
  const [value, setValue] = useState<string>((args.value as string) || '');

  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormSelect {...args} value={value} onChange={({ target: { value } }) => setValue(value)} />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'select';
const list = [
  { value: 'sergii', label: 'Sergii' },
  { value: 'alex', label: 'Alex' },
  { value: 'tom', label: 'Tom' },
];
const defaultProps = {
  list,
  fieldKey,
};

export const Default = Template.bind({});
Default.args = {
  ...defaultProps,
};

export const FilledDefault = Template.bind({});
FilledDefault.args = {
  ...defaultProps,
  value: list[0].value,
};

export const Filled = Template.bind({});
Filled.args = {
  ...defaultProps,
  value: list[1].value,
};

export const WithErrors = Template.bind({});
WithErrors.args = {
  ...defaultProps,
  value: '',
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
