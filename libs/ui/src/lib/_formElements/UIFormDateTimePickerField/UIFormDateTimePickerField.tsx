import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UIDateTimePickerField, UIDateTimePickerFieldProps } from './components/UIDateTimePickerField';

export type UIFormDateTimePickerFieldProps = UIDateTimePickerFieldProps & {
  errors?: FieldErrors<any>;
  control?: Control<any, any>;
};

export const UIFormDateTimePickerField = ({ fieldKey, errors = {}, control, value = new Date(), ...other }: UIFormDateTimePickerFieldProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UIDateTimePickerField {...props} value={value} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={value}
      render={({ field: { onChange, value } }) => <UIDateTimePickerField {...props} controllerOnChange={onChange} value={value} />}
    />
  );
};

export default UIFormDateTimePickerField;
