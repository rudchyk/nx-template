import { Story, Meta } from '@storybook/react';
import { withDescriptionLink } from '@storybook/utils';
import { StoryFormElementPreview } from '@storybook/components';
import UIFormDateTimePickerField, { UIFormDateTimePickerFieldProps } from './UIFormDateTimePickerField';
import { useState } from 'react';

export default {
  component: UIFormDateTimePickerField,
  title: 'Form Elements/UI Form DateTimePicker Field',
  ...withDescriptionLink({ text: 'DesktopDatePicker API', url: 'https://mui.com/x/api/date-pickers/desktop-date-picker/' }),
} as Meta;

const Template: Story<UIFormDateTimePickerFieldProps> = (args) => {
  const defaultValue = args.value === null ? args.value : (args.value as Date) || new Date();
  const [value, setValue] = useState<Date | null>(defaultValue);

  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={String(value ? new Date(value) : value)}>
      <UIFormDateTimePickerField {...args} onChange={(date) => setValue(date as Date | null)} value={value} />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'dateOfBirth';

export const Default = Template.bind({});
Default.args = {
  fieldKey,
};

export const Empty = Template.bind({});
Empty.args = {
  fieldKey,
  value: null,
};

export const Filled = Template.bind({});
Filled.args = {
  fieldKey,
  value: new Date(544700332000),
};

export const Errors = Template.bind({});
Errors.args = {
  fieldKey,
  value: null,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
