import DesktopDatePicker, { DesktopDatePickerProps } from '@mui/lab/DesktopDatePicker';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DateAdapter from '@mui/lab/AdapterMoment';
import classnames from 'classnames';
import { TextField, TextFieldProps } from '@mui/material';
import { MUIMarginType } from '@interfaces';
import { getManualChangeEvent, ManualChangeEvent } from '@utils';
import styles from './UIDateTimePickerField.module.sass';

export type UIDateTimePickerFieldProps = Omit<DesktopDatePickerProps, 'renderInput' | 'value' | 'onChange'> & {
  fieldKey: string;
  error?: string;
  label?: string;
  fullWidth?: boolean;
  inputFormat?: string;
  margin?: MUIMarginType;
  value?: Date | null;
  className?: string;
  onChange?: (date: unknown, keyboardInputValue?: string | undefined) => void;
  onDateChange?: (e: ManualChangeEvent<Date | null>) => void;
  controllerOnChange?: (...event: any[]) => void;
};

export const UIDateTimePickerField = ({
  fieldKey,
  error,
  label = fieldKey,
  margin = 'normal',
  fullWidth,
  inputFormat = 'DD/MM/yyyy',
  onChange,
  controllerOnChange,
  onDateChange,
  value,
  className,
  ...other
}: UIDateTimePickerFieldProps) => {
  return (
    <LocalizationProvider dateAdapter={DateAdapter}>
      <DesktopDatePicker
        {...other}
        inputFormat={inputFormat}
        value={value}
        onChange={(...args) => {
          onChange && onChange(...args);
          controllerOnChange && controllerOnChange(...args);
          onDateChange && onDateChange(getManualChangeEvent(fieldKey, args[0] as Date | null));
        }}
        renderInput={(params: TextFieldProps) => {
          return (
            <TextField
              {...params}
              fullWidth={fullWidth}
              className={classnames(className, styles.field)}
              margin={margin}
              id={fieldKey}
              label={label}
              error={Boolean(error)}
              helperText={error}
            />
          );
        }}
      />
    </LocalizationProvider>
  );
};

export default UIDateTimePickerField;
