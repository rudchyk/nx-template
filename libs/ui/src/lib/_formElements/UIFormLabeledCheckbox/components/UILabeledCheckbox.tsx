import { FormHelperText } from '@mui/material';
import FormControlLabel, { FormControlLabelProps } from '@mui/material/FormControlLabel';
import Checkbox, { CheckboxProps } from '@mui/material/Checkbox';
import { ChangeEvent, ForwardedRef, forwardRef } from 'react';
import { getManualChangeEvent, ManualChangeEvent } from '@utils';

export interface UILabeledCheckboxProps {
  fieldKey: string;
  error?: string;
  label?: string;
  disabled?: boolean;
  CheckboxProps?: CheckboxProps;
  FormControlLabelProps?: FormControlLabelProps;
  value?: boolean;
  controllerOnChange?: (event: ChangeEvent<HTMLInputElement>, checked: boolean) => void;
  onChange?: (event: ChangeEvent<HTMLInputElement>, checked: boolean) => void;
}

export const UILabeledCheckbox = forwardRef(
  (
    {
      fieldKey,
      error,
      label = fieldKey,
      disabled = false,
      CheckboxProps = {},
      value,
      onChange,
      controllerOnChange,
      FormControlLabelProps,
    }: UILabeledCheckboxProps,
    ref: ForwardedRef<any>
  ) => {
    return (
      <>
        <FormControlLabel
          {...(FormControlLabelProps || {})}
          disabled={disabled}
          control={
            <Checkbox
              {...CheckboxProps}
              id={fieldKey}
              name={fieldKey}
              value={value}
              checked={!!value}
              ref={ref}
              onChange={(...args) => {
                onChange && onChange(...args);
                // onChange && onChange(getChangeEvent(fieldKey, checked));
                controllerOnChange && controllerOnChange(...args);
              }}
            />
          }
          label={label}
        />
        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
      </>
    );
  }
);

export default UILabeledCheckbox;
