import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import UIFormLabeledCheckbox, { UIFormLabeledCheckboxProps } from './UIFormLabeledCheckbox';

export default {
  component: UIFormLabeledCheckbox,
  title: 'Form Elements/UI Form Labeled Checkbox',
} as Meta;

const Template: Story<UIFormLabeledCheckboxProps> = (args) => {
  const [value, setValue] = useState<boolean>(args.value || false);

  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={String(value)}>
      <UIFormLabeledCheckbox
        {...args}
        onChange={(event, checked) => {
          setValue(checked);
        }}
        value={value}
      />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'remember';

export const DefaultChecked = Template.bind({});
DefaultChecked.args = {
  fieldKey,
  value: true,
};

export const DefaultUnchecked = Template.bind({});
DefaultUnchecked.args = {
  fieldKey,
};

export const Errors = Template.bind({});
Errors.args = {
  fieldKey,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid value`,
    },
  },
};
