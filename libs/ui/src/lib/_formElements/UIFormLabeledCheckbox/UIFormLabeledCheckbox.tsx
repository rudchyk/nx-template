import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UILabeledCheckbox, UILabeledCheckboxProps } from './components/UILabeledCheckbox';

export interface UIFormLabeledCheckboxProps extends UILabeledCheckboxProps {
  errors?: FieldErrors<any>;
  control: Control<any, any>;
}

export const UIFormLabeledCheckbox = ({ fieldKey, errors = {}, value = false, control, ...other }: UIFormLabeledCheckboxProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UILabeledCheckbox {...props} value={value} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={value}
      render={({ field: { onChange, value, ref } }) => <UILabeledCheckbox {...props} controllerOnChange={onChange} ref={ref} value={value} />}
    />
  );
};

export default UIFormLabeledCheckbox;
