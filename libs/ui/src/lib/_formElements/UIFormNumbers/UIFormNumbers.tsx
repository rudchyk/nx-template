import { Control, Controller, FieldErrors } from 'react-hook-form';
import { UINumbers, UINumbersProps } from './components/UINumbers';
export interface UIFormNumbersProps extends UINumbersProps {
  errors?: FieldErrors<any>;
  control?: Control<any, any>;
  value?: number;
}

export const UIFormNumbers = ({ fieldKey, errors = {}, value = 0, control, ...other }: UIFormNumbersProps) => {
  const props = {
    fieldKey,
    error: errors[fieldKey]?.message,
    ...other,
  };

  if (!control) {
    return <UINumbers {...props} value={value} />;
  }

  return (
    <Controller
      name={fieldKey}
      control={control}
      defaultValue={value}
      render={({ field: { value, onChange, ref } }) => <UINumbers {...props} ref={ref} value={value} controllerOnChange={onChange} />}
    />
  );
};

export default UIFormNumbers;
