import { Story, Meta } from '@storybook/react';
import { StoryFormElementPreview } from '@storybook/components';
import { useState } from 'react';
import UIFormNumbers, { UIFormNumbersProps } from './UIFormNumbers';

export default {
  component: UIFormNumbers,
  title: 'Form Elements/UI Form Numbers',
} as Meta;

const Template: Story<UIFormNumbersProps> = (args) => {
  const [value, setValue] = useState<number>(args.value || 0);
  return (
    <StoryFormElementPreview fieldKey={args.fieldKey} view={value}>
      <UIFormNumbers {...args} onChange={({ target: { value } }) => setValue(value)} value={value} />
    </StoryFormElementPreview>
  );
};

const fieldKey = 'numberValue';

export const Default = Template.bind({});
Default.args = {
  fieldKey,
};

export const Filled = Template.bind({});
Filled.args = {
  fieldKey,
  value: 1000,
};

export const Errors = Template.bind({});
Errors.args = {
  fieldKey,
  value: -113,
  errors: {
    [fieldKey]: {
      message: `${fieldKey} must be a valid number`,
    },
  },
};
