import { FormHelperText, FormControl, FormLabel, Box } from '@mui/material';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import InputBase from '@mui/material/InputBase';
import NumberFormat, { NumberFormatValues } from 'react-number-format';
import { forwardRef, useState, useEffect } from 'react';
import { getManualChangeEvent, ManualChangeEvent } from '@utils';
import { MUIButtonsColors } from '@interfaces';
export interface UINumbersProps {
  fieldKey: string;
  label?: string;
  step?: number;
  increaseButtonColor?: MUIButtonsColors;
  increaseButtonAriaLabel?: string;
  decreaseButtonColor?: MUIButtonsColors;
  decreaseButtonAriaLabel?: string;
  direction?: 'left' | 'right';
  controllerOnChange?: (val: number) => void;
  onChange?: (e: ManualChangeEvent<number>) => void;
  error?: string;
  value?: number;
}

export const UINumbers = forwardRef(
  (
    {
      onChange,
      fieldKey,
      label = fieldKey,
      step = 1,
      increaseButtonColor = 'primary',
      increaseButtonAriaLabel = 'increase button',
      decreaseButtonColor = 'primary',
      decreaseButtonAriaLabel = 'decrease button',
      direction = 'left',
      error,
      value: defaultValue,
      controllerOnChange,
    }: UINumbersProps,
    ref
  ) => {
    const [value, setValue] = useState<number>(defaultValue || 0);
    const fireOnChange = (newValue: number) => {
      setValue(newValue);
      onChange && onChange(getManualChangeEvent(fieldKey, newValue));
      controllerOnChange && controllerOnChange(newValue);
    };
    const onIncrease = () => fireOnChange(value + step);
    const onDecrease = () => fireOnChange(value - step);
    const onValueChange = (values: NumberFormatValues) => fireOnChange(values.floatValue || 0);

    useEffect(() => {
      setValue(defaultValue || 0);
    }, [defaultValue]);

    return (
      <FormControl>
        {label && <FormLabel component="legend">{label}</FormLabel>}
        <Box sx={{ display: 'flex', alignItems: 'center', flexDirection: direction === 'left' ? 'row' : 'row-reverse', my: 1 }}>
          <Fab size="small" color={increaseButtonColor} aria-label={increaseButtonAriaLabel} onClick={onIncrease}>
            <AddIcon />
          </Fab>
          <NumberFormat
            sx={{ width: 50, mx: 1 }}
            name={fieldKey}
            inputRef={ref}
            inputProps={{
              style: {
                textAlign: 'center',
                color: error ? 'red' : undefined,
              },
            }}
            allowNegative
            value={value}
            onValueChange={onValueChange}
            customInput={InputBase}
          />
          <Fab size="small" color={decreaseButtonColor} aria-label={decreaseButtonAriaLabel} onClick={onDecrease}>
            <RemoveIcon />
          </Fab>
        </Box>
        {error && <FormHelperText error={!!error}>{error}</FormHelperText>}
      </FormControl>
    );
  }
);

export default UINumbers;
