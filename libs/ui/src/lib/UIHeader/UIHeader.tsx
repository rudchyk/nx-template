import Toolbar, { ToolbarProps } from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import { ReactElement, ReactNode } from 'react';

interface UIHeaderProps extends ToolbarProps {
  title: string;
  isBarOpened?: boolean;
  onToggleDrawer?: () => void;
  children?: ReactNode;
  leftBar?: ReactElement | null;
}

export const UIHeader = ({ title, leftBar, children, ...other }: UIHeaderProps) => (
  <Toolbar {...other}>
    {leftBar}
    <Typography component="div" variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
      {title}
    </Typography>
    {children}
  </Toolbar>
);

export default UIHeader;
