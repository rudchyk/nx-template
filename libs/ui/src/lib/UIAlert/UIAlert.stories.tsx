import { AlertColor, Button, Stack } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import { ArgsTable, PRIMARY_STORY } from '@storybook/addon-docs';
import UIAlertComponent, { UIAlertProps } from './UIAlert';
import { withCustomDocs } from '../../../../storiesHelpers/utils';
import UIButton from '../UIButton/UIButton';

const severityList = ['success', 'info', 'warning', 'error'];

export default {
  component: UIAlertComponent,
  title: 'Components/UI Alert',
  argTypes: {
    ref: {
      table: {
        disable: true,
      },
    },
    severity: {
      type: {
        name: 'enum',
        value: severityList,
      },
      defaultValue: 'error',
      table: {
        defaultValue: { summary: 'error' },
      },
      control: 'radio',
      options: severityList,
      description: `The severity of the alert. This defines the color and icon used.`,
    },
  },
} as Meta;

const Template: Story<UIAlertProps> = (args) => <UIAlertComponent {...args} />;

export const UIAlert = Template.bind({});
UIAlert.args = {
  children: `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Possimus, fugiat!`,
  elevation: 1,
  severity: 'error',
  square: false,
  mb: 3,
};
