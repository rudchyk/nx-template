import { Alert, AlertProps } from '@mui/material';

export interface UIAlertProps extends AlertProps {
  mb?: number;
}

export const UIAlert = ({ mb = 3, sx = {}, ...other }: UIAlertProps) => (other.children ? <Alert {...other} sx={{ mb, ...sx }} /> : null);

export default UIAlert;
