import { Story, Meta } from '@storybook/react';
import UILocalizationSwitcher, { UILocalizationSwitcherProps } from './UILocalizationSwitcher';

export default {
  title: 'Components/UI Localization Switcher',
} as Meta;

const Template: Story<UILocalizationSwitcherProps> = (args) => <UILocalizationSwitcher {...args} />;

export const WithOneLanguage = Template.bind({});
WithOneLanguage.args = {
  language: 'en',
};

export const WithLanguages = Template.bind({});
WithLanguages.args = {
  language: 'en',
  onChangeLanguage: (language) => alert(`Changed Language ${language}`),
  languagesList: ['en', 'ua', 'de'],
};
