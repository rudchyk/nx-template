import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import { useState } from 'react';

export interface UILocalizationSwitcherProps {
  language: string;
  languagesList?: string[];
  onChangeLanguage?: (language: string) => void;
  className?: string;
}

export const UILocalizationSwitcher = ({ onChangeLanguage, language, className, languagesList = [] }: UILocalizationSwitcherProps) => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const onMenuItemClick = (languageItem: string) => {
    onChangeLanguage && onChangeLanguage(languageItem);
    handleClose();
  };
  const menuId = 'localizationMenu';
  const btnId = 'localizationButton';

  return (
    <div className={className}>
      <Button
        variant="outlined"
        color="inherit"
        id={btnId}
        disabled={!languagesList.length || languagesList.length === 1}
        aria-controls={open ? menuId : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}>
        {language}
      </Button>
      {languagesList.length > 1 && (
        <Menu
          id={menuId}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': btnId,
          }}>
          {languagesList.map(
            (languageItem) =>
              languageItem !== language && (
                <MenuItem key={languageItem} onClick={() => onMenuItemClick(languageItem)}>
                  {languageItem.toUpperCase()}
                </MenuItem>
              )
          )}
        </Menu>
      )}
    </div>
  );
};

export default UILocalizationSwitcher;
