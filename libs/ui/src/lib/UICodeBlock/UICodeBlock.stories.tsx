import { Story, Meta } from '@storybook/react';
import UICodeBlockComponent, { UICodeBlockProps } from './UICodeBlock';

const languages = ['javascript', 'html', 'markup', 'ts', 'sass', 'tsx', 'css'];

export default {
  title: 'Components/UI Code Block',
  argTypes: {
    lines: {
      type: {
        name: 'boolean',
      },
      defaultValue: true,
      description: 'If `true`, lines will be shown',
    },
    copyText: {
      type: {
        name: 'string',
      },
      defaultValue: 'Copy to clipboard!',
      description: `Copy Button Text`,
    },
    language: {
      type: {
        name: 'enum',
        value: languages,
      },
      defaultValue: 'tsx',
      table: {
        defaultValue: { summary: 'tsx' },
      },
      control: 'radio',
      options: languages,
      description: `language`,
    },
  },
} as Meta;

const Template: Story<UICodeBlockProps> = (args) => <UICodeBlockComponent {...args} />;

export const UICodeBlock = Template.bind({});
UICodeBlock.args = {
  code: `import Typography from '@mui/material/Typography';

export const PageTitle = (props: any) => (
  <Typography {...props} component="h1" variant="h3" color="inherit" noWrap sx={{ flexGrow: 1, mb: 3, display: 'flex', alignItems: 'center' }} />
);

export default PageTitle;`,
};
