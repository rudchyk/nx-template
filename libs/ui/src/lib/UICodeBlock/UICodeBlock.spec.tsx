import { render } from '@testing-library/react';
import UICodeBlock from './UICodeBlock';

describe('Code', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UICodeBlock />);

    expect(baseElement).toBeTruthy();
  });
});
