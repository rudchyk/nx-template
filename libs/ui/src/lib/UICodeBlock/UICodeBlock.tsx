import { useEffect } from 'react';
import Prism from 'prismjs';
import classnames from 'classnames';
import './Code.sass';

export interface UICodeBlockProps {
  code: string;
  language?: 'javascript' | 'html' | 'markup' | 'ts' | 'sass' | 'tsx' | 'css';
  lines?: boolean;
  copyText?: string;
}

export const UICodeBlock = ({ code, language = 'tsx', lines = true, copyText = 'Copy to clipboard!' }: UICodeBlockProps) => {
  useEffect(() => {
    Prism.highlightAll();
  }, []);

  return (
    <div className={classnames({ 'line-numbers': lines })} data-prismjs-copy-timeout="500">
      <pre>
        <code className={`language-${language}`} data-prismjs-copy={copyText}>
          {code.trim()}
        </code>
      </pre>
    </div>
  );
};

export default UICodeBlock;
