import { Button } from '@mui/material';
import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import { ArgsTable, PRIMARY_STORY } from '@storybook/addon-docs';
import UIDialogComponent, { UIDialogProps } from './UIDialog';
import { Docs } from '../../../../storiesHelpers/components';

export default {
  component: UIDialogComponent,
  title: 'Components/UI Dialog',
  parameters: {
    actions: { disable: true },
    layout: 'centered',
  },
  argTypes: {
    open: {
      control: {
        type: null,
      },
      defaultValue: false,
      description: 'If `true`, the component is shown.',
    },
    trigger: {
      control: {
        type: null,
      },
      description: 'The trigger element',
    },
    onDialogClose: {
      control: {
        type: null,
      },
      description: 'Callback fired when the component requests to be closed.',
    },
  },
} as Meta;

const code = `
import { useState } from 'react';
import { UIDialog } from '@ui';

const App = () => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleClickOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };

  return (
    <UIDialog
      trigger={
        <Button variant="outlined" onClick={handleClickOpen}>
          Open UI dialog
        </Button>
      }
      title="Hello world!!!"
      open={false}
      onDialogClose={handleDialogClose}
      onButtonPrimaryClick={handleDialogClose}
    >
      Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, odit!
    </UIDialog>
  );
};
`;

const Template: Story<UIDialogProps> = (args, cx) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const handleClickOpen = () => {
    setIsDialogOpen(true);
  };
  const handleDialogClose = () => {
    setIsDialogOpen(false);
  };
  const trigger = (
    <Button variant="outlined" onClick={handleClickOpen}>
      Open UI dialog
    </Button>
  );

  cx.parameters.docs.page = () => (
    <Docs onLoad={handleDialogClose} code={code}>
      <ArgsTable story={PRIMARY_STORY} />
    </Docs>
  );

  return <UIDialogComponent {...args} trigger={trigger} open={isDialogOpen} onDialogClose={handleDialogClose} onButtonPrimaryClick={handleDialogClose} />;
};

export const UIDialog = Template.bind({});
UIDialog.args = {
  title: 'Hello world!!!',
  dialogContentProps: {
    dividers: true,
  },
  buttonPrimaryText: 'Ok',
  isButtonClose: true,
  buttonCloseLabel: 'close',
  buttonCancelText: 'Cancel',
  isButtonCancel: true,
  children: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. A, odit!`,
};
