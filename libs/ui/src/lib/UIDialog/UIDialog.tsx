import Dialog, { DialogProps } from '@mui/material/Dialog';
import DialogActions, { DialogActionsProps } from '@mui/material/DialogActions';
import DialogContent, { DialogContentProps } from '@mui/material/DialogContent';
import DialogTitle, { DialogTitleProps } from '@mui/material/DialogTitle';
import { IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { useEffect, ReactElement, forwardRef } from 'react';
import { AnyObj } from '@interfaces';
import { UIButton, UIButtonProps } from '../../';

export interface UIDialogProps extends DialogProps {
  title?: string;
  trigger?: ReactElement | (() => ReactElement) | null;
  onDialogClose?: (event: AnyObj, reason: 'backdropClick' | 'escapeKeyDown' | 'closeClick' | 'cancelClick') => void;
  onDialogOpen?: () => void;
  dialogActionsProps?: DialogActionsProps;
  dialogContentProps?: DialogContentProps;
  dialogTitleProps?: DialogTitleProps;
  buttonPrimaryProps?: UIButtonProps;
  buttonPrimaryText?: string;
  onButtonPrimaryClick?: () => void;
  buttonCancelProps?: UIButtonProps;
  buttonCancelText?: string;
  onButtonCancelClick?: () => void;
  isButtonCancel?: boolean;
  isButtonClose?: boolean;
  onButtonCloseClick?: () => void;
  buttonCloseLabel?: string;
}

export const UIDialog = forwardRef<HTMLDivElement, UIDialogProps>(
  (
    {
      title,
      trigger = null,
      onClose,
      onDialogClose,
      onDialogOpen,
      dialogActionsProps = {},
      dialogContentProps = {},
      dialogTitleProps = {},
      buttonPrimaryProps = {},
      buttonPrimaryText = 'Ok',
      onButtonPrimaryClick,
      isButtonClose = true,
      buttonCloseLabel = 'close',
      onButtonCloseClick,
      buttonCancelProps,
      buttonCancelText = 'Cancel',
      onButtonCancelClick,
      isButtonCancel = true,
      open = false,
      children,
      ...other
    },
    ref
  ) => {
    if (onClose) {
      console.warn('onClose will not work! Please, use "onDialogClose"');
    }
    const handleButtonCloseClick = (event: AnyObj) => {
      onButtonCloseClick && onButtonCloseClick();
      onDialogClose && onDialogClose(event, 'closeClick');
    };
    const handleButtonCancelClick = (event: AnyObj) => {
      onButtonCancelClick && onButtonCancelClick();
      onDialogClose && onDialogClose(event, 'cancelClick');
    };

    useEffect(() => {
      if (open && onDialogOpen) {
        onDialogOpen();
      }
    }, [open]);

    return (
      <>
        {trigger}
        <Dialog {...other} ref={ref} onClose={onDialogClose} open={open}>
          <DialogTitle
            {...dialogTitleProps}
            sx={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              ...(dialogTitleProps && dialogTitleProps.sx ? dialogTitleProps.sx : {}),
            }}>
            {title}
            {isButtonClose && (
              <IconButton edge="end" color="inherit" onClick={handleButtonCloseClick} aria-label={buttonCloseLabel}>
                <CloseIcon />
              </IconButton>
            )}
          </DialogTitle>
          {children && <DialogContent {...dialogContentProps}>{children}</DialogContent>}
          <DialogActions {...dialogActionsProps}>
            {dialogActionsProps.children}
            {isButtonCancel && (
              <UIButton {...buttonCancelProps} onClick={handleButtonCancelClick}>
                {buttonCancelText}
              </UIButton>
            )}
            {onButtonPrimaryClick && (
              <UIButton variant="contained" color="primary" {...buttonPrimaryProps} onClick={onButtonPrimaryClick}>
                {buttonPrimaryText}
              </UIButton>
            )}
          </DialogActions>
        </Dialog>
      </>
    );
  }
);

export default UIDialog;
