import { Button } from '@mui/material';
import { render } from '@testing-library/react';
import UIDialog from './UIDialog';

describe('Dialog', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <UIDialog
        trigger={
          <Button variant="outlined" onClick={() => alert(1)}>
            Open UI dialog
          </Button>
        }
        title="test"
        open={false}
        onDialogClose={() => alert(1)}
        onButtonPrimaryClick={() => alert(2)}
      />
    );

    expect(baseElement).toBeTruthy();
  });
});
