import { render } from '@testing-library/react';
import UITheme from './UITheme';

describe('UITheme', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <UITheme isDarkMode>
        <div>Test</div>
      </UITheme>
    );

    expect(baseElement).toBeTruthy();
  });
});
