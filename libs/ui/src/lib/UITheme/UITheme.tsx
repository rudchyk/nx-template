import { createTheme, ThemeProvider, Theme as ThemeInterface } from '@mui/material/styles';
import { ReactNode, useMemo, useEffect } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import GlobalStyles from '@mui/material/GlobalStyles';
import { PaletteMode } from '@mui/material';
import { ThemeModesEnum } from '@constants';

interface AnyObj {
  [key: string]: any;
}
interface ThemeProps {
  children: ReactNode;
  themeMode?: PaletteMode;
  palette?: AnyObj;
  components?: AnyObj;
}

export const UITheme = ({ children, themeMode = 'light', palette = {}, components = {} }: ThemeProps) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: themeMode,
          ...palette,
        },
        components: {
          ...components,
        },
      }),
    [themeMode, palette]
  );
  const createGlobalStyles = (theme: ThemeInterface) => {
    const cssVars: any = {};

    for (const key in theme.palette) {
      if (Object.prototype.hasOwnProperty.call(theme.palette, key)) {
        const element = (theme.palette as any)[key];
        if (element instanceof Object) {
          for (const elementKey in element) {
            if (Object.prototype.hasOwnProperty.call(element, elementKey)) {
              const prop = element[elementKey];
              if (typeof prop === 'string') {
                cssVars[`--${key}-${elementKey}`] = prop;
              }
            }
          }
        }
      }
    }

    return {
      ':root': cssVars,
    };
  };

  useEffect(() => {
    document.body.classList.toggle(ThemeModesEnum.DARK, themeMode === ThemeModesEnum.DARK);
  }, [themeMode]);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline enableColorScheme />
      <GlobalStyles styles={createGlobalStyles} />
      {children}
    </ThemeProvider>
  );
};

export default UITheme;
