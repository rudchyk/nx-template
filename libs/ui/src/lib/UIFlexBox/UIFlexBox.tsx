import { SxProps } from '@mui/material';
import Box, { BoxProps } from '@mui/material/Box';

enum JustifyContentValuesEnum {
  sb = 'space-between',
  c = 'center',
  s = 'flex-start',
  e = 'flex-end',
}

enum FlexDirectionValuesEnum {
  r = 'row',
  c = 'column',
}

enum AlignItemsValuesEnum {
  n = 'normal',
  c = 'center',
  s = 'flex-start',
  e = 'flex-end',
}

type JustifyContentValueKeys = keyof typeof JustifyContentValuesEnum;

type FlexDirectionValueKeys = keyof typeof FlexDirectionValuesEnum;

type AlignItemsValueKeys = keyof typeof AlignItemsValuesEnum;

export interface UIFlexBoxProps extends BoxProps {
  children?: React.ReactNode;
  jc?: JustifyContentValueKeys;
  fd?: FlexDirectionValueKeys;
  ai?: AlignItemsValueKeys;
  sx?: SxProps;
  height?: number | string;
  width?: number | string;
}

export const UIFlexBox = ({ children, jc, fd, ai, height = '100%', width, sx = {}, ...other }: UIFlexBoxProps) => (
  <Box
    {...other}
    sx={{
      ...sx,
      display: 'flex',
      justifyContent: jc ? JustifyContentValuesEnum[jc] : JustifyContentValuesEnum.c,
      flexDirection: fd ? FlexDirectionValuesEnum[fd] : null,
      alignItems: ai ? AlignItemsValuesEnum[ai] : AlignItemsValuesEnum.c,
      width,
      height,
    }}>
    {children}
  </Box>
);

export default UIFlexBox;
