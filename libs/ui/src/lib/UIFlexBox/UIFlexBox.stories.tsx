import { Story, Meta } from '@storybook/react';
import UIFlexBoxComponent, { UIFlexBoxProps } from './UIFlexBox';

export default {
  component: UIFlexBoxComponent,
  title: 'Components/UI Flex Box',
  argTypes: {
    height: {
      type: {
        name: 'string',
      },
      defaultValue: '100%',
    },
    width: {
      type: {
        name: 'string',
      },
    },
  },
} as Meta;

const Template: Story<UIFlexBoxProps> = (args) => (
  <UIFlexBoxComponent {...args}>
    <div>1</div>
    <div>2</div>
    <div>3</div>
  </UIFlexBoxComponent>
);
export const UIFlexBox = Template.bind({});
// UIFlexBox.args = {
//   text: 'text',
// };
