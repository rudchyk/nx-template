import { Story, Meta } from '@storybook/react';
import { useState } from 'react';
import InfoIcon from '@mui/icons-material/Info';
import { Box } from '@mui/material';
import { Title, Subtitle, Description, ArgsTable, Wrapper, PRIMARY_STORY } from '@storybook/addon-docs';
import { GlobalStyles } from '@mui/material';
import UIPopupComponent, { UIPopupProps } from './UIPopup';
import { UICodeBlock } from '../UICodeBlock/UICodeBlock';
import { uiPopupPlacementList } from '../../../../storiesHelpers/constants';

const code = `
import { useState } from 'react';
import InfoIcon from '@mui/icons-material/Info';
import { UIPopup } from '@ui';

const App = () => {
  const [anchorElement, setAnchorElement] = useState<HTMLElement | null>(null);

  return (
    <>
      <div ref={setAnchorElement}>
        <InfoIcon />
      </div>
      <UIPopup open={true} placement="left" anchorEl={anchorElement}>
        Lorem ipsum dolor sit amet consectetur adipisicing elit.
        Repellat, delectus aliquam totam molestiae magnam tempore recusandae,
        quisquam nobis enim incidunt ipsa dolore sequi vel blanditiis temporibus
        porro repellendus dicta sapiente?
      </UIPopup>
    </>
  );
};
`;

export default {
  title: 'Components/UI Popup',
  parameters: {
    actions: { disable: true },
    layout: 'centered',
    options: { showPanel: true },
    docs: {
      page: () => (
        <Wrapper>
          <GlobalStyles styles={{ '[data-story-id="UIPopup"]': { display: 'none' } }} />
          <Title />
          <Subtitle />
          <Description />
          <UICodeBlock code={code} />
          <ArgsTable story={PRIMARY_STORY} />
        </Wrapper>
      ),
    },
  },
  controls: { expanded: true },
  argTypes: {
    open: {
      type: {
        name: 'boolean',
      },
      description: 'If `true`, the component is shown.',
    },
    anchorEl: {
      control: {
        type: null,
      },
      description: `An HTML element, virtualElement, or a function that returns either. It's used to set the position of the popper. The return value will passed as the reference object of the Popper instance.`,
    },
    children: {
      control: {
        type: null,
      },
      description: `Popup render function or node.`,
    },
    placement: {
      type: {
        name: 'enum',
        value: uiPopupPlacementList,
      },
      defaultValue: 'bottom',
      table: {
        defaultValue: { summary: 'bottom' },
      },
      control: 'radio',
      options: uiPopupPlacementList,
      description: `Popper placement.`,
    },
    onClose: {
      description: 'On popup close callback',
    },
    arrow: {
      type: {
        name: 'boolean',
      },
      defaultValue: true,
      table: {
        defaultValue: { summary: true },
      },
      description: 'Does popup have an arrow',
    },
    arrowWidth: {
      defaultValue: 16,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 16 },
      },
      description: "An Arrow's width",
    },
    modifiers: {
      defaultValue: [],
      type: {
        name: 'array',
        value: {
          name: 'object',
          value: {
            name: {
              name: 'string',
            },
            enabled: {
              name: 'boolean',
            },
            options: {
              name: 'other',
              value: 'any',
            },
          },
        },
      },
      description: `Popper.js is based on a "plugin-like" architecture, most of its features are fully encapsulated "modifiers".
      A modifier is a function that is called each time Popper.js needs to compute the position of the popper. For this reason, modifiers should be very performant to avoid bottlenecks. To learn how to create a modifier, [read the modifiers documentation](https://popper.js.org/docs/v2/modifiers/).`,
    },
    fadeTimeout: {
      defaultValue: 350,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 350 },
      },
      description: `The duration for the transition, in milliseconds. You may specify a single timeout for all transitions, or individually with an object.`,
    },
    maxWidth: {
      defaultValue: 450,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 450 },
      },
      description: `The popup's max width`,
    },
    contentPadding: {
      defaultValue: 2,
      type: {
        name: 'number',
      },
      description: "The popup's content padding",
      table: {
        defaultValue: { summary: 2 },
      },
    },
    elevation: {
      defaultValue: 3,
      type: {
        name: 'number',
      },
      table: {
        defaultValue: { summary: 3 },
      },
      description: 'Shadow depth, corresponds to dp in the spec. It accepts values between 0 and 24 inclusive. [Paper API](https://mui.com/api/paper/)',
    },
  },
} as Meta;

const Template: Story<UIPopupProps> = (args) => {
  const [anchorElement, setAnchorElement] = useState<HTMLElement | null>(null);

  return (
    <>
      <Box sx={{ width: 24, height: 24 }} ref={setAnchorElement}>
        <InfoIcon />
      </Box>
      <UIPopupComponent {...args} anchorEl={anchorElement} data-story-id="UIPopup" />
    </>
  );
};

export const UIPopup = Template.bind({});
UIPopup.args = {
  open: true,
  children: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellat, delectus aliquam totam molestiae magnam tempore recusandae, quisquam nobis enim incidunt ipsa dolore sequi vel blanditiis temporibus porro repellendus dicta sapiente?`,
};
