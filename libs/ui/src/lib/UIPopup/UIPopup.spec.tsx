import { render } from '@testing-library/react';
import UIPopup from './UIPopup';

describe('UIPopup', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<UIPopup>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum, impedit.</UIPopup>);

    expect(baseElement).toBeTruthy();
  });
});
