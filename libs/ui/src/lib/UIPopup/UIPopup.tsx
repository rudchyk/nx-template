import { Box, ClickAwayListener, Fade, Paper, Popper, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ReactElement, useState, useRef } from 'react';
import { VirtualElement, Options, Placement } from '@popperjs/core';
import classnames from 'classnames';

export interface UIPopupProps {
  children: ReactElement | (() => ReactElement) | string;
  open?: boolean;
  onClose?: () => void;
  arrow?: boolean;
  placement?: Placement;
  anchorEl?: VirtualElement | (() => VirtualElement) | null;
  modifiers?: Options['modifiers'];
  fadeTimeout?: number;
  popupClassName?: string;
  maxWidth?: number;
  contentPadding?: number | null;
  elevation?: number;
  arrowWidth?: number;
}

export const UIPopup = ({
  placement = 'bottom',
  fadeTimeout = 350,
  modifiers = [],
  anchorEl,
  arrow = true,
  open = false,
  onClose,
  children,
  popupClassName,
  maxWidth = 450,
  contentPadding,
  elevation = 3,
  arrowWidth = 16,
  ...other
}: UIPopupProps) => {
  const useStyles = makeStyles((theme: Theme) => {
    const color = theme.palette.background.paper;
    const arrowLength = (arrowWidth * Math.sqrt(2)) / 2;
    const arrowHeight = Math.sqrt(Math.pow(arrowLength, 2) - Math.pow(arrowWidth, 2) / 4);
    const getContentPadding = () => {
      switch (contentPadding) {
        case null:
          return undefined;
        case undefined:
          return theme.spacing(2);
        default:
          return theme.spacing(contentPadding);
      }
    };
    const getPopoverRootMargin = (margin: number) => {
      return arrow ? margin : undefined;
    };

    return {
      popoverRoot: {
        position: 'relative',
        maxWidth,
      },
      content: {
        position: 'relative',
        padding: getContentPadding(),
        backgroundColor: color,
      },
      arrowWrapper: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        top: 0,
        left: 0,
      },
      arrowBack: {
        '& $arrowElement': {
          '&::before': {
            boxShadow: theme.shadows[elevation],
          },
        },
      },
      arrowFront: {
        overflow: 'hidden',
      },
      arrowElement: {
        width: arrowWidth,
        height: arrowWidth,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '&::before': {
          content: '""',
          display: 'block',
          width: arrowLength,
          height: arrowLength,
          backgroundColor: 'currentColor',
          transform: 'rotate(45deg)',
          transformOrigin: 'center',
        },
      },
      arrow: {
        width: arrowWidth,
        height: arrowHeight,
        color,
      },
      popper: {
        '&[data-popper-placement*="bottom"]': {
          '& $arrow': {
            top: -arrowHeight,
          },
          '& $popoverRoot': {
            marginTop: getPopoverRootMargin(arrowHeight),
          },
        },
        '&[data-popper-placement*="top"]': {
          '& $arrow': {
            bottom: -arrowHeight,
            '& $arrowWrapper': {
              top: -arrowHeight,
            },
          },
          '& $popoverRoot': {
            marginBottom: getPopoverRootMargin(arrowHeight),
          },
        },
        '&[data-popper-placement*="right"]': {
          '& $arrow': {
            left: 0,
            marginLeft: -arrowHeight,
            height: arrowWidth,
            width: arrowHeight,
          },
          '& $popoverRoot': {
            marginLeft: getPopoverRootMargin(arrowHeight),
          },
        },
        '&[data-popper-placement*="left"]': {
          '& $arrow': {
            right: 0,
            marginRight: -arrowHeight,
            height: arrowWidth,
            width: arrowHeight,
            '& $arrowWrapper': {
              left: -arrowHeight,
            },
          },
          '& $popoverRoot': {
            marginRight: getPopoverRootMargin(arrowHeight),
          },
        },
      },
    };
  });
  const paperEl = useRef<any>(null);
  const classes = useStyles();
  const [arrowRef, setArrowRef] = useState<HTMLElement | null>(null);
  const onPopupClose = () => {
    onClose && onClose();
  };
  const Arrow = (
    <span className={classes.arrow} ref={setArrowRef}>
      <span className={classnames(classes.arrowWrapper, classes.arrowBack)}>
        <span className={classes.arrowElement}></span>
      </span>
      <span className={classnames(classes.arrowWrapper, classes.arrowFront)}>
        <span className={classes.arrowElement}></span>
      </span>
    </span>
  );

  return (
    <Popper
      open={open}
      anchorEl={anchorEl}
      placement={placement}
      transition
      className={classnames(classes.popper, popupClassName)}
      modifiers={[
        ...modifiers,
        {
          name: 'flip',
          enabled: true,
          options: {
            altBoundary: true,
            rootBoundary: 'document',
            padding: 8,
          },
        },
        {
          name: 'preventOverflow',
          enabled: true,
          options: {
            altAxis: true,
            altBoundary: true,
            tether: true,
            rootBoundary: 'document',
            padding: 8,
          },
        },
        {
          name: 'arrow',
          enabled: true,
          options: {
            element: arrowRef,
          },
        },
      ]}
      {...other}>
      {({ TransitionProps }) => (
        <Fade {...TransitionProps} timeout={fadeTimeout}>
          <div>
            <ClickAwayListener onClickAway={onPopupClose}>
              <Paper ref={paperEl} className={classes.popoverRoot} elevation={elevation} sx={{ maxWidth }}>
                {arrow ? Arrow : null}
                <Box className={classes.content}>{children}</Box>
              </Paper>
            </ClickAwayListener>
          </div>
        </Fade>
      )}
    </Popper>
  );
};

export default UIPopup;
