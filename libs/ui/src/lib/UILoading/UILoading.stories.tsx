import { Story, Meta } from '@storybook/react';
import UILoadingComponent, { UILoadingProps } from './UILoading';

export default {
  component: UILoadingComponent,
  title: 'Components/UI Loading',
  argTypes: {
    height: {
      type: {
        name: 'number',
      },
      defaultValue: 300,
    },
    size: {
      type: {
        name: 'number',
      },
      defaultValue: 80,
    },
  },
} as Meta;

const Template: Story<UILoadingProps> = (args) => <UILoadingComponent {...args} />;

export const UILoading = Template.bind({});
UILoading.args = {};
