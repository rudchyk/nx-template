import CircularProgress from '@mui/material/CircularProgress';
import { UIFlexBox } from '../../';

export interface UILoadingProps {
  height?: number;
  size?: number;
}

export const UILoading = ({ height = 300, size = 80 }: UILoadingProps) => {
  return (
    <UIFlexBox height={height}>
      <CircularProgress size={size} />
    </UIFlexBox>
  );
};

export default UILoading;
