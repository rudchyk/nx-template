import LinearProgress from '@mui/material/LinearProgress';
export interface WithProgressProps {
  [key: string]: any;
  isLoading: boolean;
}

export const WithProgress = (WrappedComponent: any) => {
  return ({ isLoading, ...props }: WithProgressProps) => (isLoading ? <LinearProgress /> : <WrappedComponent {...props} />);
};

export default WithProgress;
