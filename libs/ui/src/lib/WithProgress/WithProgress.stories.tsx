import { Story, Meta } from '@storybook/react';
import { withCustomDocs } from 'libs/ui/storiesHelpers/utils';
import WithProgressComponent, { WithProgressProps } from './WithProgress';

const code = `
import { WithProgress } from '@ui';

const ElementWithProgress = WithProgress(() => <div>After progress</div>);

const App = () => <ElementWithProgress isLoading={true} />;
`;

export default {
  title: 'HOC/With Progress',
  parameters: {
    ...withCustomDocs({ code }),
  },
} as Meta;

const ElementWithProgress = WithProgressComponent(() => <div>After progress</div>);

const Template: Story<WithProgressProps> = (args) => <ElementWithProgress {...args} />;

export const WithProgress = Template.bind({});
WithProgress.args = {
  isLoading: true,
};
