import { ComponentType, ChangeEvent } from 'react';
import { Notification } from '@interfaces';
import { SubmitErrorHandler, SubmitHandler } from 'react-hook-form';

export interface WithRichFormProps<T> {
  onSubmit: SubmitHandler<T>;
  onChange?: (...event: any[]) => void;
  onError?: SubmitErrorHandler<T>;
  alert?: Notification | null;
  isLoading?: boolean;
  [key: string]: any;
}

export function WithRichForm<T>(Form: ComponentType<WithRichFormProps<T>>) {
  return ({ onSubmit, onChange, onError, submitTrigger, alert, isLoading, ...props }: WithRichFormProps<T>) => (
    <Form {...props} onSubmit={onSubmit} onChange={onChange} onError={onError} alert={alert} isLoading={isLoading} />
  );
}

export default WithRichForm;
