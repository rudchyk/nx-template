import { Story, Meta } from '@storybook/react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { SubmitErrorHandler, useForm } from 'react-hook-form';
import { Grid } from '@mui/material';
import { withCustomDocs } from '@storybook/utils';
import { ChangeEvent, useState } from 'react';
import { UIFormTextField, UIButton, UIAlert } from '../../';
import WithRichFormHOC, { WithRichFormProps } from './WithRichForm';

const code = `
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { useForm, SubmitErrorHandler } from 'react-hook-form';
import { WithRichForm, RichFormAlert, UIAlert } from '@ui';

enum TestFormInputsEnum {
  NAME = 'name',
  EMAIL = 'email',
}

interface TestFormInputs {
  [TestFormInputsEnum.NAME]: string;
  [TestFormInputsEnum.EMAIL]: string;
}

const TestForm = WithRichFormHOC<TestFormInputs>(({ onSubmit, onChange, onError, alert }) => {
  const validationSchema = yup.object().shape({
    [TestFormInputsEnum.NAME]: yup.string().required(),
    [TestFormInputsEnum.EMAIL]: yup.string().email().required(),
  });
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<TestFormInputs>({
    resolver: yupResolver(validationSchema),
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField margin="none" fieldKey={TestFormInputsEnum.NAME} control={control} errors={errors} required fullWidth label="User name" />
        </Grid>
        <Grid item xs={12}>
          <UIFormTextField
            margin="none"
            fieldKey={TestFormInputsEnum.EMAIL}
            type="email"
            control={control}
            errors={errors}
            required
            fullWidth
            label="User email"
          />
        </Grid>
      </Grid>
      <UIButton type="submit">Submit</UIButton>
    </form>
  );
});

const App = () => {
  const [alert, setAlert] = useState<any>();
  const onSubmit = (formData: TestFormInputs) => {
    setAlert({
      severity: 'success',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onError: SubmitErrorHandler<TestFormInputs> = (formData) => {
    setAlert({
      severity: 'error',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onChange = ({ target: { value, name } }: ChangeEvent<HTMLFormElement>) => {
    setAlert({
      severity: 'info',
      message: \`\${name}: \${value}\`,
    });
  };

  return <TestForm onChange={onChange} alert={alert} onSubmit={onSubmit} onError={onError} />;
};
`;

export default {
  title: 'HOC/With Rich Form',
  parameters: {
    ...withCustomDocs({ code }),
  },
} as Meta;

enum TestFormInputsEnum {
  NAME = 'name',
  EMAIL = 'email',
}

interface TestFormInputs {
  [TestFormInputsEnum.NAME]: string;
  [TestFormInputsEnum.EMAIL]: string;
}

const TestForm = WithRichFormHOC<TestFormInputs>(({ onSubmit, onChange, onError, alert }) => {
  const validationSchema = yup.object().shape({
    [TestFormInputsEnum.NAME]: yup.string().required(),
    [TestFormInputsEnum.EMAIL]: yup.string().email().required(),
  });
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<TestFormInputs>({
    resolver: yupResolver(validationSchema),
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <UIAlert severity={alert?.severity}>{alert?.message}</UIAlert>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <UIFormTextField margin="none" fieldKey={TestFormInputsEnum.NAME} control={control} errors={errors} required fullWidth label="User name" />
        </Grid>
        <Grid item xs={12}>
          <UIFormTextField
            margin="none"
            fieldKey={TestFormInputsEnum.EMAIL}
            type="email"
            control={control}
            errors={errors}
            required
            fullWidth
            label="User email"
          />
        </Grid>
      </Grid>
      <UIButton sx={{ mt: 3 }} type="submit">
        Submit
      </UIButton>
    </form>
  );
});

const Template: Story<WithRichFormProps<TestFormInputs>> = () => {
  const [alert, setAlert] = useState<any>();
  const onSubmit = (formData: TestFormInputs) => {
    setAlert({
      severity: 'success',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onError: SubmitErrorHandler<TestFormInputs> = (formData) => {
    setAlert({
      severity: 'error',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onChange = ({ target: { value, name } }: ChangeEvent<HTMLFormElement>) => {
    setAlert({
      severity: 'info',
      message: `${name}: ${value}`,
    });
  };

  return <TestForm onChange={onChange} alert={alert} onSubmit={onSubmit} onError={onError} />;
};

export const WithRichForm = Template.bind({});
