import { Grid, Paper } from '@mui/material';
import { UICodeBlock, UIFlexBox } from '@ui';
import * as svgs from '@svg';

const SvgItem = ({ data }: any) => {
  const SvgElement = (svgs as any)[data];
  const size = 150;
  const max = '100%';

  return (
    <Grid item xs={4}>
      <Paper
        sx={{
          p: 2,
        }}>
        <UIFlexBox>
          <SvgElement style={{ width: size, height: size, maxWidth: max, maxHeight: max }} />
        </UIFlexBox>
        <UICodeBlock lines={false} code={`import { ${data} } from '@svg';`} />
        <UICodeBlock lines={false} code={`<${data} />`} />
      </Paper>
    </Grid>
  );
};

const SVGPreview = () => {
  if (!svgs) {
    return null;
  }

  return (
    <Grid container spacing={2}>
      {Object.keys(svgs).map((svgItem: string) => (
        <SvgItem key={svgItem} data={svgItem} />
      ))}
    </Grid>
  );
};

export default SVGPreview;
