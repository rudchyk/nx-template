import { Story, Meta } from '@storybook/react';
import { withoutDocsOptions } from '../../storiesHelpers/constants';
import SVGPreviewComponent from './SVGPreview';

export default {
  title: 'SVG Preview',
  parameters: {
    ...withoutDocsOptions,
  },
} as Meta;

const SVGPreviewTemplate: Story = () => <SVGPreviewComponent />;

export const SVGPreview = SVGPreviewTemplate.bind({});
