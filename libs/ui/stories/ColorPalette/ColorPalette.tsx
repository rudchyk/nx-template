import { Box, Grid, Typography, useTheme, Paper } from '@mui/material';
import { UIInputCopyeditor, UIFormTextField } from '../../src/components';
import styles from './ColorPalette.module.sass';

interface ColorItem {
  name: string;
  cssVar: string;
  hex: string;
}

const ColorPalette = () => {
  const theme = useTheme();

  const getColorsList = () => {
    const colors: ColorItem[] = [];

    for (const key in theme.palette) {
      if (Object.prototype.hasOwnProperty.call(theme.palette, key)) {
        const cssVars: any = {};
        const element = (theme.palette as any)[key];

        if (element instanceof Object) {
          for (const elementKey in element) {
            if (Object.prototype.hasOwnProperty.call(element, elementKey)) {
              const prop = element[elementKey];
              if (typeof prop === 'string') {
                cssVars[`--${key}-${elementKey}`] = prop;

                const possibleNumber = Number(elementKey);
                const isNumber = Number.isInteger(possibleNumber);
                const jsProp = isNumber ? `[${elementKey}]` : `.${elementKey}`;

                colors.push({
                  name: `palette.${key}${jsProp}`,
                  cssVar: `var(--${key}-${elementKey})`,
                  hex: prop,
                });
              }
            }
          }
        }
      }
    }

    return colors;
  };

  const colors = getColorsList();

  return (
    <Grid container spacing={2}>
      {colors.map(({ name, cssVar, hex }: ColorItem) => (
        <Grid key={name} item xs={3}>
          <Paper
            elevation={2}
            sx={{
              p: 1,
              display: 'flex',
            }}>
            <div className={styles.box}>
              <Box className={styles.colorBox} sx={{ background: hex }} />
              <Typography sx={{ mt: 1 }} variant="body2" align="center">
                {hex}
              </Typography>
            </div>
            <div>
              <UIInputCopyeditor readOnly>
                <UIFormTextField size="small" label="JS Name" fieldKey={name} defaultValue={name} />
              </UIInputCopyeditor>
              <UIInputCopyeditor readOnly>
                <UIFormTextField size="small" label="CSS Variable" fieldKey={cssVar} defaultValue={cssVar} />
              </UIInputCopyeditor>
            </div>
          </Paper>
        </Grid>
      ))}
    </Grid>
  );
};

export default ColorPalette;
