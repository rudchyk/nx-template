import { Story, Meta } from '@storybook/react';
import { withoutDocsOptions } from '../../storiesHelpers/constants';
import ColorPaletteComponent from './ColorPalette';

export default {
  title: 'Color Palette',
  parameters: {
    ...withoutDocsOptions,
  },
} as Meta;

const Template: Story = () => <ColorPaletteComponent />;

export const ColorPalette = Template.bind({});
