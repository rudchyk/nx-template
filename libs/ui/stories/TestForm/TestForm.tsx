import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { Grid, InputAdornment } from '@mui/material';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { UserRolesEnum } from '@constants';
import { top100Films, FilmOption } from '@ui/constants';
import { faker } from '@faker-js/faker';
import {
  UIFormTextField,
  UIFormPasswordField,
  UIFormPhoneNumberField,
  UIFormDateTimePickerField,
  UIFormNumberField,
  UIFormSelect,
  UIFormLabeledCheckbox,
  UIFormUrlField,
  UIInputCopyeditor,
  UIFormLabeledRadio,
  UIFormLabeledRadioOption,
  UIFormNumbers,
  WithRichForm,
  UIFormAutocomplete,
} from '../../src/components';

enum TestFormInputsEnum {
  ID = 'id',
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  EMAIL = 'email',
  PASSWORD = 'password',
  REPEATED_PASSWORD = 'repeatedPassword',
  URL = 'url',
  PHONE = 'phone',
  DOB = 'dob',
  ROLE = 'role',
  BUDGET = 'budget',
  REMEMBER = 'remember',
  GENDER = 'gender',
  FAVORITE_NUMBER = 'favoriteNumber',
  FAVORITE_FILM = 'favoriteFilm',
}

export interface TestFormInputs {
  [TestFormInputsEnum.ID]: string;
  [TestFormInputsEnum.FIRST_NAME]: string;
  [TestFormInputsEnum.LAST_NAME]: string;
  [TestFormInputsEnum.EMAIL]: string;
  [TestFormInputsEnum.PASSWORD]: string;
  [TestFormInputsEnum.REPEATED_PASSWORD]: string;
  [TestFormInputsEnum.URL]: string;
  [TestFormInputsEnum.PHONE]: string;
  [TestFormInputsEnum.DOB]: string | Date | null;
  [TestFormInputsEnum.ROLE]: UserRolesEnum;
  [TestFormInputsEnum.BUDGET]: string;
  [TestFormInputsEnum.REMEMBER]: boolean;
  [TestFormInputsEnum.GENDER]: string;
  [TestFormInputsEnum.FAVORITE_NUMBER]: number;
  [TestFormInputsEnum.FAVORITE_FILM]: FilmOption | null;
}

export const TestForm = WithRichForm<TestFormInputs>(({ onSubmit, onChange, onError }) => {
  const { t } = useTranslation();
  const validationSchema = yup.object().shape({
    [TestFormInputsEnum.ID]: yup.string(),
    [TestFormInputsEnum.FIRST_NAME]: yup.string(),
    [TestFormInputsEnum.LAST_NAME]: yup.string(),
    [TestFormInputsEnum.EMAIL]: yup.string().email().required(),
    [TestFormInputsEnum.PASSWORD]: yup.string().min(4).required(),
    [TestFormInputsEnum.REPEATED_PASSWORD]: yup
      .string()
      .min(4)
      .required()
      .test(TestFormInputsEnum.REPEATED_PASSWORD, t('password must be the same'), (value, context) => value === context.parent.password),
    [TestFormInputsEnum.URL]: yup.string().url(),
    [TestFormInputsEnum.PHONE]: yup.string(),
    [TestFormInputsEnum.DOB]: yup.string().nullable(),
    [TestFormInputsEnum.ROLE]: yup.string(),
    [TestFormInputsEnum.BUDGET]: yup.string(),
    [TestFormInputsEnum.REMEMBER]: yup.boolean(),
    [TestFormInputsEnum.GENDER]: yup.string(),
    [TestFormInputsEnum.FAVORITE_NUMBER]: yup.number().test(TestFormInputsEnum.FAVORITE_NUMBER, t('Favorite Number must be > 0'), (value = 0) => value > 0),
    [TestFormInputsEnum.FAVORITE_FILM]: yup.object().nullable(),
  });
  const pass = '1111';
  const roles = Object.values(UserRolesEnum).map((item) => ({
    value: item,
    label: item.toUpperCase(),
  }));
  const genders: UIFormLabeledRadioOption[] = [
    {
      label: 'Female',
      value: 'female',
    },
    {
      label: 'Male',
      value: 'male',
    },
    {
      label: 'Other',
      value: 'other',
    },
  ];
  const defaultValues = {
    [TestFormInputsEnum.ID]: '624326981f9f1c3157a67a74',
    [TestFormInputsEnum.FIRST_NAME]: faker.name.firstName(),
    [TestFormInputsEnum.LAST_NAME]: faker.name.lastName(),
    [TestFormInputsEnum.EMAIL]: faker.internet.email(),
    [TestFormInputsEnum.PASSWORD]: pass,
    [TestFormInputsEnum.REPEATED_PASSWORD]: pass,
    [TestFormInputsEnum.URL]: 'https://www.google.com/',
    [TestFormInputsEnum.PHONE]: faker.phone.phoneNumber('+38097#######'),
    [TestFormInputsEnum.DOB]: faker.date.past(20),
    [TestFormInputsEnum.ROLE]: UserRolesEnum.USER,
    [TestFormInputsEnum.BUDGET]: '10000',
    [TestFormInputsEnum.REMEMBER]: true,
    [TestFormInputsEnum.FAVORITE_NUMBER]: 9,
    [TestFormInputsEnum.FAVORITE_FILM]: null,
  };
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<TestFormInputs>({
    resolver: yupResolver(validationSchema),
    defaultValues,
  });

  return (
    <form onChange={onChange} onSubmit={handleSubmit(onSubmit, onError)} noValidate>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <UIInputCopyeditor readOnly>
            <UIFormTextField margin="none" control={control} fieldKey={TestFormInputsEnum.ID} fullWidth label="ID" />
          </UIInputCopyeditor>
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormTextField margin="none" fieldKey={TestFormInputsEnum.FIRST_NAME} control={control} errors={errors} fullWidth label="First name" />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormTextField margin="none" fieldKey={TestFormInputsEnum.LAST_NAME} control={control} errors={errors} fullWidth label="Last name" />
        </Grid>
        <Grid item xs={12} sm={12}>
          <UIFormTextField
            fieldKey={TestFormInputsEnum.EMAIL}
            margin="none"
            control={control}
            errors={errors}
            fullWidth
            label="Email Address"
            autoComplete="email"
            required
            type="email"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField fieldKey={TestFormInputsEnum.PASSWORD} margin="none" control={control} errors={errors} label="Password" required fullWidth />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPasswordField
            fieldKey={TestFormInputsEnum.REPEATED_PASSWORD}
            margin="none"
            control={control}
            errors={errors}
            label="Repeated password"
            fullWidth
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormSelect
            control={control}
            onChange={onChange}
            fieldKey={TestFormInputsEnum.ROLE}
            margin="none"
            errors={errors}
            label="Role"
            fullWidth
            list={roles}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormUrlField margin="none" fieldKey={TestFormInputsEnum.URL} errors={errors} control={control} fullWidth label="Site" />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormPhoneNumberField
            defaultCountry="ua"
            margin="none"
            fieldKey={TestFormInputsEnum.PHONE}
            errors={errors}
            control={control}
            onPhoneNumberChange={onChange}
            fullWidth
            label="Phone"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormDateTimePickerField
            fieldKey={TestFormInputsEnum.DOB}
            errors={errors}
            control={control}
            onDateChange={onChange as any}
            fullWidth
            margin="none"
            label="Date of birth"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormNumberField
            thousandSeparator
            returnedValueType="value"
            InputProps={{
              startAdornment: <InputAdornment position="start">$</InputAdornment>,
            }}
            control={control}
            fieldKey={TestFormInputsEnum.BUDGET}
            errors={errors}
            fullWidth
            margin="none"
            label="Budget"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormAutocomplete<FilmOption>
            fullWidth
            options={top100Films}
            control={control}
            onAutocompleteChange={onChange}
            fieldKey={TestFormInputsEnum.FAVORITE_FILM}
            errors={errors}
            margin="none"
            label="Favorite Film"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormNumbers onChange={onChange} control={control} fieldKey={TestFormInputsEnum.FAVORITE_NUMBER} errors={errors} label="Favorite Number" />
        </Grid>
        <Grid item xs={12} sm={6}>
          <UIFormLabeledRadio options={genders} control={control} fieldKey={TestFormInputsEnum.GENDER} errors={errors} label="Gender" isRow />
        </Grid>
        <Grid item xs={12} sm={12}>
          <UIFormLabeledCheckbox control={control} fieldKey={TestFormInputsEnum.REMEMBER} errors={errors} label="Remember me" />
        </Grid>
      </Grid>
    </form>
  );
});

export default TestForm;
