import { Story, Meta } from '@storybook/react';
import { withCustomDocs } from '@storybook/utils';
import { UIButton, UIAlert } from '@ui';
import { ChangeEvent, useState, MouseEvent } from 'react';
import { SubmitErrorHandler, SubmitHandler } from 'react-hook-form';
import { Notification } from '@interfaces';
import code from './TestForm.code';
import TestFormComponent, { TestFormInputs } from './TestForm';

export default {
  component: TestFormComponent,
  title: 'Test Form',
  parameters: {
    ...withCustomDocs({ code }),
  },
  argTypes: {},
} as Meta;

const Template: Story = () => {
  const [alert, setAlert] = useState<Notification>();
  const onSubmit: SubmitHandler<TestFormInputs> = (formData) => {
    setAlert({
      severity: 'success',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onError: SubmitErrorHandler<TestFormInputs> = (errors) => {
    const errorObj = { ...errors };

    for (const key in errorObj) {
      if (Object.prototype.hasOwnProperty.call(errorObj, key)) {
        if ((errorObj as any)[key].ref) {
          delete (errorObj as any)[key].ref;
        }
      }
    }

    setAlert({
      severity: 'error',
      message: JSON.stringify(errorObj, null, 2),
    });
  };
  const onChange = (e: ChangeEvent<HTMLFormElement>) => {
    const { target } = e;
    const { value: defaultValue, name, type, checked } = target;
    let value = defaultValue;

    if (type === 'checkbox') {
      value = checked;
    }

    if (typeof value === 'object') {
      value = JSON.stringify(value, null, 2);
    }

    setAlert({
      severity: 'info',
      message: `${name}: ${value}`,
    });
  };
  const onSubmitClick = ({ currentTarget: { parentElement } }: MouseEvent<HTMLButtonElement>) => {
    const formElement = parentElement?.querySelector('form');
    formElement?.dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }));
  };
  return (
    <div>
      <TestFormComponent onSubmit={onSubmit} onChange={onChange} onError={onError} />
      <UIButton type="submit" sx={{ mt: 3 }} fullWidth onClick={onSubmitClick} variant="contained">
        Submit
      </UIButton>
      <UIAlert sx={{ mt: 3 }} severity={alert?.severity}>
        {alert?.message && <pre style={{ margin: 0 }}>{alert?.message}</pre>}
      </UIAlert>
    </div>
  );
};

export const TestForm = Template.bind({});
