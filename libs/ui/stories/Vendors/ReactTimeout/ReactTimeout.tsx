import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import ReactTimeoutLib, { Timer, ReactTimeoutProps as ReactTimeoutLibProps } from 'react-timeout';
import { UIFlexBox } from '../../../src/components';
import LoadingButton from '@mui/lab/LoadingButton';
export interface ReactTimeoutProps extends ReactTimeoutLibProps {
  timeout?: number;
  bgColor?: string;
}

let timer: any = null;

export const ReactTimeout: any = ReactTimeoutLib(({ timeout = 2000, bgColor = 'red', setTimeout, clearTimeout }: ReactTimeoutProps) => {
  const [on, setOn] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const { t } = useTranslation();
  const toggle = async () => {
    await setOn(!on);
    await setIsLoading(false);
  };
  const handleClick = () => {
    setIsLoading(true);
    clearTimeout && clearTimeout(timer);

    if (setTimeout) {
      timer = setTimeout(toggle, timeout);
    }
  };

  return (
    <UIFlexBox height="100vh" sx={{ m: '-1rem', background: on ? bgColor : null }}>
      <LoadingButton loading={isLoading} variant="contained" onClick={handleClick}>
        {t('clickMe')}!
      </LoadingButton>
    </UIFlexBox>
  );
});

export default ReactTimeout;
