import { Story, Meta } from '@storybook/react';
import ReactTimeoutComponent, { ReactTimeoutProps } from './ReactTimeout';
import { withCustomDocs } from '@storybook/utils';

const code = `
import ReactTimeout, { ReactTimeoutProps } from 'react-timeout';

let timer: any = null;

export const Component = ReactTimeout(({ setTimeout, clearTimeout }: ReactTimeoutProps) => {
  const handleClick = () => {
    clearTimeout && clearTimeout(timer);

    if (setTimeout) {
      timer = setTimeout(() => alert('!'), 2000);
    }
  };
  return (<button onClick={handleClick}>Click</button>)
});

export default Component;
`;
const link = {
  text: 'React Timeout',
  url: 'https://github.com/plougsgaard/react-timeout',
};

export default {
  title: 'Vendors/React Timeout',
  parameters: {
    ...withCustomDocs({ link, code, showPanel: true }),
  },
  controls: { expanded: true },
  argTypes: {
    timeout: {
      control: { type: 'number' },
    },
    bgColor: {
      control: {
        type: 'color',
        presetsColors: ['red'],
      },
    },
  },
} as Meta;

const Template: Story<ReactTimeoutProps> = (args) => <ReactTimeoutComponent {...args} />;

export const ReactTimeout = Template.bind({});
ReactTimeout.args = {
  timeout: 2000,
  bgColor: 'red',
};
