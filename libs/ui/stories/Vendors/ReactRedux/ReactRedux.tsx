import { Provider, useSelector, useDispatch } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import ReduxThunk from 'redux-thunk';
import { combineReducers } from 'redux';
import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';

const CONFIG_KEY = 'config';

interface ConfigState {
  property: number;
  isPropertyLoading: boolean;
  isPropertyLoaded: boolean;
  propertyErrorMessage: string | null;
}

interface StoreState {
  [CONFIG_KEY]: ConfigState;
}

const initialState: ConfigState = {
  property: 0,
  isPropertyLoading: false,
  isPropertyLoaded: false,
  propertyErrorMessage: null,
};

const selectConfigState = (state: StoreState): ConfigState => state[CONFIG_KEY];

export const fetchProperty = createAsyncThunk(`${CONFIG_KEY}/fetchProperty`, async (_, { rejectWithValue }) => {
  try {
    return 5;
  } catch (error) {
    const err = error as any;
    return rejectWithValue(err.message);
  }
});

const configSlice = createSlice({
  name: CONFIG_KEY,
  initialState,
  reducers: {
    setProperty: (state, { payload }: PayloadAction<number>) => {
      state.property += payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProperty.pending, (state) => {
        state.isPropertyLoading = true;
      })
      .addCase(fetchProperty.fulfilled, (state, { payload }: PayloadAction<number>) => {
        state.property = payload;
        state.isPropertyLoaded = true;
        state.isPropertyLoading = false;
      })
      .addCase(fetchProperty.rejected, (state, action) => {
        state.isPropertyLoading = false;
        state.propertyErrorMessage = action.error.message || null;
      });
  },
});
const { setProperty } = configSlice.actions;
const configReducer = configSlice.reducer;
const reducers = combineReducers<StoreState>({
  [CONFIG_KEY]: configReducer,
});
const store = configureStore({
  reducer: reducers,
  middleware: [ReduxThunk],
  devTools: process.env.NODE_ENV !== 'production',
});

const App = () => {
  const dispatch = useDispatch();
  const { property, isPropertyLoading } = useSelector(selectConfigState);

  return (
    <div>
      <p>
        <button onClick={() => dispatch(fetchProperty())}>Fetch property</button>
      </p>
      {isPropertyLoading && <div>Loading...</div>}
      <p>
        <button onClick={() => dispatch(setProperty(-1))}>-1</button>
        <span style={{ margin: '0 1rem' }}>{property}</span>
        <button onClick={() => dispatch(setProperty(1))}>+1</button>
      </p>
    </div>
  );
};

const ReactRedux = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default ReactRedux;
