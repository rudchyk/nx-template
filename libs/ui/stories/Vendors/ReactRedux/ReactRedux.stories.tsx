import { Story, Meta } from '@storybook/react';
import { withCustomDocs } from '../../../storiesHelpers/utils';
import ReactReduxComponent from './ReactRedux';

const list = [
  {
    name: 'App.tsx',
    code: `
import { useSelector, useDispatch } from 'react-redux';
import { selectConfigState, setProperty, fetchProperty } from './config.slice.ts'

const App = () => {
  const dispatch = useDispatch();
  const { property, isPropertyLoading } = useSelector(selectConfigState);

  return (
    <div>
      <p>
        <button onClick={() => dispatch(fetchProperty())}>Fetch property</button>
      </p>
      {isPropertyLoading && <div>Loading...</div>}
      <p>
        <button onClick={() => dispatch(setProperty(-1))}>-1</button>
        <span style={{ margin: '0 1rem' }}>{property}</span>
        <button onClick={() => dispatch(setProperty(1))}>+1</button>
      </p>
    </div>
  );
}

export default App;
    `,
  },
  {
    name: './config.slice.ts',
    code: `
import { createSlice, PayloadAction, createAsyncThunk } from '@reduxjs/toolkit';
import { RootState } from './store';

export const CONFIG_KEY = 'config';

export interface ConfigState {
  property: number;
  isPropertyLoading: boolean;
  isPropertyLoaded: boolean;
  propertyErrorMessage: string | null;
}

const initialState: ConfigState = {
  property: 0,
  isPropertyLoading: false,
  isPropertyLoaded: false,
  propertyErrorMessage: null,
};

export const fetchProperty = createAsyncThunk<number>(
  \`\${CONFIG_KEY}/fetchProperty\`,
  async (_, { dispatch, getState, rejectWithValue }) => {
    try {
      return 5;
    } catch (error) {
      const err = error as any;
      return rejectWithValue(err.message);
    }
  }
);

export const selectConfigState = (state: RootState): ConfigState => state[CONFIG_KEY];

export const configSlice = createSlice({
  name: CONFIG_KEY,
  initialState,
  reducers: {
    setProperty: (state, { payload }: PayloadAction<number>) => {
      state.property += payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchProperty.pending, (state) => {
        state.isPropertyLoading = true;
      })
      .addCase(fetchProperty.fulfilled, (state, { payload }: PayloadAction<number>) => {
        state.property = payload;
        state.isPropertyLoaded = true;
        state.isPropertyLoading = false;
      })
      .addCase(fetchProperty.rejected, (state, action) => {
        state.isPropertyLoading = false;
        state.propertyErrorMessage = action.error.message || null;
      });
  },
});

export const { setProperty } = configSlice.actions;

export const configReducer = configSlice.reducer;
    `,
  },
  {
    name: './reducers.ts',
    code: `
import { combineReducers } from 'redux';
import {
  CONFIG_KEY,
  ConfigState,
  configReducer,
  // imports
} from './config.slice.ts';

export interface StoreState {
  [CONFIG_KEY]: ConfigState;
  // state
}

export const reducers = combineReducers<StoreState>({
  [CONFIG_KEY]: configReducer,
  // reducers
});
`,
  },
  {
    name: './store.ts',
    code: `
import { configureStore } from '@reduxjs/toolkit';
import ReduxThunk from 'redux-thunk';
import { reducers } from './reducers';

export type RootState = ReturnType<typeof store.getState>;

const store = configureStore({
  reducer: reducers,
  middleware: [ReduxThunk],
  devTools: process.env.NODE_ENV !== 'production',
});

export default store;

    `,
  },
  {
    name: 'Configuring',
    code: `
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import App from './App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
    `,
  },
];

const link = {
  text: 'React Redux',
  url: 'https://redux-toolkit.js.org/',
};

export default {
  title: 'Vendors/React Redux',
  parameters: {
    ...withCustomDocs({ link, list }),
  },
} as Meta;

const Template: Story = () => <ReactReduxComponent />;

export const ReactRedux = Template.bind({});
