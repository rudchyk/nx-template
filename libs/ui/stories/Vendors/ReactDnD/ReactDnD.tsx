import { useCallback, useState } from 'react';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';
import update from 'immutability-helper';
import Card from './components/Card';

export interface Item {
  id: number;
  text: string;
}

export const ReactDnDSection = () => {
  const defaultList: Item[] = [
    {
      id: 1,
      text: 'Write a cool JS library',
    },
    {
      id: 2,
      text: 'Make it generic enough',
    },
    {
      id: 3,
      text: 'Write README',
    },
    {
      id: 4,
      text: 'Create some examples',
    },
    {
      id: 5,
      text: 'Spam in Twitter and IRC to promote it (note that this element is taller than the others)',
    },
    {
      id: 6,
      text: '???',
    },
    {
      id: 7,
      text: 'PROFIT',
    },
  ];
  const [cards, setCards] = useState(defaultList);

  const moveCard = useCallback((dragIndex: number, hoverIndex: number) => {
    setCards((prevCards: Item[]) =>
      update(prevCards, {
        $splice: [
          [dragIndex, 1],
          [hoverIndex, 0, prevCards[dragIndex] as Item],
        ],
      })
    );
  }, []);

  return (
    <DndProvider backend={HTML5Backend}>
      {cards.map((card, i) => (
        <Card key={card.id} index={i} id={card.id} text={card.text} moveCard={moveCard} />
      ))}
    </DndProvider>
  );
};

export default ReactDnDSection;
