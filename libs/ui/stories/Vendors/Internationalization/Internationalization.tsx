import i18n from 'i18next';
import { useEffect, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { initReactI18next } from 'react-i18next';
import { UILocalizationSwitcher } from '../../../src/components';

const resources = {
  en: {
    translation: {
      simpleContent: 'Just simple content',
      simpleContentWithVar: 'Just simple content for user {{name}}',
      simpleContentWithComponents: 'Hello <0>{{name}}</0>, you have {{count}} unread message. <1>Go to react-i18next</1>.',
      simpleContentWithNamedComponents: 'hello <italic>beautiful</italic> <bold>{{what}}</bold>',
      sergii: 'Sergii',
    },
  },
  ua: {
    translation: {
      simpleContent: 'Простий вміст',
      simpleContentWithVar: 'Простий вміст для користувача {{name}}',
      simpleContentWithComponents: 'Привіт <0>{{name}}</0>, ви маєте {{count}} непрочитаних повідомлення. <1>Перейти до react-i18next</1>.',
      simpleContentWithNamedComponents: 'Привіт <italic>красивий</italic> <bold>{{what}}</bold>',
      sergii: 'Сергій',
    },
  },
};
const defaultLanguage = 'en';
const languagesList = [defaultLanguage, 'ua'];
const getLangValue = () => {
  if (languagesList.includes(navigator.language)) {
    return navigator.language;
  }

  return defaultLanguage;
};

i18n.use(initReactI18next).init({
  lng: navigator.language || defaultLanguage,
  fallbackLng: defaultLanguage,
  debug: process.env.NODE_ENV !== 'production',
  interpolation: {
    escapeValue: false,
  },
  resources,
});

export const Internationalization = () => {
  const [language, setLanguage] = useState(getLangValue());
  const { t, i18n } = useTranslation();
  const name = t('sergii');

  const onMenuItemClick = (language: string) => {
    setLanguage(language);
  };

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language]);

  return (
    <div>
      <UILocalizationSwitcher onChangeLanguage={onMenuItemClick} language={language} languagesList={languagesList} />
      <p>{t('simpleContent')}</p>
      <p>{t('simpleContentWithVar', { name })}</p>
      <Trans
        parent="p"
        i18nKey="simpleContentWithComponents"
        values={{
          count: 12,
          name,
        }}
        components={[<strong title={t('nameTitle')} />, <a href="https://react.i18next.com/" target="_blank" />]}
      />
      <Trans
        parent="p"
        i18nKey="simpleContentWithNamedComponents"
        values={{
          what: 'World',
        }}
        components={{ italic: <i />, bold: <strong /> }}
      />
    </div>
  );
};

export default Internationalization;
