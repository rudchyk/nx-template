import { Story, Meta } from '@storybook/react';
import { withCustomDocs, CodeElement } from '../../../storiesHelpers/utils';

const configuringRoutesCode = `
import { render } from 'react-dom';
import { BrowserRouter, Routes, Route, Outlet, useParams, useNavigate, useLocation } from 'react-router-dom';
// import your route components too

const Invoices = () => {
  return (
    <div>
      <h1>Invoices</h1>
      <Outlet />
    </div>
  );
};

const Invoice = () => {
  const { invoiceId } = useParams();
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <main style={{ padding: '1rem' }}>
      <h1>Invoice {invoiceId}</h1>
      <p>
        <button onClick={() => navigate('/invoices' + location.search)}>Delete</button>
      </p>
    </main>
  );
};

const NotFound = () => {
  return (
    <main style={{ padding: '1rem' }}>
      <p>There's nothing here!</p>
    </main>
  );
};

const Home = () => {
  return (
    <main style={{ padding: '1rem' }}>
      <p>Home</p>
    </main>
  );
};

const Dashboard = () => {
  return (
    <main style={{ padding: '1rem' }}>
      <p>Dashboard</p>
    </main>
  );
};

render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="dashboard" element={<Dashboard />}>
        <Route path="invoices" element={<Invoices />}>
          <Route path=":invoiceId" element={<Invoice />} />
        </Route>
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);
`;

const navigateCode = `
import { Navigate } from 'react-router-dom';

const App = () => {
  return <Navigate to='/' replace />;
};
`;

const list: CodeElement[] = [
  {
    name: 'Configuring Routes',
    code: configuringRoutesCode,
  },
  {
    name: 'Navigate',
    code: navigateCode,
  },
];

const link = {
  text: 'React Router',
  url: 'https://reactrouter.com/docs/en/v6/getting-started/overview',
};

export default {
  title: 'Vendors/React Router',
  parameters: {
    ...withCustomDocs({ link, list, hideCanvas: true }),
  },
} as Meta;

const Template: Story = () => <div />;

export const ReactRouter = Template.bind({});
