import { Story, Meta } from '@storybook/react';
import FeatureTogglesComponent from './FeatureToggles';
import { withCustomDocs } from '../../../storiesHelpers/utils';

const code = `
import { Feature, useFeatures, FeatureToggles } from '@paralleldrive/react-feature-toggles';

const FaqFeatureComponent = () => {
  return (
    <div>
      <h3>Faq Feature</h3>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam, aut.</p>
    </div>
  );
};

const Test = () => {
  const features = useFeatures();
  const isFaq = = () => features.includes('faq');

  return (
    <>
      {isFaq() && <div>FAQ</div>}
      <Feature name="faq" activeComponent={FaqFeatureComponent} />
    </>
  )
}

const App = () => {
  const features = ['faq', 'foo', 'bar'];

  return (
    <FeatureToggles features={features}>
      <Test />
    </FeatureToggles>
  )
}
`;

const link = {
  text: 'React Feature Toggles',
  url: 'https://github.com/paralleldrive/react-feature-toggles',
};

export default {
  title: 'Vendors/Feature Toggles',
  parameters: {
    ...withCustomDocs({ link, code }),
  },
} as Meta;

const Template: Story = () => <FeatureTogglesComponent />;

export const FeatureToggles = Template.bind({});
