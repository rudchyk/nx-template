import { Feature, useFeatures, FeatureToggles } from '@paralleldrive/react-feature-toggles';
import { ChangeEvent, useState } from 'react';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import classnames from 'classnames';
import styles from './FeatureToggles.module.sass';

enum FeaturesEnum {
  FAQ = 'faq',
  FOO = 'foo',
}

const FooFeatureComponent = () => {
  return (
    <div>
      <h3>Faq Feature</h3>
      <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Magnam, aut.</p>
    </div>
  );
};

const FaqFeatureComponent = () => {
  return (
    <div>
      <h3>Faq Feature</h3>
      <p>
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio veritatis ducimus incidunt quos cum voluptate reprehenderit vitae, sit modi beatae
        nesciunt est repellendus quidem natus aspernatur facere vel assumenda! Illum consequatur veniam corporis deleniti labore? Eaque nihil, libero magni
        alias temporibus architecto, officiis quo quam quisquam ea omnis sapiente iste!
      </p>
    </div>
  );
};

interface FeatureTogglesComponentProps {
  onChange: any;
}

const FeatureTogglesComponent = ({ onChange }: FeatureTogglesComponentProps) => {
  const storedFeatures = Object.values(FeaturesEnum);
  const features = useFeatures();
  const isFeatureEnabled = (value: string) => features.includes(value);
  const getFeatureBtnText = (value: string) => `${isFeatureEnabled(value) ? 'Disable' : 'Enable'} ${value.toUpperCase()} Feature`;

  return (
    <>
      <FormGroup>
        {storedFeatures.map((storedFeature) => (
          <FormControlLabel
            key={storedFeature}
            control={<Switch onChange={onChange} checked={isFeatureEnabled(storedFeature)} value={storedFeature} />}
            label={getFeatureBtnText(storedFeature)}
          />
        ))}
      </FormGroup>
      <div>
        <p
          className={classnames({
            [styles.red]: !features.includes(FeaturesEnum.FAQ),
            [styles.green]: features.includes(FeaturesEnum.FAQ),
          })}>
          {features.includes(FeaturesEnum.FAQ) ? 'The FAQ feature is active' : 'The FAQ feature is inactive'}
        </p>
        <p
          className={classnames({
            [styles.red]: !features.includes(FeaturesEnum.FOO),
            [styles.green]: features.includes(FeaturesEnum.FOO),
          })}>
          {features.includes(FeaturesEnum.FOO) ? 'The FOO feature is active' : 'The FOO feature is inactive'}
        </p>
        <Feature name={FeaturesEnum.FAQ} activeComponent={FaqFeatureComponent} />
        <Feature name={FeaturesEnum.FOO} activeComponent={FooFeatureComponent} />
        {!features.includes(FeaturesEnum.FOO) && (
          <div>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Enim quos aliquam in, ea rerum saepe, dicta nam laborum numquam neque exercitationem earum,
            distinctio omnis repellat et soluta quibusdam architecto velit.
          </div>
        )}
      </div>
    </>
  );
};

export const FeatureTogglesSection = () => {
  const [enabledFeatures, setEnabledFeatures] = useState<string[]>([]);
  const isFeatureEnabled = (value: string) => enabledFeatures.includes(value);

  const onFeatureChange = ({ currentTarget: { value } }: ChangeEvent<HTMLInputElement>) => {
    if (isFeatureEnabled(value)) {
      setEnabledFeatures(enabledFeatures.filter((feature) => feature !== value));
    } else {
      setEnabledFeatures([...enabledFeatures, value]);
    }
  };

  return (
    <FeatureToggles features={enabledFeatures}>
      <FeatureTogglesComponent onChange={onFeatureChange} />
    </FeatureToggles>
  );
};

export default FeatureTogglesSection;
