import { Story, Meta } from '@storybook/react';
import { withoutDocsOptions } from '../../storiesHelpers/constants';
import SassMixinsComponent from './SassMixins';
import SassVariablesComponent from './SassVariables';

export default {
  title: 'Sass',
  parameters: {
    ...withoutDocsOptions,
  },
} as Meta;

const SassMixinsTemplate: Story = () => <SassMixinsComponent />;
const SassVariablesTemplate: Story = () => <SassVariablesComponent />;

export const Mixins = SassMixinsTemplate.bind({});

export const Variables = SassVariablesTemplate.bind({});
