import { Typography, Paper } from '@mui/material';
import { UICodeBlock } from '../../src/components';
import styles from './SassDoc.module.sass';

interface Mixin {
  name: string;
  code: string;
  use: string;
  example: string;
  snippet: string;
}

const SassMixins = () => {
  const mixins: Mixin[] = [
    {
      name: 'Dark Theme',
      code: `@mixin dark()
  :global(.dark) &
    @content`,
      use: `{CSS Selector}
  +dark
    {CSS Rule}`,
      example: `:global(pre[class*="language-"])
  +dark
    background: var(--neutral-main)
.component
  color: var(--primary-light)`,
      snippet: `+dark`,
    },
  ];

  return (
    <>
      <Typography variant="h2" className={styles.title}>
        MIXINS
      </Typography>
      {mixins.map(({ name, code, use, example, snippet }: any) => (
        <Paper
          key={name}
          elevation={2}
          sx={{
            p: 1,
            my: 2,
          }}>
          <Typography sx={{ my: 2 }} variant="h5">
            {name}
          </Typography>
          <Typography variant="body1">Mixin:</Typography>
          <UICodeBlock code={code} language="sass" />
          <Typography variant="body1">Snippet:</Typography>
          <UICodeBlock code={snippet} language="sass" />
          <Typography variant="body1">Usage:</Typography>
          <UICodeBlock code={use} language="sass" />
          <Typography variant="body1">Example:</Typography>
          <UICodeBlock code={example} language="sass" />
        </Paper>
      ))}
    </>
  );
};

export default SassMixins;
