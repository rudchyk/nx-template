import { Typography, Paper } from '@mui/material';
import { UIInputCopyeditor, UIFlexBox, UIFormTextField } from '../../src/components';
import styles from './SassDoc.module.sass';
import sassVariables from '../../src/styles/export.module.sass';

const SassVariables = () => {
  return (
    <>
      <Typography variant="h2" className={styles.title}>
        VARIABLES
      </Typography>
      {Object.keys(sassVariables).map((variable) => (
        <UIFlexBox ai="c" jc="s" className={styles.box}>
          <UIInputCopyeditor readOnly>
            <UIFormTextField size="small" className={styles.input} label={null} key={variable} fieldKey={variable} defaultValue={`$${variable}`} />
          </UIInputCopyeditor>
          <span className={styles.separator}>:</span>
          <span className={styles.fz}>{sassVariables[variable]}</span>
        </UIFlexBox>
      ))}
    </>
  );
};

export default SassVariables;
