interface withDescriptionLinkProps {
  text: string;
  url: string;
}

export const withDescriptionLink = ({ text, url }: withDescriptionLinkProps) => {
  return {
    parameters: {
      docs: {
        description: {
          component: `[${text}](${url})`,
        },
      },
    },
  };
};
