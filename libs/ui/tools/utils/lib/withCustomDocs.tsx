import { Docs, DocsProps, CodeItem } from '../../components';

interface withCustomDocsProps extends DocsProps {
  hideCanvas?: boolean;
  showPanel?: boolean;
  onLoad?: () => void;
  onDestroy?: () => void;
}

export type CodeElement = CodeItem;

export const withCustomDocs = ({ link, code, list, children, hideCanvas = false, showPanel = false, onLoad, onDestroy }: withCustomDocsProps) => {
  const props: any = {
    docs: {
      page: () => <Docs link={link} code={code} list={list} children={children} onLoad={onLoad} onDestroy={onDestroy} />,
    },
    options: {
      // https://storybook.js.org/docs/react/configure/features-and-behavior
      showPanel,
    },
  };

  if (hideCanvas) {
    props.previewTabs = {
      canvas: { hidden: true },
    };
    props.viewMode = 'docs';
  }

  return props;
};
