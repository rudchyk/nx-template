export const getFormSnippet = (formName: string) => `
import { ${formName} } from '@client/forms';
import { useRichForm } from '@hooks';
import { ${formName}Inputs } from '@interfaces';

const App = () => {
  const { isLoading, onSubmit, alert, clearAlert } = useRichForm({
    service: {{API Service}},
  });
  const onFormSubmit = (formData: ${formName}Inputs) => {
    onSubmit<${formName}Inputs, {{API Interface}}>(formData, (data) => {
      // TODO
    });
  };
  const onFormError = (errors) => {
    // TODO
  };

  return (
    <${formName} onChange={clearAlert} onSubmit={onFormSubmit} onError={onFormError} isLoading={isLoading} alert={alert} />
  );
};
`;
