interface DisabledControls {
  [key: string]: {
    control: boolean;
  };
}

export const getDisabledControls = (list: string[]): DisabledControls => {
  const obj: DisabledControls = {};

  list.forEach((item) => {
    obj[item] = {
      control: false,
    };
  });

  return obj;
};
