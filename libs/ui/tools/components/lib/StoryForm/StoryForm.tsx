import { UIButton, UIButtonProps, UIAlert } from '@ui';
import { ReactElement, MouseEvent, cloneElement, useState, ChangeEvent } from 'react';
import { Notification } from '@interfaces';

export interface StoryFormProps {
  children: ReactElement;
  isDefaultButton?: boolean;
  defaultButtonProps?: UIButtonProps;
}

export const StoryForm = ({ children, isDefaultButton = false, defaultButtonProps = {} }: StoryFormProps) => {
  const [alert, setAlert] = useState<Notification>();
  const onSubmit = (formData: any) => {
    setAlert({
      severity: 'success',
      message: JSON.stringify(formData, null, 2),
    });
  };
  const onError = (errors: any) => {
    const errorObj = { ...errors };

    for (const key in errorObj) {
      if (Object.prototype.hasOwnProperty.call(errorObj, key)) {
        if (errorObj[key].ref) {
          delete errorObj[key].ref;
        }
      }
    }

    setAlert({
      severity: 'error',
      message: JSON.stringify(errorObj, null, 2),
    });
  };
  const onChange = (e: ChangeEvent<HTMLFormElement>) => {
    const { target } = e;
    const { value: defaultValue, name, type, checked } = target;
    let value = defaultValue;

    if (type === 'checkbox') {
      value = checked;
    }

    setAlert({
      severity: 'info',
      message: `${name}: ${value}`,
    });
  };
  const Form = cloneElement(children, { onChange, onSubmit, onError });
  const onSubmitClick = ({ currentTarget: { parentElement } }: MouseEvent<HTMLButtonElement>) => {
    const formElement = parentElement?.querySelector('form');
    formElement?.dispatchEvent(new Event('submit', { cancelable: true, bubbles: true }));
  };

  return (
    <div>
      {Form}
      {isDefaultButton && (
        <UIButton type="submit" sx={{ mt: 3 }} fullWidth onClick={onSubmitClick} variant="contained" {...defaultButtonProps}>
          Submit
        </UIButton>
      )}
      <UIAlert sx={{ mt: 3 }} severity={alert?.severity}>
        {alert?.message && <pre style={{ margin: 0 }}>{alert?.message}</pre>}
      </UIAlert>
    </div>
  );
};

export default StoryForm;
