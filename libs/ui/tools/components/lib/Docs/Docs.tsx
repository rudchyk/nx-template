import { Title, Subtitle, Subheading, Wrapper, Heading, Description } from '@storybook/addon-docs';
import { ReactNode, useEffect } from 'react';
import { UICodeBlock } from '@ui';
import { DescriptionLink } from '../DescriptionLink/DescriptionLink';

export interface DocsLink {
  text: string;
  url: string;
}

export interface CodeItem {
  name: string;
  code: string;
}
export interface DocsProps {
  link?: DocsLink;
  code?: string;
  list?: CodeItem[];
  children?: ReactNode;
  description?: string;
  onLoad?: () => void;
  onDestroy?: () => void;
}

export const Docs = ({ link, code, list, children, description, onLoad, onDestroy }: DocsProps) => {
  useEffect(() => {
    onLoad && onLoad();
    return () => {
      onDestroy && onDestroy();
    };
  }, []);

  return (
    <Wrapper>
      <Title />
      {link && (
        <div style={{ margin: '2rem 0' }}>
          <DescriptionLink text={link.text} url={link.url} />
        </div>
      )}
      {description && <Description>{description}</Description>}
      {code && (
        <div style={{ margin: '1rem 0' }}>
          <UICodeBlock code={code} />
        </div>
      )}
      {list &&
        list.map(({ name, code }) => (
          <div key={name} style={{ margin: '2rem 0' }}>
            <Heading>{name}</Heading>
            <div style={{ margin: '1rem 0' }}>
              <UICodeBlock code={code} />
            </div>
          </div>
        ))}
      {children}
    </Wrapper>
  );
};
