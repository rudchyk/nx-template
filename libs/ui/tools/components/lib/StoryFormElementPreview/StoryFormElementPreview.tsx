import { ReactElement } from 'react';
import { UIAlert } from '@ui';

export interface StoryFormElementPreviewProps {
  children: ReactElement;
  fieldKey: string;
  view: any;
}

export const StoryFormElementPreview = ({ children, fieldKey, view }: StoryFormElementPreviewProps) => (
  <div>
    <UIAlert sx={{ mt: 3 }} severity="info">
      <pre style={{ margin: 0 }}>
        {fieldKey}: {view}
      </pre>
    </UIAlert>
    {children}
  </div>
);

export default StoryFormElementPreview;
