interface DescriptionLinkProps {
  text: string;
  url: string;
}
export const DescriptionLink = ({ text, url }: DescriptionLinkProps) => (
  <a href={url} rel="noreferrer" title={text} target="_blank">
    {text}
  </a>
);
