export const withoutDocsOptions = {
  previewTabs: {
    'storybook/docs/panel': {
      hidden: true,
    },
  },
  options: {
    // https://storybook.js.org/docs/react/configure/features-and-behavior
    showPanel: false,
  },
};
