import en from './lib/en.json';
import ua from './lib/ua.json';

const translations = {
  en,
  ua,
};

export default translations;
