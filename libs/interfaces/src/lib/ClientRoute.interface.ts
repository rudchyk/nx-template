import { UserRolesEnum, ClientRouteProtectionTypesEnum } from '@constants';

export type ClientRouteProtection = UserRolesEnum.ADMIN | UserRolesEnum.USER | ClientRouteProtectionTypesEnum.FEATURE | undefined;
