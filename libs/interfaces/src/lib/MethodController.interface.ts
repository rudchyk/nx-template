import { Request, Response, NextFunction } from 'express';

export type MethodController = (req: Request, res: Response, next: NextFunction) => Promise<void | Response>;
