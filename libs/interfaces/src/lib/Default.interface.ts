export interface AnyObj {
  [key: string]: any;
}

export type MUIMarginType = 'normal' | 'none' | 'dense';

export type MUIButtonsColors = 'inherit' | 'primary' | 'secondary' | 'default' | 'success' | 'error' | 'info' | 'warning';
