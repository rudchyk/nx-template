import { ConfigEnum, FeaturesEnum } from '@constants';

export interface ConfigData {
  [ConfigEnum.ENABLED_FEATURES]: FeaturesEnum[];
}
