import { AlertColor } from '@mui/material';

export interface Notification {
  severity: AlertColor;
  message: string;
}
