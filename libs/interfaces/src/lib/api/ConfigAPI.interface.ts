import { ConfigData } from '../Config.interface';
export interface UpdateConfigResponseAPIData {
  message: string;
}
export interface GetConfigResponseAPIData {
  config: ConfigData;
}
