import { User } from '../User.interface';

export interface UsersResponseAPIData {
  users: User[];
}

export interface DeleteUsersResponseAPIData {
  message: string;
  users: User[];
}
export interface AddUserAPIData {
  message: string;
  users: User[];
}
