export interface PostAPIInterface {
  userId: number;
  id: number;
  title: string;
  body: string;
}
