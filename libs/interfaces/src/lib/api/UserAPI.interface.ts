import { User } from '../User.interface';

export interface UserResponseAPIData {
  user: User;
}

export interface DeleteUserResponseAPIData {
  message: string;
}

export interface UpdateUserResponseAPIData {
  message: string;
  user: User;
}

export interface ChangeUserPasswordResponseAPIData {
  message: string;
  user: User;
}
