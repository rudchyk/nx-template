export interface ApiErrorResponse {
  controller: string;
  message: string;
}

export interface ApiSuccessResponse {
  [key: string]: any;
}

export interface DeleteItemsRequestData {
  ids: string[];
}
