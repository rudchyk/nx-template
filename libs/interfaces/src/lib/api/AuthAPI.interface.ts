import { UserRolesEnum } from '@constants';
import { User } from '../User.interface';
export interface VerifyEmailAPIData {
  email: string;
}
export interface ResetPasswordAPIData {
  email: string;
}

export interface AuthAPIData {
  user: User;
  token: string;
  remember: boolean;
}

export interface AuthHeaderAPIData {
  email: string;
  id: string;
  role: UserRolesEnum;
}
