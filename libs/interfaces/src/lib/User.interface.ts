import { UserRolesEnum } from '@constants';

export interface UserData {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  role: UserRolesEnum;
  dob?: Date;
  phone?: string;
  createdAt?: Date;
  updatedAt?: Date;
  emailConfirmedAt?: Date;
  emailConfirmedCode?: string;
}

export interface User extends UserData {
  id: string;
}
