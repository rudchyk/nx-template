import {
  // enum
  AddUserFormInputsEnum,
  UserRolesEnum,
  ChangePasswordFormInputsEnum,
  LogInFormInputsEnum,
  ResetPasswordFormInputsEnum,
  SignUpFormInputsEnum,
  UpdateProfileFormInputsEnum,
  VerifyEmailFormInputsEnum,
} from '@constants';

export interface AddUserFormData {
  [AddUserFormInputsEnum.FIRST_NAME]: string;
  [AddUserFormInputsEnum.LAST_NAME]: string;
  [AddUserFormInputsEnum.EMAIL]: string;
  [AddUserFormInputsEnum.PASSWORD]: string;
  [AddUserFormInputsEnum.ROLE]: UserRolesEnum;
}

export interface AddUserFormInputs extends AddUserFormData {
  [AddUserFormInputsEnum.REPEATED_PASSWORD]: string;
}

export interface ChangePasswordFormData {
  [ChangePasswordFormInputsEnum.OPD_PASSWORD]: string;
  [ChangePasswordFormInputsEnum.NEW_PASSWORD]: string;
}
export interface ChangePasswordFormInputs extends ChangePasswordFormData {
  [ChangePasswordFormInputsEnum.REPEATED_NEW_PASSWORD]: string;
}

export interface LogInFormInputs {
  [LogInFormInputsEnum.EMAIL]: string;
  [LogInFormInputsEnum.PASSWORD]: string;
  [LogInFormInputsEnum.REMEMBER]: boolean;
}

export interface ResetPasswordData {
  [ResetPasswordFormInputsEnum.EMAIL]: string;
  [ResetPasswordFormInputsEnum.PASSWORD]: string;
}

export interface ResetPasswordFormInputs extends ResetPasswordData {
  [ResetPasswordFormInputsEnum.REPEATED_PASSWORD]: string;
}

export interface SignUpData {
  [SignUpFormInputsEnum.FIRST_NAME]: string;
  [SignUpFormInputsEnum.LAST_NAME]: string;
  [SignUpFormInputsEnum.EMAIL]: string;
  [SignUpFormInputsEnum.PASSWORD]: string;
  [SignUpFormInputsEnum.REPEATED_PASSWORD]: string;
  [SignUpFormInputsEnum.REMEMBER]: boolean;
  [SignUpFormInputsEnum.MARKETING_PROMOTIONS]: boolean;
}

export interface SignUpFormInputs extends SignUpData {
  [SignUpFormInputsEnum.REPEATED_PASSWORD]: string;
}

export interface UpdateProfileFormInputs {
  [UpdateProfileFormInputsEnum.FIRST_NAME]: string;
  [UpdateProfileFormInputsEnum.LAST_NAME]: string;
  [UpdateProfileFormInputsEnum.PHONE]: string;
  [UpdateProfileFormInputsEnum.DOB]: string | Date | null;
}

export interface VerifyEmailFormInputs {
  [VerifyEmailFormInputsEnum.EMAIL]: string;
}
